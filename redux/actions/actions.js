import {
    FETCH_PRODUCTS,
    LOGOUT,
    SIGN_IN,
    SET_GUEST_ID,
    HOME_BANNER_DATA,
    ADD_TO_CART,
    REMOVE_FROM_CART,
    UPDATE_QUANTITY_IN_CART,
    VIEW_CART,
    ADD_TO_WISHLIST,
    HOME_PRODUCT_FAILURE,
    HOME_PRODUCT_REQUEST,
    HOME_PRODUCT_SUCCESS,
    REMOVE_FROM_WISHLIST,
    CHANGE_APP_LANGUAGE,
    // SET_CUSTOMER_ADDRESS_REQUEST,
    // SET_CUSTOMER_ADDRESS_SUCCESS,
    // SET_CUSTOMER_ADDRESS_FAILURE,
    BUY_NOW,
    RESET_ADDRESS,
    SEARCH_REQUEST,
    SEARCH_SUCCESS,
    SEARCH_FAILED,
    PLACE_ORDER_REQUEST,
    PLACE_ORDER_SUCCESS,
    PLACE_ORDER_FAILED,
    RESET_STATUS,
    ORDERS_REQUEST,
    ORDERS_SUCCESS,
    ORDERS_FAILED,
    QUANTITY_DOWN,
    QUANTITY_UP,
    QUANTITY_CHANGE,
    QUANTITY_CHANGE_FAILURE,
    QUANTITY_CHANGE_SUCCESS,
    VIEW_CART_REQUEST,
    VIEW_CART_SUCCESS,
    VIEW_CART_FAILURE,
    VIEW_WISHLIST_REQUEST,
    VIEW_WISHLIST_SUCCESS,
    VIEW_WISHLIST_FAILURE,
    ADD_TO_CART_REQUEST,
    ADD_TO_CART_SUCCESS,
    ADD_TO_CART_FAILURE,
    ADD_TO_WISHLIST_REQUEST,
    ADD_TO_WISHLIST_SUCCESS,
    ADD_TO_WISHLIST_FAILURE,
    REMOVE_FROM_CART_REQUEST,
    REMOVE_FROM_CART_SUCCESS,
    REMOVE_FROM_CART_FAILURE,
    REMOVE_FROM_WISHLIST_REQUEST,
    REMOVE_FROM_WISHLIST_SUCCESS,
    REMOVE_FROM_WISHLIST_FAILURE,
    PRODUCT_DETAIL_REQUEST,
    PRODUCT_DETAIL_SUCCESS,
    PRODUCT_DETAIL_FAILURE,
    SET_USER_TOKEN,
    SET_USER_DETAILS,
    SET_USER_DETAILS_SUCCESS,
    SET_USER_DETAILS_FAILURE,
    GET_USER_DETAILS_SUCCESS,
    GET_USER_DETAILS_FAILURE,
    GET_CUSTOMER_ADDRESS_LIST_REQUEST,
    GET_CUSTOMER_ADDRESS_LIST_SUCCESS,
    GET_CUSTOMER_ADDRESS_LIST_FAILURE,

    ADD_CUSTOMER_ADDRESS_REQUEST, ADD_CUSTOMER_ADDRESS_SUCCESS, ADD_CUSTOMER_ADDRESS_FAILURE,

    UPDATE_CUSTOMER_ADDRESS_LIST_REQUEST, UPDATE_CUSTOMER_ADDRESS_LIST_SUCCESS, UPDATE_CUSTOMER_ADDRESS_LIST_FAILURE,

    DELETE_CUSTOMER_ADDRESS_REQUEST, DELETE_CUSTOMER_ADDRESS_SUCCESS, DELETE_CUSTOMER_ADDRESS_FAILURE
} from '../actions/types'

import AxiosInstance from '../misc/axiosInstance';

export function getUserDetails(token) {
    return async dispatch=> {
        try {
            let response = await AxiosInstance.post('userdata.php', token);
            console.log(response, 'response from userdata')
            if (!response.data.error) {
                dispatch({
                    type: GET_USER_DETAILS_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: GET_USER_DETAILS_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log(e);
            dispatch({
                type: GET_USER_DETAILS_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}

export function setUserDetails(details) {
    return async dispatch => {

        dispatch({
            type: SET_USER_DETAILS,
            payload: 'pending'
        })
        try {
            let response = await AxiosInstance.post('update_user_data.php', details)
            console.log(response, 'response from userdata')
            response.details = details;
            if (!response.data.error) {
                dispatch({
                    type: SET_USER_DETAILS_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: SET_USER_DETAILS_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log(e);
            dispatch({
                type: SET_USER_DETAILS_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}

export function changeAppLanguage(language) {
    return{
        type: CHANGE_APP_LANGUAGE,
        payload: language
    }
}

export function setGuestId(id){
    return{
        type:SET_GUEST_ID,
        payload:id
    }
}
export function fetchHomeProducts(id) {
    return {
        type: FETCH_PRODUCTS,
        payload: id
    }
}
export function userSignIn(data) {
    return {
        type: SIGN_IN,
        payload: data
    }
}
export function logout() {
    return {
        type: LOGOUT,
        payload: true
    }
}
export function setHomeBannerData(data) {
    return {
        type: HOME_BANNER_DATA,
        payload: data
    }
}
// GET ALL PRODUCT REQUEST THUNK
export function getAllProduct() {
    return async dispatch => {

        dispatch({
            type: HOME_PRODUCT_REQUEST,
            payload: 'pending'
        })
        try {
            let response = await AxiosInstance.get('home-product.php')
            console.log(response, 'response from products')
            if (!response.data.error) {
                dispatch({
                    type: HOME_PRODUCT_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: HOME_PRODUCT_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log(e);
            dispatch({
                type: HOME_PRODUCT_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}
// GET SINGLE PRODUCT REQUEST THUNK
export function getSingleProduct(proid) {
    return async dispatch => {

        dispatch({
            type: PRODUCT_DETAIL_REQUEST,
            payload: 'pending'
        })
        try {
            let response = await AxiosInstance.post('single-product.php', { proid })
            console.log(response, 'response from single products')
            if (!response.data.error) {
                console.log('this is success')
                dispatch({
                    type: PRODUCT_DETAIL_SUCCESS,
                    payload: response
                })
            } else {
                console.log('this is failure')
                dispatch({
                    type: PRODUCT_DETAIL_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log('this is in catch')
            console.log(e);
            dispatch({
                type: PRODUCT_DETAIL_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}
// VIEW CART REQUEST THUNK
export function viewCart(data) {
    console.log("holalallalal",data)
    return async dispatch => {

        dispatch({
            type: VIEW_CART_REQUEST,
            payload: 'pending'
        })
        try {
            let response = await AxiosInstance.post('view-cart.php', data)
            console.log(response, 'response from products')
            response.msg ="Somehting"
            if (!response.data.error) {
                dispatch({
                    type: VIEW_CART_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: VIEW_CART_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log(e);
            dispatch({
                type: VIEW_CART_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}
// VIEW WISHLIST REQUEST THUNK
export function viewWishlist(data) {
    console.log("Im sending this to wishlist: ", data);
    return async dispatch => {

        dispatch({
            type: VIEW_WISHLIST_REQUEST,
            payload: 'pending'
        })
        try {
            let response = await AxiosInstance.post('view-wishlist.php', data)
            console.log(response, 'response from products')
            if (!response.data.error) {
                dispatch({
                    type: VIEW_WISHLIST_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: VIEW_WISHLIST_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log(e);
            dispatch({
                type: VIEW_WISHLIST_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}
// ADD IN CART REQUEST THUNK
export function addInCart() {
    return async dispatch => {

        dispatch({
            type: ADD_TO_CART_REQUEST,
            payload: 'pending'
        })
        try {
            let response = await AxiosInstance.get('home-product.php')
            console.log(response, 'response from products')
            if (!response.data.error) {
                dispatch({
                    type: ADD_TO_CART_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: ADD_TO_CART_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log(e);
            dispatch({
                type: ADD_TO_CART_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}

export function changeQuantity(data, index, quantity) {
    console.log(data);
    return async dispatch => {
        dispatch({
            type: QUANTITY_CHANGE,
            payload: 'pending'
        })
        try {
            let response = await AxiosInstance.post('addToCart.php', data)
            response.index = index;
            response.changedQuantity = quantity;
            console.log(response, 'response from change quantity');
            if (!response.data.error) {
                console.log('this is success')
                dispatch({
                    type: QUANTITY_CHANGE_SUCCESS,
                    payload: response
                })
            } else {
                console.log('this is failure')
                dispatch({
                    type: QUANTITY_CHANGE_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log('this is in catch')
            console.log(e);
            dispatch({
                type: QUANTITY_CHANGE_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}

// ADD IN WISHLIST REQUEST THUNK
export function addInWishlist(obj) {
    return async dispatch => {

        dispatch({
            type: ADD_TO_WISHLIST_REQUEST,
            payload: 'pending'
        })
        try {
            let response = await AxiosInstance.post('add-to-wishlist.php', obj)
            console.log(response, 'response from add to wishlist')
            if (!response.data.error) {
                console.log('this is success')
                dispatch({
                    type: ADD_TO_WISHLIST_SUCCESS,
                    payload: response
                })
            } else {
                console.log('this is failure')
                dispatch({
                    type: ADD_TO_WISHLIST_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log('this is in catch')
            console.log(e);
            dispatch({
                type: ADD_TO_WISHLIST_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}
// REMOVE IN CART REQUEST THUNK
export function removeInCart() {
    return async dispatch => {

        dispatch({
            type: REMOVE_FROM_CART_REQUEST,
            payload: 'pending'
        })
        try {
            let response = await AxiosInstance.get('home-product.php')
            console.log(response, 'response from products')
            if (!response.data.error) {
                dispatch({
                    type: REMOVE_FROM_CART_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: REMOVE_FROM_CART_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log(e);
            dispatch({
                type: REMOVE_FROM_CART_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}
// REMOVE IN WISHLIST REQUEST THUNK
export function removeInWishlist(obj) {
    return async dispatch => {

        dispatch({
            type: REMOVE_FROM_WISHLIST_REQUEST,
            payload: 'pending'
        })
        try {
            let response = await AxiosInstance.post('delete-wishlist.php', obj)
            console.log(response, 'response from products')
            if (!response.data.error) {
                dispatch({
                    type: REMOVE_FROM_WISHLIST_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: REMOVE_FROM_WISHLIST_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log(e);
            dispatch({
                type: REMOVE_FROM_WISHLIST_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}
// others
// export function addToCart(cartArray) {
//     //console.log("hjhjh")
//     return {
//         type: ADD_TO_CART,
//         payload: cartArray
//     }
// }

export function addToCart(data){
    return async dispatch => {
        console.log("addtocart in actions.js: ",data);
        dispatch({
            type: ADD_TO_CART,
            payload:  {data: {msg: 'pending'}}
        })
        try {
            let response = await AxiosInstance.post('addToCart.php', data);
            console.log(response, 'response from add to cart');
            if (!response.data.error) {
                console.log(response)
                dispatch({
                    type: ADD_TO_CART_SUCCESS,
                    payload: response
                })
            } else {
                console.log('this is failure')
                dispatch({
                    type: ADD_TO_CART_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log('this is in catch')
            console.log(e);
            dispatch({
                type: ADD_TO_CART_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}
export function removeFromCart(data, index) {
    return async dispatch => {
        console.log("removfromcart",data);
        dispatch({
            type: REMOVE_FROM_CART,
            payload:  {data: {msg: 'pending'}}
        })
        try {
            let response = await AxiosInstance.post('removeFromCart.php', data);
            console.log(response, 'response from remove from cart');
            if (!response.data.error) {
                console.log(response)
                response.index = index;
                dispatch({
                    type: REMOVE_FROM_CART_SUCCESS,
                    payload: response
                })
            } else {
                console.log('this is failure')
                dispatch({
                    type: REMOVE_FROM_CART_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log('this is in catch')
            console.log(e);
            dispatch({
                type: REMOVE_FROM_CART_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}
export function addToWishlist(data) {
    return async dispatch => {
        console.log("addto wishlist",data);
        dispatch({
            type: ADD_TO_WISHLIST,
            payload:  {data: {msg: 'pending'}}
        })
        try {
            let response = await AxiosInstance.post('add-to-wishlist.php', data);
            console.log(response, 'response from add to wishlist');
            if (!response.data.error) {
                console.log(response)
                //response.index = index;
                dispatch({
                    type: ADD_TO_WISHLIST_SUCCESS,
                    payload: response
                })
            } else {
                console.log('this is failure')
                dispatch({
                    type: ADD_TO_WISHLIST_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log('this is in catch')
            console.log(e);
            dispatch({
                type: ADD_TO_WISHLIST_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}
export function removeFromWishlist(data, index) {
    if(index!=undefined && index == null){
        //find index

    }
    else{
        return async dispatch => {
            console.log("removefromwishlist",data);
            dispatch({
                type: REMOVE_FROM_WISHLIST,
                payload:  {data: {msg: 'pending'}}
            })
            try {
                let response = await AxiosInstance.post('add-to-wishlist.php', data);
                console.log(response, 'response from wishlist remove');
                if (!response.data.error) {
                    console.log(response)
                    response.index = index;
                    dispatch({
                        type: REMOVE_FROM_WISHLIST_SUCCESS,
                        payload: response
                    })
                } else {
                    console.log('this is failure')
                    dispatch({
                        type: REMOVE_FROM_WISHLIST_FAILURE,
                        payload: response
                    })
                }
            } catch (e) {
                console.log('this is in catch')
                console.log(e);
                dispatch({
                    type: REMOVE_FROM_WISHLIST_FAILURE,
                    payload: { data: { msg: 'Something went wrong' } }
                })
            }
        }
    }
}

export function addCustomerAddress(data) {
    return async dispatch => {
        dispatch({
            type: ADD_CUSTOMER_ADDRESS_REQUEST,
            payload: 'pending'
        })
        try {
            let response = await AxiosInstance.post('add-customer-address.php', data)
            console.log(response, 'response from products')
            response.address = data.address;
            if (!response.data.error) {
                dispatch({
                    type: ADD_CUSTOMER_ADDRESS_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: ADD_CUSTOMER_ADDRESS_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log(e);
            dispatch({
                type: ADD_CUSTOMER_ADDRESS_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}

export function deleteCustomerAddress(data, index) {
    return async dispatch => {
        dispatch({
            type: DELETE_CUSTOMER_ADDRESS_REQUEST,
            payload: 'pending'
        })
        try {
            let response = await AxiosInstance.post('delete-customer-address.php', data)
            console.log(response, 'response from products')
            response.index =    index;
            if (!response.data.error) {
                dispatch({
                    type: DELETE_CUSTOMER_ADDRESS_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: DELETE_CUSTOMER_ADDRESS_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log(e);
            dispatch({
                type: DELETE_CUSTOMER_ADDRESS_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}

export function updateCustomerAddress(data, index) {
    return async dispatch => {
        dispatch({
            type: UPDATE_CUSTOMER_ADDRESS_LIST_REQUEST,
            payload: 'pending'
        })
        try {
            let response = await AxiosInstance.post('add-customer-address.php', data)
            console.log(response, 'response from products')
            // response.address = data.address;
            response.index = index;
            if (!response.data.error) {
                dispatch({
                    type: UPDATE_CUSTOMER_ADDRESS_LIST_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: UPDATE_CUSTOMER_ADDRESS_LIST_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log(e);
            dispatch({
                type: UPDATE_CUSTOMER_ADDRESS_LIST_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}

// export function selectAddress(address) {
//     return async dispatch => {
//         dispatch({
//             type: SELECT_ADDRESS,
//             payload: address
//         })
//     }
// }

export function getCustomerAddressList(token) {
    return async dispatch => {
        dispatch({
            type: GET_CUSTOMER_ADDRESS_LIST_REQUEST,
            payload: 'pending'
        })
        try {
            let response = await AxiosInstance.post('get-customer-addresses.php', token)
            console.log(response, 'response from products')
            if (!response.data.error) {
                dispatch({
                    type: GET_CUSTOMER_ADDRESS_LIST_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: GET_CUSTOMER_ADDRESS_LIST_FAILURE,
                    payload: response
                })
            }
        } catch (e) {
            console.log(e);
            dispatch({
                type: GET_CUSTOMER_ADDRESS_LIST_FAILURE,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}

export function sendProductInBuyNow(product) {
    return {
        type: BUY_NOW,
        payload: product
    }
}
export function resetAddress() {
    return {
        type: RESET_ADDRESS,
        payload: true
    }
}
export function search(key) {
    return async dispatch => {
        dispatch({
            type: SEARCH_REQUEST,
            payload: "pending"
        })
        try {
            const response = await AxiosInstance.post('search.php', key);
            console.log(response, 'response from search');
            if (!response.data.error) {
                response.data.typeKey = key.key
                dispatch({
                    type: SEARCH_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: SEARCH_FAILED,
                    payload: response
                })
            }

        } catch (e) {
            console.log(e)
            dispatch({
                type: SEARCH_FAILED,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}
export function getOrders(token) {
    return async dispatch => {
        dispatch({
            type: ORDERS_REQUEST,
            payload: "pending"
        })
        try {
            const response = await AxiosInstance.post('my-orders.php',  token);
            console.log(response, 'response from place order');
            if (!response.data.error) {
                dispatch({
                    type: ORDERS_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: ORDERS_FAILED,
                    payload: response
                })
            }

        } catch (e) {
            console.log(e)
            dispatch({
                type: ORDERS_FAILED,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}
export function placeOrder(params) {
    return async dispatch => {
        dispatch({
            type: PLACE_ORDER_REQUEST,
            payload: "pending"
        })
        try {
            const response = await AxiosInstance.post('checkout.php', params);
            console.log(response, 'response from place order');
            if (!response.data.error) {
                dispatch({
                    type: PLACE_ORDER_SUCCESS,
                    payload: response
                })
            } else {
                dispatch({
                    type: PLACE_ORDER_FAILED,
                    payload: response
                })
            }

        } catch (e) {
            console.log(e)
            dispatch({
                type: PLACE_ORDER_FAILED,
                payload: { data: { msg: 'Something went wrong' } }
            })
        }
    }
}
export function setUserToken(token) {
    return {
        type: SET_USER_TOKEN,
        payload: token
    }
}
export function addQuantity(payload) {
    return {
        type: QUANTITY_UP,
        payload
    }

    // return{
    //     type : ADD_TO_CART,
    //     payload: payload
    // }
}
export function removeQuantity(payload) {
    return {
        type: QUANTITY_DOWN,
        payload
    }
}
export function resetState() {
    return {
        type: RESET_STATUS,
        payload: true
    }
}
