import AxiosInstance from './axiosInstance';

export const signIn = async (
    email,
    pass
) => {
    try {
        const response = await AxiosInstance.post('login.php',{
            email, pass
        });
        console.log(response , 'in sign in')
        if (!response.data.error) {
            return response.data
        } else {
            return false
        }
    } catch (e) {
        console.log(e);
        return false
    }
}

export const signUp = async (name, phone, email, pass) => {
    try {
        const data = {name, phone, email , pass};
        debugger;
        const response = await AxiosInstance.post('signup.php',data);
        console.log(response , 'in signup');
        if (!response.data.error) {
            return response.data.token;
        } else {
            return false
        }
    } catch (e) {
        console.log(e)
        return false
    }
}
export const getBannerData = async () => {
    try {
        const response = await AxiosInstance.get('slider.php');
        console.log('in slider ', response)
        if (!response.data.error) {
            return response.data;
        } else {
            return false
        }
    } catch (e) {
        console.log(e)
        return false
    }
}
