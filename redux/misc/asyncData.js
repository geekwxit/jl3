import { AsyncStorage } from 'react-native';

export const saveDataInAsync = async (key, data) => {
    try {
        await AsyncStorage.setItem(key,data);
    }catch(e){
        console.log(e)
    }
}