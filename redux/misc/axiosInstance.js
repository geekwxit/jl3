import axios from 'axios';

export default Instance = axios.create({
    baseURL: 'http://cripcoin.co/jewellery/mobileApi/',
    timeout: 5000,
})