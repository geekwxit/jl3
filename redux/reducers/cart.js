import {
    ADD_TO_CART, UPDATE_QUANTITY_IN_CART, REMOVE_FROM_CART, ADD_TO_WISHLIST, REMOVE_FROM_WISHLIST, BUY_NOW,
    ORDERS_SUCCESS, RESET_STATUS, QUANTITY_UP, QUANTITY_DOWN, QUANTITY_CHANGE,QUANTITY_CHANGE_SUCCESS,
    QUANTITY_CHANGE_FAILURE,
ADD_TO_CART_FAILURE,ADD_TO_CART_SUCCESS,
    ADD_TO_WISHLIST_REQUEST, ADD_TO_WISHLIST_SUCCESS, ADD_TO_WISHLIST_FAILURE,
    VIEW_WISHLIST_REQUEST, VIEW_WISHLIST_SUCCESS, VIEW_WISHLIST_FAILURE,
    VIEW_CART_REQUEST, VIEW_CART_FAILURE, VIEW_CART_SUCCESS,
    REMOVE_FROM_CART_REQUEST, REMOVE_FROM_CART_FAILURE, REMOVE_FROM_CART_SUCCESS,
    REMOVE_FROM_WISHLIST_REQUEST, REMOVE_FROM_WISHLIST_FAILURE, REMOVE_FROM_WISHLIST_SUCCESS

} from '../actions/types';

const initialState = {
    isLoading: false,
    cartArray: [],
    cartCount: null,
    wishlistArray: [],
    wishlistCount: null,
    remove: false,
    buyNow: [],
    error: false, errorMsg: null,
    responseCode: null,
    lang: 'en',
}

export default function (state = initialState, { type, payload }) {
    // console.log( typeof state.cartArray)
    switch (type) {
        case VIEW_CART_REQUEST || VIEW_WISHLIST_REQUEST || ADD_TO_WISHLIST_REQUEST || REMOVE_FROM_CART_REQUEST || REMOVE_FROM_CART_REQUEST:
            return {
                ...state, isLoading: true, errorMsg: null, error: false,
            }

        case VIEW_CART_FAILURE || VIEW_WISHLIST_FAILURE || ADD_TO_WISHLIST_FAILURE || REMOVE_FROM_CART_FAILURE:
            return {
                ...state, isLoading: false, errorMsg: payload.data.msg, error: payload.data.error,
            }
        case VIEW_WISHLIST_SUCCESS:
            console.log("Viewing the wishlist: ", payload);
            //debugger;
            return {
                ...state, isLoading: false, errorMsg: payload.data.msg, error: payload.data.error,
                wishlistCount: payload.data.total_item, wishlistArray: payload.data.wishlist_data ? [...payload.data.wishlist_data] : []
            }
        case VIEW_CART_SUCCESS:
            return {
                ...state, isLoading: false, errorMsg: payload.data.msg, error: payload.data.error, cartCount: payload.data.total_item, cartArray: payload.data.cart_data?payload.data.cart_data: []
            }
        case ADD_TO_WISHLIST_SUCCESS:
            console.log("Wishlist payload: ", payload);
            return {
                ...state, isLoading: false, errorMsg: payload.data.msg, error: payload.data.error,
                wishlistCount: payload.data.total_item, wishlistArray: payload.data.wishlist_data ? payload.data.wishlist_data : []
            }
            //return {...state}
        case REMOVE_FROM_CART_SUCCESS:
            cartArray2 = Object.assign([], state.cartArray);
            payload.index!=null?cartArray2.splice(payload.index, 1):null
            return {
                ...state, isLoading: false, errorMsg: payload.data.msg, error: payload.data.error,
                cartArray: cartArray2, cartCount: cartArray2.length
            }
        case REMOVE_FROM_WISHLIST_SUCCESS:
            console.log("REMOVE FROM WISHLIST SUCCESS");
            if(payload.index!=null){
                wishlistArray2 = Object.assign([], state.wishlistArray);
                wishlistArray2.splice([payload.index],1);
                console.log("WISHLIST ARRAY: ", wishlistArray2)
                return {
                    ...state, isLoading: false, errorMsg: payload.data.msg, error: payload.data.error, remove: true,
                    wishlistArray: wishlistArray2?wishlistArray2:[],wishlistCount: wishlistArray2.length
                }
            }
            else{
                return {...state}
            }
        case ADD_TO_CART:
            if(payload.data){
                console.log("entered here")
            return {
                ...state, isLoading: false, errorMsg: payload.data.msg, error: payload.data.error,
                //cartCount: payload.data.total_item, cartArray: payload.data.wishlist_data ? [...payload.data.wishlist_data] : []
            }};
        case ADD_TO_CART_SUCCESS:
            console.log("success cart entered here:", payload);
            cartArray = Object.assign([], payload.data.productDetails?[...state.cartArray, payload.data.productDetails]:[]);
            return {
                ...state, isLoading: false, errorMsg: payload.data.msg, error: payload.data.error,
                cartCount: payload.data.total_item, cartArray: cartArray,
            }
        case ADD_TO_CART_FAILURE:
            if(payload.data){
                console.log("failure cart entered here")
            return {
                ...state, isLoading: false, errorMsg: payload.data.msg, error: payload.data.error,
                cartCount: payload.data.total_item, cartArray: payload.data.wishlist_data ? [...payload.data.wishlist_data] : []
            }}
        case REMOVE_FROM_CART:
            return {
                ...state, isLoading: false, error: false,
            }
        case ADD_TO_WISHLIST:
            return {
                ...state, isLoading: false,error: false,
            }
        case REMOVE_FROM_WISHLIST:
            return {
                ...state, isLoading: false, error: false,
            }
        case BUY_NOW:
            return {
                ...state, buyNow: payload
            }
        case QUANTITY_UP:
            return {
                ...state, isLoading: false, errorMsg: payload.data.msg, error: payload.data.error,
                cartCount: payload.data.total_item, cartArray: payload.data.wishlist_data ? [...payload.data.wishlist_data] : []
            }
        case QUANTITY_DOWN:
            return {
                ...state, isLoading: false, errorMsg: payload.data.msg, error: payload.data.error,
                cartCount: payload.data.total_item, cartArray: payload.data.wishlist_data ? [...payload.data.wishlist_data] : []
            }

        case QUANTITY_CHANGE:
            return {
                ...state, isLoading: false,
                // errorMsg: payload.data.msg, error: payload.data.error,
                // cartCount: payload.data.total_item, cartArray: payload.data.wishlist_data ? [...payload.data.wishlist_data] : []
            }
        case QUANTITY_CHANGE_SUCCESS:
            //console.log("thisis state in quantitiy change success",{...state});
            //cartArray2 = [...state.cartArray.slice(0, payload.index), payload.data.productDetails, ...state.cartArray.slice(0, payload.index)]

            console.log('fasf');
            cartArray2 = Object.assign([], [...state.cartArray, ]);
            cartArray2[payload.index] = payload.data.productDetails;
            console.log("some cause for user", payload.changedQuantity);
            //console.log("changedquanti: ", parseInt(payload.changedQuantity) , "cartar: ", parseInt(cartArray2[payload.index].c_quantity));
            //cartArray2[payload.index].c_quantity = parseInt(payload.changedQuantity) + parseInt(cartArray2[payload.index].c_quantity);
            //console.log("PAYLOAD:" , payload);
            //console.log("CARTARRAYYYAYYAAYYA: ", cartArray2 );
            // state.cartArray[payload.index].c_quantity = parseInt(payload.changedQuantity)+parseInt(state.cartArray[payload.index].c_quantity);
            //console.log("state in quantiyt: ", state)
            return {
                ...state, //isLoading: false, errorMsg: payload.data.msg, error: payload.data.error,
                cartArray: cartArray2
            }
        case QUANTITY_CHANGE_FAILURE:
            // return {
            //     ...state, isLoading: false, errorMsg: payload.data.msg, error: payload.data.error,
            //     cartCount: payload.data.total_item, cartArray: payload.data.wishlist_data ? [...payload.data.wishlist_data] : []
            // }
            return {
                ...state
            }

        case RESET_STATUS:
            return {
                ...state, cartArray: []
            }

        default:
            return state
    }
}
