import {
    HOME_PRODUCT_REQUEST, HOME_PRODUCT_SUCCESS, HOME_PRODUCT_FAILURE,
    SEARCH_REQUEST, SEARCH_FAILED, SEARCH_SUCCESS,
    ORDERS_REQUEST, ORDERS_FAILED, ORDERS_SUCCESS,
    PRODUCT_DETAIL_REQUEST, PRODUCT_DETAIL_SUCCESS, PRODUCT_DETAIL_FAILURE,
    ADD_TO_WISHLIST_REQUEST, ADD_TO_WISHLIST_SUCCESS, ADD_TO_WISHLIST_FAILURE

} from '../actions/types';

const initialState = {
    isLoading: false,
    allProducts: [],
    singleProduct: {},
    relatedProduct : [],
    productGallery:[],
    searchData: [],
    wishlist_total_item : null,
    ordersData: [],
    typeKey: null,
    error: false, errorMsg: null,
    responseCode: null,
    lang: 'en',
}

export default function (state = initialState, { type, payload }) {
    // console.log(payload)
    switch (type) {
        case HOME_PRODUCT_REQUEST || SEARCH_REQUEST || ORDERS_REQUEST || PRODUCT_DETAIL_REQUEST:
            return {
                ...state, isLoading: true, error: false, errorMsg: null,
                ordersData: [], singleProduct: {}
            }
        case HOME_PRODUCT_FAILURE || SEARCH_FAILED || ORDERS_FAILED || PRODUCT_DETAIL_FAILURE:
            return {
                ...state, isLoading: false, error: true,
                ordersData: [], singleProduct: {},relatedProduct:[],productGallery:[],
                errorMsg: payload.data.msg, success: false,
            }
        case HOME_PRODUCT_SUCCESS:
            return {
                ...state, isLoading: false,
                allProducts: payload.data.all_product, error: false, errorMsg: payload.data.msg,
            }
        case SEARCH_SUCCESS:
            return {
                ...state, isLoading: false, typeKey: payload.data.typeKey,
                searchData: payload.data.product, error: false, errorMsg: payload.data.msg
            }
        case ORDERS_FAILED:
            return {
                ...state, isLoading: false, error: true, ordersData: [],
                errorMsg: payload.data.msg, success: false,
            }
        case ORDERS_SUCCESS:
            return {
                ...state, isLoading: false,
                ordersData: payload.data.orders?payload.data.orders:[], error: false, errorMsg: payload.data.msg,
            }
        case PRODUCT_DETAIL_SUCCESS:
            return{
                ...state, isLoading:false,
                singleProduct:payload.data.single_product ,
                productGallery:payload.data.gallery,
                relatedProduct:payload.data.related, error:false
            }
        default:
            return state
    }
}
