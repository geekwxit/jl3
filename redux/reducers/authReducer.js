import {
    FETCH_PRODUCTS,
    SIGN_IN,
    SET_USER_TOKEN,
    LOGOUT,
    SET_GUEST_ID,
    SET_USER_DETAILS,
    SET_USER_DETAILS_SUCCESS, SET_USER_DETAILS_FAILURE,
    GET_USER_DETAILS,
    GET_USER_DETAILS_SUCCESS, GET_USER_DETAILS_FAILURE
} from '../actions/types';

const initialState = {
    signIn: false,
    token: null,
    name:null,
    email:null,
    phone:null,
    guest_id: null,
}

export default function (state = initialState, action) {
    // console.log('in reducer ',action)
    switch (action.type) {
        case GET_USER_DETAILS:
            return {
                ...state
            }
        case GET_USER_DETAILS_SUCCESS:
            payload = action.payload;
            console.log(payload);
            //debugger;
            return {
                ...state, name: payload.data.userData.c_name, phone: payload.data.userData.c_phone, email: payload.data.userData.c_email
            }
        case GET_USER_DETAILS_FAILURE:
            return {
                ...state
            }
        case SET_USER_DETAILS:
            return {
                ...state
            }
        case SET_USER_DETAILS_SUCCESS:
            detail = action.payload.data.success?action.payload.details:null;
            detail?alert(action.payload.data.msg):null;
            return {
                ...state, name: detail?detail.name:state.name, phone:  detail?detail.phone:state.phone
            }
        case SET_USER_DETAILS_FAILURE:
            return {
                ...state
            }
        case SIGN_IN:
            return {
                ...state,
                token: action.payload.token,
                email:action.payload.email,
                name: action.payload.name,
                phone:action.payload.phone,
                signIn: true
            }
        case SET_USER_TOKEN:
            return {
                ...state,
                token: action.payload
            }
        case LOGOUT:
            return {
                ...state,
                name: null,
                email: null,phone: null,signIn: false,
                token: null
            }
        case SET_GUEST_ID:
            console.log("this action is being called", action);
            return {
                ...state,
                guest_id: action.payload
            }
        default:
            return state
    }
}
