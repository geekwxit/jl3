import {
     RESET_ADDRESS,
    PLACE_ORDER_REQUEST, PLACE_ORDER_SUCCESS, PLACE_ORDER_FAILED,
    RESET_STATUS,
    GET_CUSTOMER_ADDRESS_LIST_FAILURE, GET_CUSTOMER_ADDRESS_LIST_SUCCESS, GET_CUSTOMER_ADDRESS_LIST_REQUEST,

    ADD_CUSTOMER_ADDRESS_REQUEST, ADD_CUSTOMER_ADDRESS_SUCCESS, ADD_CUSTOMER_ADDRESS_FAILURE,

    DELETE_CUSTOMER_ADDRESS_REQUEST, DELETE_CUSTOMER_ADDRESS_SUCCESS, DELETE_CUSTOMER_ADDRESS_FAILURE,

    UPDATE_CUSTOMER_ADDRESS_LIST_FAILURE,
    UPDATE_CUSTOMER_ADDRESS_LIST_SUCCESS,
    UPDATE_CUSTOMER_ADDRESS_LIST_REQUEST
} from '../actions/types';

//SET_CUSTOMER_ADDRESS_REQUEST, SET_CUSTOMER_ADDRESS_FAILURE, SET_CUSTOMER_ADDRESS_SUCCESS,

const initialState = {
    isLoading: false,
    customerAddress: [],
    addressId: null,
    order_id_Array: [],
    current_order_id: null,
    orderSuccess: false,
    success: false,
    error: false, errorMsg: null,
    responseCode: null,
    lang: 'en',
}

export default function (state = initialState, { type, payload }) {
    // console.log(payload)
    switch (type) {
        // case SET_CUSTOMER_ADDRESS_REQUEST || PLACE_ORDER_REQUEST:
        //     return { ...state, isLoading: true, error: false, errorMsg: null, success: false, orderSuccess: false }
        // case SET_CUSTOMER_ADDRESS_FAILURE || PLACE_ORDER_FAILED:
        //     return { ...state, isLoading: false, error: true, errorMsg: payload.data.msg,
        //         success: false, orderSuccess: false }
        // case SET_CUSTOMER_ADDRESS_SUCCESS:
        //     return {
        //         ...state, isLoading: false, success: true, addressId: payload.data.address_id,
        //         customerAddress: payload.data.address, error: false, errorMsg: payload.data.msg
        //     }
        case PLACE_ORDER_SUCCESS:
            return {
                ...state, isLoading: false, success: true,
                order_id_Array: [...state.order_id_Array, payload.data.order_id],
                orderSuccess: true,
                current_order_id: payload.data.order_id,
                error: false, errorMsg: payload.data.msg
            }
        case RESET_ADDRESS:
            return { ...state, customerAddress: [], success: false }
        case RESET_STATUS:
            return {...state, current_order_id : null , orderSuccess:false ,error:false, errorMsg:null }


        case GET_CUSTOMER_ADDRESS_LIST_REQUEST:
            return {...state}
        case GET_CUSTOMER_ADDRESS_LIST_SUCCESS:
            return { ...state, customerAddress: payload.data.addresses?payload.data.addresses: [], success: false}
        case GET_CUSTOMER_ADDRESS_LIST_FAILURE:
            return {...state, error:payload.error, errorMsg:payload.msg}


        case ADD_CUSTOMER_ADDRESS_REQUEST:
            return {...state}
        case ADD_CUSTOMER_ADDRESS_SUCCESS:
            var address = payload.data.address;
            var newAddress = [...state.customerAddress];
            newAddress.push(address);
            //return { ...state, customerAddress: payload.data.address?payload.data.address: [], success: false}
            //success: false
            console.log(payload);
            console.log(state);
            console.log(state.customerAddress);
            console.log(payload.data);
            return { ...state, customerAddress: payload.data.addressStatus?newAddress:state.customerAddress}
        case ADD_CUSTOMER_ADDRESS_FAILURE:
            return {...state, error:payload.error, errorMsg:payload.msg}


        case DELETE_CUSTOMER_ADDRESS_REQUEST:
            return {...state}
        case DELETE_CUSTOMER_ADDRESS_SUCCESS:
            var address = Object.assign([], [...state.customerAddress]);
            address.splice(payload.index, 1);
            return { ...state, customerAddress: payload.data.addressStatus?address:state.customerAddress}
        case DELETE_CUSTOMER_ADDRESS_FAILURE:
            return {...state, error:payload.error, errorMsg:payload.msg}


        case UPDATE_CUSTOMER_ADDRESS_LIST_REQUEST:
            return {...state}
        case UPDATE_CUSTOMER_ADDRESS_LIST_SUCCESS:
            var address = Object.assign([], [...state.customerAddress]);
            address[payload.index] = payload.data.address;
            //var address = state.customerAddress[payload.index];
            //{address : payload.data.address};
            // var index = payload.index;
            // var address = payload.data.address;
            // address[payload.index]
            // console.log(payload);
            // debugger;
            // return { ...state,
            //     customerAddress: [...state.customerAddress, eval(index): {address}], success: false}
            return {
                ...state, customerAddress: payload.data.addressStatus?address:state.customerAddress
            }
        case UPDATE_CUSTOMER_ADDRESS_LIST_FAILURE:
            return {...state, error:payload.error, errorMsg:payload.msg}

        default:
            return state
    }
}
