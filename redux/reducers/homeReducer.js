import {
    FETCH_PRODUCTS,
    HOME_BANNER_DATA,
    CHANGE_APP_LANGUAGE,
    ORDERS_REQUEST,
    ORDERS_SUCCESS,
    ORDERS_FAILED,
    VIEW_CART_FAILURE,
    VIEW_WISHLIST_FAILURE,
    ADD_TO_WISHLIST_FAILURE,
    REMOVE_FROM_CART_FAILURE
} from '../actions/types';
import {Language} from "../../screens/Language";

const initialState = {
    bannerData: [],
    app_language: Language.en,
}

export default function (state = initialState, action) {
    switch (action.type) {
        case HOME_BANNER_DATA:
            return {
                ...state,
                bannerData: action.payload
            }
        case CHANGE_APP_LANGUAGE:
            return {
                ...state, app_language: action.payload
            }
        default:
            return state
    }
}
