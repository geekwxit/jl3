import {combineReducers} from 'redux';

// imported reducers

import homeReducer from './homeReducer';
import authReducer from './authReducer';
import productReducer from './products';
import cartReducer from './cart';
import customerReducer from './customerReducer';

export default combineReducers({
    homeReducer,
    authReducer,
    productReducer,
    cartReducer,
    customerReducer
});
