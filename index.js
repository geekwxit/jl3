/**
 * @format
 */

//To capture network calls by App
GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;

import {AppRegistry, AsyncStorage} from 'react-native';
// import HomeScreen from './screens/HomeScreen'
import App from './App';
import {name as appName} from './app.json';



AppRegistry.registerComponent(appName, () => App);
