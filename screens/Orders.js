import React, { Component } from "react";
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";


import Loader from '../screens/components/Loader';
import { addToCart, addToWishlist, removeFromCart, getOrders } from '../redux/actions/actions';

import { connect } from 'react-redux';
const { height, width } = Dimensions.get('window');

class Orders extends Component {

    static navigationOptions = {
        title: "My Orders"
    }

    constructor(props) {
        super(props)
        this.state = {
            ordersArray: []
        }

        /**This needs to be uncommented when live **/
        if (this.props.token){
            this.props.getOrders({user_id: this.props.token})
            // this.props.getOrders({user_id: 'rEa6dTY5ugAPZ83cSDnM'});
        }
    }


    render() {
        const { isLoading, ordersData } = this.props
        const { ordersArray } = this.state
        console.log(isLoading, ordersData)

        if (isLoading) {
            return (
                <Loader visible={isLoading} />
            )
        } else {
            return (
                <View style={styles.container}>

                    {/* <Text style={{ fontSize: 22, color: '#333', textAlign: 'center', marginVertical: 35 }}>My Orders</Text> */}

                    {this.props.ordersData.length > 0 ? <View style={{flex: 1}}>
                        <ScrollView style={{ flex:1, marginTop: 10 }}>
                            {this.props.ordersData.map((data, key) => (
                                <View key={key} style={{ paddingBottom: 10, flex: 1, height: 90, backgroundColor: '#fff', alignItems: 'center', borderColor: '#bbb', borderBottomWidth: 0.5, marginBottom: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ flex: 9, paddingLeft: 10, paddingRight: 10, justifyContent: 'space-evenly' }}>
                                        <View>
                                            <Text style={{ fontSize: 18, color: '#333', }} numberOfLines={2} >
                                                <Text style={{ fontWeight: 'bold' }}>Order ID : </Text>{data.ord_key}</Text>
                                        </View>
                                        <View style={{ marginTop: 5 }}>
                                            <Text style={{ fontSize: 16, color: '#333', }}>Ordered on <Text style={{ fontSize: 14, color: '#777', }}>{data.ord_date}</Text> </Text>
                                        </View>
                                        <View style={{ marginTop: 5 }}>
                                        </View>
                                    </View>
                                    <View style={{ flex: 1.5, justifyContent: 'center', alignItems: 'center' }}>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderDetails', { orderDetails: data })}>
                                            <Icon name={'ios-arrow-forward'} size={20} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            ))}

 <View> 

                            
                            
                            <View style={{backgroundColor: '#ffffff', elevation: 2, paddingTop: 0, borderBottomWidth: 0.5, borderColor: '#ccc',marginHorizontal:10,marginBottom:10}}>
                                <View style={{ flexDirection: 'row', height: width /3-20, paddingHorizontal: 0}}>

                                    <View style={{ width:'35%',}}>
                                        <Image source={require('../images/sl1.jpg')} style={{ flex: 1, height: null, width: null, resizeMode: 'cover', }} />
                                    </View>

                                    <View style={{ width:'65%',paddingLeft: 10,}}>

                                        <View style={{ flexDirection: 'row',justifyContent:'space-between',marginTop:7}}>
                                            <View style={{ width: '80%', }}>
                                                <Text numberOfLines={1} style={{ fontSize: 16, color: '#333', paddingRight: 10 }}>Product Name Here</Text>
                                            </View> 
                                        </View> 
                                        
                                        <View style={{marginTop:10}}> 
                                        <Text style={{ fontSize: 17, color: '#555', fontWeight: '500' }}>$500</Text>
                                        </View>  

                                        <View style={{marginTop:12}}> 
                                        <Text style={{ fontSize: 17, color: '#777', fontWeight: '400' }}>Quantity : 1</Text>
                                        </View> 
                                    </View> 
                                </View> 
                            </View>


                            <View style={{backgroundColor: '#ffffff', elevation: 2, paddingTop: 0, borderBottomWidth: 0.5, borderColor: '#ccc',marginHorizontal:10,marginBottom:10}}>
                                <View style={{ flexDirection: 'row', height: width /3-20, paddingHorizontal: 0}}>

                                    <View style={{ width:'35%',}}>
                                        <Image source={require('../images/sl2.jpg')} style={{ flex: 1, height: null, width: null, resizeMode: 'cover', }} />
                                    </View>

                                    <View style={{ width:'65%',paddingLeft: 10,}}>

                                        <View style={{ flexDirection: 'row',justifyContent:'space-between',marginTop:7}}>
                                            <View style={{ width: '80%', }}>
                                                <Text numberOfLines={1} style={{ fontSize: 16, color: '#333', paddingRight: 10 }}>Product Name Here</Text>
                                            </View> 
                                        </View> 
                                        
                                        <View style={{marginTop:10}}> 
                                        <Text style={{ fontSize: 17, color: '#555', fontWeight: '500' }}>$500</Text>
                                        </View>  

                                        <View style={{marginTop:12}}> 
                                        <Text style={{ fontSize: 17, color: '#777', fontWeight: '400' }}>Quantity : 1</Text>
                                        </View> 
                                    </View> 
                                </View> 
                            </View>

                            <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:10}}> 
                            <View style={{marginVertical:10}}>
                                    <Text style={{ fontSize: 15, color: '#555', fontWeight: '400' }}>Status</Text>
                                    <Text style={{ fontSize: 17, color: '#222', fontWeight: '500',textTransform:'uppercase',color:'orange' }}>IN TRANSIT</Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 15, color: '#555', fontWeight: '400' }}>Estimated Delivery</Text>
                                    <Text style={{ fontSize: 17, color: '#222', fontWeight: '500',textAlign:'right' }}>20 Sep, 2017</Text>
                                </View>
                            </View>


                        <View style={{borderWidth:0.5,borderColor:'#fff,',marginVertical:20}}/>
                            

                            <View style={{backgroundColor: '#ffffff', elevation: 2, paddingTop: 0, borderBottomWidth: 0.5, borderColor: '#ccc',marginHorizontal:10,marginBottom:10}}>
                                <View style={{ flexDirection: 'row', height: width /3-20, paddingHorizontal: 0}}>

                                    <View style={{ width:'35%',}}>
                                        <Image source={require('../images/sl3.jpg')} style={{ flex: 1, height: null, width: null, resizeMode: 'cover', }} />
                                    </View>

                                    <View style={{ width:'65%',paddingLeft: 10,}}>

                                        <View style={{ flexDirection: 'row',justifyContent:'space-between',marginTop:7}}>
                                            <View style={{ width: '80%', }}>
                                                <Text numberOfLines={1} style={{ fontSize: 16, color: '#333', paddingRight: 10 }}>Product Name Here</Text>
                                            </View> 
                                        </View> 
                                        
                                        <View style={{marginTop:10}}> 
                                        <Text style={{ fontSize: 17, color: '#555', fontWeight: '500' }}>$500</Text>
                                        </View>  

                                        <View style={{marginTop:12}}> 
                                        <Text style={{ fontSize: 17, color: '#777', fontWeight: '400' }}>Quantity : 1</Text>
                                        </View> 
                                    </View> 
                                </View> 
                            </View>



                            <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:10}}> 
                                <View style={{marginVertical:10}}>
                                    <Text style={{ fontSize: 15, color: '#555', fontWeight: '400' }}>Status</Text>
                                    <Text style={{ fontSize: 17, color: '#222', fontWeight: '500',textTransform:'uppercase',color:'green' }}>Completed</Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 15, color: '#555', fontWeight: '400' }}>Estimated Delivery</Text>
                                    <Text style={{ fontSize: 17, color: '#222', fontWeight: '500',textAlign:'right' }}>20 Sep, 2017</Text>
                                </View>
                            </View>
</View>


                        </ScrollView>
                    </View>
                        : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Text
                                style={{
                                    fontSize: 20, fontWeight: 'bold',
                                }}>Your orders are empty</Text>
                        </View>
                    }



                   


                </View>
            );
        }
    }
}
const mapStateToProps = (state) => ({
    isLoading: state.productReducer.isLoading,
    allProducts: state.productReducer.allProducts,
    cartArray: state.cartReducer.cartArray,
    customerAddress: state.customerReducer.customerAddress,
    wishlistArray: state.cartReducer.wishlistArray,
    error: state.productReducer.error,
    errorMsg: state.productReducer.errorMsg,
    success: state.customerReducer.success,
    token: state.authReducer.token,
    ordersData: state.productReducer.ordersData
});
const mapDispatchToProps = {
    addToCart, addToWishlist, removeFromCart, getOrders
}
export default connect(mapStateToProps, mapDispatchToProps)(Orders);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f7f7f7',
    }
});
