import React, { Component } from "react";
import {View, Text, StyleSheet, SafeAreaView, TextInput, Dimensions,Modal,Image, ScrollView, TouchableOpacity, ActivityIndicator, FlatList, RefreshControl
} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
// custome Loader
import Loader from '../screens/components/Loader';
// connect for redux
import { connect } from 'react-redux';
// actions imported
import { setHomeBannerData, getAllProduct, getUserDetails } from '../redux/actions/actions';
// apiRequest
import { getBannerData } from '../redux/misc/apiRequest';
// Data storing in async
import { saveDataInAsync } from '../redux/misc/asyncData';
import ImageSlider from 'react-native-image-slider';
import axios from 'axios';


const { height, width } = Dimensions.get('window');

const baseUrl = "http://cripcoin.co/jewellery/mobileApi/images/"

class HomeScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        return {
            headerRight: (
                <View style={{ justifyContent: 'center' }}>
                    {/* <TouchableOpacity onPress={() => { navigation.state.params.handleSave() }}>
                        <View style={{ paddingHorizontal: 10 }}>
                            <Icon name="md-cart" size={30} style={{ color: '#fff' }} />
                        </View>
                    </TouchableOpacity>
                    {params ?
                        (<View style={{ position: 'absolute', top: 0, marginLeft: 30, marginTop: 7, zIndex: 100 }}>
                            {params.productOnCart &&
                                <View style={{
                                    textAlign: 'center',
                                    justifyContent: 'center',
                                    position: 'absolute', backgroundColor: 'red', width: 15, height: 15, borderRadius: 7.5
                                }}>
                                    <Text style={{ color: 'white', fontSize: 8, paddingLeft: 4 }}>{navigation.state.params.productOnCart}</Text>
                                </View>
                            }
                        </View>)
                        : (<View />)} */}
                    <TouchableOpacity onPress={() => { navigation.navigate('Search') }}>
                        <View style={{ paddingHorizontal: 10 }}>
                            <Icon name="search" size={30} color="#fff" style={{fontWeight:'100'}} />
                        </View>
                    </TouchableOpacity>
                </View>
            ),
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.cartArray !== prevState.cartArray) {
            // nextProps.navigation.setParams({
            //     productOnCart: nextProps.cartArray.length
            // })
            return {
                cartArray: nextProps.cartArray
            }
        }
        if (nextProps.error !== prevState.error) {
            alert(this.props.language.network)
            return {
                isLoading: false,
                error: nextProps.error
            }
        }


        return null
    }


    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            bannerData: [],
            cartArray: [],
            cartCount: null,
            images: [],
            b_images: [],
            allProduct: [],
            guest_id: '',
            error: false,
            refreshing: false
        }

        if (this.props.cartArray.length !== 0) {
            this.props.navigation.setParams({
                productOnCart: this.props.cartArray.length
            })
        }
    }




    componentDidMount() {
        this.props.navigation.setParams({ handleSave: this._moveToCartPage });
        console.log(this.props.navigation)

        var guest_id = this.props.guest_id;
        if (this.props.navigation.state.params) {
            guest_id = this.props.navigation.state.params.guest_id;
        }
        this.setState({ guest_id: guest_id });
        console.log('guest_id in home screen', guest_id)

        const url = 'http://cripcoin.co/jewellery/mobileApi/slider.php';


        // fetch(url, {
        //     //     method: 'POST',
        //     //     headers: { 'Accept': 'application/json', 'Content-Type': 'application/json', },
        //     // })
        axios.get(url)
            .then((response) => response.data)
            .then((responseData) => {

                for (var i = 0; i < responseData.images.length; i++) {
                    const copied = [...this.state.images]
                    copied[i] = responseData.images[i].m_img
                    this.setState({ images: copied })
                }
                for (var i = 0; i < responseData.banner.length; i++) {
                    const copied = [...this.state.b_images]
                    copied[i] = responseData.banner[i].b_image
                    this.setState({ b_images: copied })
                }
            })
            .catch((error) => {
                console.log("Error");
            });
        this.props.getAllProduct();
        // component Focusing code
        //----------
        // this.focusListener = this.props.navigation.addListener('didFocus', () => {

        // });
    }

    // componentWillUnmount() {
    //     this.focusListener.remove()
    // }

    _moveToCartPage = () => {
        console.log('guest_id in moveToCartPage', this.state.guest_id)
        this.props.navigation.navigate('Cart', {
            guest_id: this.state.guest_id,
            // user_id: this.state.user_id,
        });
    }

    allProduct() {
        this.props.navigation.navigate('Shop');
    }
    _product(pro_id, name) {
        this.props.navigation.navigate('Product', {
            pro_id: pro_id,
            guest_id: this.state.guest_id,
            pro_name: name
        })
    }

    onRefresh = () => {
        this.setState({ refreshing: true })
        this.props.getAllProduct()
        this.setState({ refreshing: false })
    }
    render() {
        const { allProduct, refreshing } = this.state
        const { bannerData, isLoading, allProducts } = this.props;
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView showsHorizontalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={this.onRefresh}
                        />
                    }>
                    <View style={{ marginTop: 10, alignItems: 'center' }}>
                    </View>
                    <View style={{ marginLeft: 10, width: width - 20, height: width / 2 - 40, }}>
                        <ImageSlider
                            autoPlayWithInterval={3000}
                            images={this.state.images}
                        />
                    </View>

                    <View style={{width:width-20,alignItems:'center',alignSelf:'center'}}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                            <View style={{ width: width / 3-6, height: width / 3.5,borderRightWidth:1,borderRightColor:'#fff' }}>
                                <Image source={{ uri: this.state.b_images[0] }} style={styles.promoImg} />
                            </View>
                            <View style={{ width: width / 3-6, height: width / 3.5, backgroundColor: '#081012', justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', }}>
                                    <Text style={{ color: '#fff', textAlign: 'center', fontSize: 16, fontWeight: '500', marginBottom: 10 }}>{this.props.language.midBannerText1}</Text>
                                    <Text style={styles.btn}>{this.props.language.viewAllButton}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: width / 3-6, height: width / 3.5,borderLeftWidth:1,borderLeftColor:'#fff' }}>
                                <Image source={{ uri: this.state.b_images[1] }} style={styles.promoImg} />
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between',marginTop:1 }}>
                            <View style={{borderRightWidth:1,borderRightColor:'#fff', width: width / 3-6, height: width / 3.5, backgroundColor: '#081012', justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', }}>
                                    <Text style={{ color: '#fff', textAlign: 'center', fontSize: 16, fontWeight: '500', marginBottom: 10 }}>{this.props.language.midBannerText2}</Text>
                                    <Text style={styles.btn}>{this.props.language.viewAllButton}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: width / 3-6, height: width / 3.5 }}>
                                <Image source={{ uri: this.state.b_images[2] }} style={styles.promoImg} />
                            </View>
                            <View style={{borderLeftWidth:1,borderLeftColor:'#fff', width: width / 3-6, height: width / 3.5, backgroundColor: '#081012', justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', }}>
                                    <Text style={{ color: '#fff', textAlign: 'center', fontSize: 16, fontWeight: '500', marginBottom: 10 }}>{this.props.language.midBannerText3}</Text>
                                    <Text style={styles.btn}>{this.props.language.viewAllButton}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    <View style={{ marginTop: 15, paddingHorizontal: 10, }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 15 }}>
                            <View><Text style={styles.hdng}>{this.props.language.rareCollection}</Text></View>
                            <View>
                                <TouchableOpacity
                                    onPress={() => this.allProduct()}><Text style={{ color: '#333', fontSize: 18, fontWeight: '500' }}>{this.props.language.viewAllButton} <Icon name="angle-double-right" size={18} style={{ color: '#333' }}  /></Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ marginTop: 0, marginBottom: 10 }}>
                            <ScrollView>
                                <FlatList
                                    data={allProducts}
                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                    keyExtractor={(item, index) => index.toString()}
                                    renderItem={({ item }) =>
                                    <View style={{ flexDirection: 'row', backgroundColor: 'white',}}>
                                            <TouchableOpacity onPress={() => this._product(item.pro_id, item.pro_title)}
                                                style={{flex:1,width: width/3,marginRight: 10,borderWidth: 0.5,borderColor: '#ddd'}}>
                                                <View style={{borderBottomWidth: 0.5,height: width/3, borderBottomColor: '#ddd',flex:1}}>
                                                        <Image source={{ uri: 'http://cripcoin.co/jewellery/images/' + item.featured_image }}
                                                            style={{flex:1,width: null,height: null,resizeMode: 'cover',}} />
                                                </View>
                                                <View style={{alignItems: 'center',marginTop: 7,}}>
                                                        <Text numberOfLines={1} style={{color: '#000',fontSize: 16,paddingHorizontal: 10}}>{item.pro_title} </Text>
                                                </View>
                                                <View style={{alignItems: 'center',marginVertical:8}}>
                                                        <Text numberOfLines={1} style={{color: '#000',fontSize: 16,fontWeight:'500',paddingHorizontal: 10}}>${item.pro_final_price} </Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    }
                                />
                            </ScrollView>
                        </View>

                    </View>
                    <Loader visible={isLoading} />
                </ScrollView>
            </SafeAreaView>
        );
    }

}

const mapStateToProps = (state) => ({
    isLoading: state.productReducer.isLoading,
    allProducts: state.productReducer.allProducts,
    cartArray: state.cartReducer.cartArray,
    error: state.productReducer.error,
    errorMsg: state.productReducer.errorMsg,
    guest_id: state.authReducer.guest_id,
    language: state.homeReducer.app_language.screens.HomeScreen,
});
const mapDispatchToProps = {
    getAllProduct, getUserDetails
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff'
    },
    header: {
        height: 60,
        backgroundColor: '#fff',
        borderBottomWidth: 0.5,
        borderBottomColor: '#ddd',
    },
    searchSection: {
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        marginHorizontal: 10,
        marginVertical: 10,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: '#000',
        shadowOpacity: 0.2,
        elevation: 1,
        borderRadius: 5,
    },
    searchIcon: {
        paddingLeft: 15,
        paddingRight: 15,
        color: 'grey'
    },
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        backgroundColor: '#fff',
        color: 'grey',
        fontWeight: '700',
    },
    sliderImg: { resizeMode: 'stretch', flex: 1, width: null, height: null, alignItems: 'center' },

    promoImg: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'stretch',
        borderWidth: 0.1, borderColor: '#888',
    },
    brandImg: {
        width: '100%',
        height: width / 8 - 20,
        resizeMode: 'contain',
        borderWidth: 0.1, borderColor: '#888',
    },
    hdng: {
        fontSize: 18,
        color: '#333',
        fontWeight: '700'
    },
    btn: {
        color: '#fff', backgroundColor: '#C7B048', paddingVertical: 3, fontSize: 13, borderRadius: 3, fontWeight: '500', paddingHorizontal: 7, textAlign: 'center',
    },
    modalDiv: {
        flex: 1, justifyContent: 'center', alignItems: 'center'
    },
    mainTab: {
        position: 'absolute', bottom: 0, flexDirection: 'row'
    },
    footerTabs: {
        flex: 1, backgroundColor: '#C7B048', justifyContent: 'center', alignItems: 'center', borderWidth: .5, borderColor: '#fff'
    },
    tabText: {
        padding: 20, color: '#fff'
    }
});
