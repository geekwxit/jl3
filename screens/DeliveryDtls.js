import React, { Component } from "react";
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Loader from '../screens/components/Loader';
import { addToCart, addToWishlist, removeFromCart, setCustomerAddress, resetAddress, placeOrder } from '../redux/actions/actions';

import { connect } from 'react-redux';

const { height, width } = Dimensions.get('window');

class DeliveryDtls extends Component {

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.customerAddress !== prevState.customerAddress) {
      console.log(nextProps.customerAddress);
      return {
        customerAddress: nextProps.customerAddress
      }
    }
    if (!nextProps.error && nextProps.orderSuccess) {
      nextProps.navigation.navigate('SuccessScreen', { order_id: nextProps.current_order_id });
      return {
        orderSuccess: nextProps.orderSuccess
      }
    }

    return null
  }

  state = {
    customerAddress: {}, product: null, orderSuccess: false
  }


  constructor(props) {
    super(props);
  }


  componentDidMount() {
    const product = this.props.navigation.state.params.data
    console.log(product);
    this.setState({ product })
  }

  getPayableAmount(amount) {
    var amountPayable = Number(amount) + 49 + 49
    return amountPayable;
  }

  getPrice() {
    var totalPrice = 0;
    this.props.cartArray.map((data, key) => {
      totalPrice += Number(data.pro_sale_price)
    })
    return totalPrice
  }

  placeOrder() {
    const { cartArray, token, addressId, placeOrder } = this.props

    const product = this.props.navigation.state.params.data

    if (product != "NO-DATA") {
      const buy_now = [product];
      console.log(buy_now)
      const obj = {
        cartArray: buy_now, token, address_id: addressId
      }
      console.log(obj);
      placeOrder(obj);
    }
    else if (cartArray, token, addressId) {
      const obj = {
        cartArray, token, address_id: addressId
      }
      console.log(obj);
      placeOrder(obj);
    }
  }

  render() {

    const { customerAddress, } = this.state

    const product = this.props.navigation.state.params.data
    console.log(product);
    console.log(customerAddress);
    const { isLoading } = this.props
 
    return (
      <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1, backgroundColor: '#fff', marginHorizontal: 10, marginVertical: 20 }}>
        {(product != "NO-DATA") ? (
          <View style={{ width: width, height: width / 4.5, backgroundColor: '#fff', marginBottom: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
            <View style={{ flex: 4, }}>
              <Image source={{ uri: 'http://cripcoin.co/jewellery/images/' + product.featured_image }} style={{ flex: 1, height: null, width: null, resizeMode: 'contain', }} />
            </View>
            <View style={{ flex: 8, paddingLeft: 10 }}>
              <View>
                <Text style={{ fontSize: 18, color: '#333', paddingRight: 10 }} numberOfLines={1} >
                  {product.pro_title}
                </Text>
              </View>
              <View style={{ marginTop: 5 }}>
                <Text style={{ fontSize: 16, color: '#777', fontWeight: '500', }}>{product.pro_base_category_name}</Text>
              </View>
              <View style={{ marginTop: 5 }}>
                <Text style={{ fontSize: 14, color: '#333', }}>Qty: 1</Text>
              </View>
            </View>
            <View style={{ flex: 3, }}>
              <Text style={{ fontSize: 18, color: '#cc0000', fontWeight: '500', }}>${product.pro_final_price}</Text>
            </View>
          </View>
        ) :
          this.props.cartArray.map((data, key) =>
            (<View key={key} style={{ width: width, height: width / 4.5, backgroundColor: '#fff', marginBottom: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 4, }}>
                <Image source={{ uri: 'http://cripcoin.co/jewellery/images/' + data.featured_image }} style={{ flex: 1, height: null, width: null, resizeMode: 'contain', }} />
              </View>
              <View style={{ flex: 8, paddingLeft: 10 }}>
                <View>
                  <Text style={{ fontSize: 18, color: '#333', paddingRight: 10 }} numberOfLines={1} >
                    {data.pro_title}
                  </Text>
                </View>
                <View style={{ marginTop: 5 }}>
                  <Text style={{ fontSize: 16, color: '#777', fontWeight: '500', }}>{data.pro_base_category_name}</Text>
                </View>
                <View style={{ marginTop: 5 }}>
                  <Text style={{ fontSize: 14, color: '#333', }}>Qty: 1</Text>
                </View>
              </View>
              <View style={{ flex: 3, }}>
                <Text style={{ fontSize: 18, color: '#cc0000', fontWeight: '500', }}>${data.pro_final_price}</Text>
              </View>
            </View>))
        }
        {/* 
        <View style={styles.section}>
          <View style={{ width: '40%' }}>
            <View style={styles.box}>
              <Text style={{ fontSize: 20, color: '#333', fontWeight: '500' }}>Delivery Date</Text>
            </View>
          </View>
          <View style={{ width: '60%' }}>
            <View style={styles.box}>
              <Text style={{ fontSize: 16, color: '#333', fontWeight: '500' }}>28 May 2019</Text>
              <Text style={{ fontSize: 16, color: '#333', fontWeight: '100' }}>Fixed Delivery Time</Text>
              <Text style={{ fontSize: 16, color: '#333', fontWeight: '100' }}>11:00 - 12:00 Hrs </Text>
            </View>
          </View>
        </View> */}

        <View style={styles.section}>
          <View style={{ width: '40%' }}>
            <View style={styles.box}>
              <Text style={{ fontSize: 20, color: '#333', fontWeight: '500' }}>Address</Text>
            </View>
          </View>
          <View style={{ width: '55%' }}>
            <View style={styles.box}>
              <Text style={{ fontSize: 16, color: '#333', fontWeight: '100' }}>{customerAddress.rec_address}</Text>
              <Text style={{ fontSize: 16, color: '#333', fontWeight: '100' }}>{customerAddress.landmark}</Text>
              <Text style={{ fontSize: 16, color: '#333', fontWeight: '100' }}>{customerAddress.city + ' ' + customerAddress.country + ' '}<Text>{customerAddress.zipcode}</Text></Text>
            </View>
          </View>
          <View style={{ width: '5%', justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => {
              this.props.resetAddress()
              this.props.navigation.navigate('DeliveryAddress', { data: "N0-DATA", from: "detail" })
            }}>
              <Icon name={"edit"} style={{ fontSize: 20 }} />
            </TouchableOpacity>
          </View>
        </View>

        {/* <View style={styles.section}>
              <View style={{width:'40%'}}> 
                <View style={styles.box}> 
                  <Text style={{fontSize:20,color:'#333',fontWeight:'500'}}>Message Card</Text>
                </View>
              </View> 
              <View style={{width:'40%'}}> 
                <View style={styles.box}>  
                    <Text style={{fontSize:16,color:'#333',fontWeight:'100'}}>Use Template or{"\n"}write your own</Text> 
                </View>
              </View>
              <View style={{width:'20%',justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity>
                    <Text style={{fontSize:20,fontWeight:'100',paddingVertical:2,paddingHorizontal:7,backgroundColor:'#5CAA16',color:'#fff',fontSize:15,borderRadius:5}}>FREE</Text>
                </TouchableOpacity>
              </View>
            </View> */}

        {/* <View style={styles.section}>
          <View style={{ width: '40%' }}>
            <View style={styles.box}>
              <Text style={{ fontSize: 20, color: '#333', fontWeight: '500' }}>Gift Message</Text>
            </View>
          </View>
          <View style={{ width: '60%' }}>
            <View style={styles.box}>
              <TextInput placeholder="Type gift message" placeholderTextColor="grey" style={{ height: 45, fontSize: 16, color: 'grey', borderColor: '#bbb', borderWidth: 1, borderRadius: 10, paddingHorizontal: 10 }} />
            </View>
          </View>
        </View> */}

        {/* <View style={styles.section}>
          <View style={{ width: '40%' }}>
            <View style={styles.box}>
              <Text style={{ fontSize: 20, color: '#333', fontWeight: '500' }}>Sender's Info</Text>
            </View>
          </View>
          <View style={{ width: '60%' }}>
            <View style={[styles.box, { borderColor: '#bbb', borderWidth: 0.5, borderRadius: 10 }]}>
              <TextInput placeholder="Full name" placeholderTextColor="grey" style={{ height: 45, fontSize: 16, color: 'grey', borderColor: '#bbb', borderWidth: 0.5, paddingHorizontal: 10 }} />
              <View style={{ flexDirection: 'row' }}>
                <View style={{ width: '25%' }}>
                  <TextInput placeholder="+91" placeholderTextColor="grey" style={{ height: 45, fontSize: 16, color: 'grey', borderColor: '#bbb', borderWidth: 0.5, paddingHorizontal: 10 }} />
                </View>
                <View style={{ width: '75%' }}>
                  <TextInput placeholder="Contact number" placeholderTextColor="grey" style={{ height: 45, fontSize: 16, color: 'grey', borderColor: '#bbb', borderWidth: 0.5, paddingHorizontal: 10 }} />
                </View>
              </View>
              <TextInput placeholder="Email id" placeholderTextColor="grey" style={{ height: 45, fontSize: 16, color: 'grey', borderColor: '#bbb', borderWidth: 0.5, paddingHorizontal: 10 }} />
            </View>
          </View>
        </View> */}

        <View style={styles.section}>
          <View style={{ width: '40%' }}>
            <View style={styles.box}>
              <Text style={{ fontSize: 20, color: '#333', fontWeight: '500' }}>Summary</Text>
            </View>
          </View>
          <View style={{ width: '60%' }}>
            <View style={styles.box}>
              <View style={{}}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5 }}>
                  <View>
                    <Text style={{ fontSize: 16, color: '#333', }}>Subtotal</Text>
                  </View>
                  <View>
                    <Text style={{ fontSize: 16, color: '#333', }}>
                      {(product == "NO-DATA") ? this.getPrice() : product.pro_final_price}
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5 }}>
                  <View>
                    <Text style={{ fontSize: 16, color: '#333', }}>Discount</Text>
                  </View>
                  <View>
                    <Text style={{ fontSize: 16, color: '#333', }}>$49</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5 }}>
                  <View>
                    <Text style={{ fontSize: 16, color: '#333', }}>Shipping</Text>
                  </View>
                  <View>
                    <Text style={{ fontSize: 16, color: '#333', }}>$49</Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5, marginTop: 5, paddingVertical: 10, borderColor: '#bbb', borderTopWidth: 1, borderBottomWidth: 1 }}>
                  <View>
                    <Text style={{ fontSize: 17, color: '#333', fontWeight: '500' }}>GRAND TOTAL</Text>
                  </View>
                  <View>
                    <Text style={{ fontSize: 17, color: '#333', fontWeight: '500' }}>
                      ${(product != "NO-DATA") ? this.getPayableAmount(product.pro_final_price) : this.getPayableAmount(this.getPrice())}
                    </Text>
                  </View>
                </View>
                {/* <View style={{ flexDirection: 'row', marginTop: 10, borderColor: '#bbb', borderWidth: 1 }}>

                  <View style={{ width: '60%' }}>
                    <TextInput placeholder="Coupon Code" placeholderTextColor="grey" style={{ height: 45, fontSize: 16, color: 'grey', paddingHorizontal: 10 }} />
                  </View>
                  <View style={{ width: '40%', justifyContent: 'center' }}>
                    <TouchableOpacity style={{ justifyContent: 'center' }}>
                      <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: '100', paddingVertical: 10, height: 45, textAlign: 'center', color: '#fff', backgroundColor: '#5CAA16', }}>APPLY</Text>
                    </TouchableOpacity>
                  </View>
                </View> */}
              </View>
            </View>
          </View>
        </View>

        <Loader visible={isLoading}/>
        <TouchableOpacity onPress={() => this.placeOrder()}>
          <Text style={styles.buttonText}>Place Order</Text>
        </TouchableOpacity>

      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => ({
  isLoading: state.customerReducer.isLoading,
  allProducts: state.productReducer.allProducts,
  orderSuccess: state.customerReducer.orderSuccess,
  current_order_id: state.customerReducer.current_order_id,
  cartArray: state.cartReducer.cartArray,
  customerAddress: state.customerReducer.customerAddress,
  addressId: state.customerReducer.addressId,
  wishlistArray: state.cartReducer.wishlistArray,
  error: state.customerReducer.error,
  errorMsg: state.customerReducer.errorMsg,
  token: state.authReducer.token
});
const mapDispatchToProps = {
  addToCart, addToWishlist, removeFromCart, setCustomerAddress, resetAddress, placeOrder
}

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryDtls);

const styles = StyleSheet.create({
  logo: { width: 200, resizeMode: 'contain', },
  box: { borderRadius: 5, marginBottom: 12, backgroundColor: '#fff' },
  section: { flexDirection: 'row', justifyContent: 'space-between', borderTopWidth: 1, borderColor: '#ddd', paddingTop: 12 },
  inputRadio: { borderWidth: 1, borderColor: 'grey', padding: 10, borderRadius: 5, marginBottom: 10, backgroundColor: '#fff' },
  TextInput: { fontSize: 16, color: '#333' },
  buttonText: {
    borderWidth: 1,
    fontSize: 18,
    fontWeight: '100',
    padding: 10,
    textAlign: 'center',
    color: '#fff',
    borderColor: 'grey',
    backgroundColor: '#2C2F8C',
    borderRadius: 5,
  }
});
