import React, { Component } from "react";
import {Text,
   Modal, View, StyleSheet, Platform,
  Dimensions, TouchableOpacity,TouchableWithoutFeedback,
  TextInput, Picker, SafeAreaView,
  ScrollView } from 'react-native';
// import {
//   Container, Button,Header, Content, Card, CardItem,
//   Body, Text } from 'native-base';
import Loader from '../screens/components/Loader';
import { addToCart, addToWishlist, removeFromCart, addCustomerAddress, updateCustomerAddress, getCustomerAddressList, deleteCustomerAddress } from '../redux/actions/actions';

import { connect } from 'react-redux';
import ModalDropdown from "react-native-modal-dropdown";
import axiosInstance from "../redux/misc/axiosInstance";
const { height, width } = Dimensions.get('window');


class DeliveryAddress extends Component {

  static getDerivedStateFromProps(nextProps, prevState) {

    console.log(nextProps, prevState);
    if(nextProps.customerAddressList!==prevState.customerAddressList){
      return {
        customerAddressList : nextProps.customerAddressList,
      }
    }

    if (nextProps.error !== prevState.error) {
      //alert(nextProps.errorMsg)
      return {
        error: nextProps.error,
        errorMsg: nextProps.errorMsg
      }
    }

    // if (nextProps.success !== prevState.success) {
    //   console.log(nextProps.customerAddress, prevState.customerAddress);
    //   if (nextProps.navigation.state.params.from === "detail") {
    //     nextProps.navigation.goBack(null)
    //   } else {
    //     nextProps.navigation.navigate('DeliveryDlts', { data: nextProps.navigation.state.params.data });
    //   }
    //   return {
    //     customerAddress: nextProps.customerAddress
    //   }
    // }
    return null
  }

  componentDidMount() {
    this.props.getCustomerAddressList({user_id: this.props.token});
  }

  constructor(props) {
    super(props);
    this.setPreviousData = this.setPreviousData.bind(this);
    //
    // if (this.props.navigation.state.params.data !== "NO-DATA") {
    //   const product = this.props.navigation.state.params.data
    //   console.log(product)
    //   this.setState({ product })
    // }
    //
    // if (this.props.navigation.state.params.from == "detail") {
    //   this.setState({ fromPage: "detail" })
    // }

  }

  state = {
    // recipient_name: null, recipient_addresss: null, landmark: null, city: null, country: null,
    // zipcode: null, contact_number: null, alt_number: null, recipient_email: null,
    error: false, errorMsg: null, customerAddressList: [], success: false, product: null, fromPage: null,
    addressBox: false, ad: false, shouldAdd: true, shouldUpdate: false,
    dataForAddressBox: null, selectedAddress: null
  }

  deleteAddress(index){
    var address = this.props.customerAddressList[index];
    var data = {user_id: this.props.token, requestType: "DELETE", address: address}
    this.props.deleteCustomerAddress(JSON.stringify(data), index)
        .then(()=>this.setState({addressBox: false, ad: true,shouldUpdate: false, shouldAdd: true,}))

    newAddress = Object.assign([], this.state.customerAddressList);
    newAddress.splice(index, 1);
    if(newAddress.length>0){
      this.setState({
        customerAddressList : newAddress
      })
    }
    else {
      this.setState({
        customerAddressList : [],
        ad:false
      })
    }
  }
  editAddress(index){
    address = this.props.customerAddressList[index];
    address.index = index;
    this.setState({shouldUpdate:true, shouldAdd: false, addressBox: true})
    this.setState({dataForAddressBox: address});
    console.warn("SOMEMEOMEOEMOE", this.state.addressBox);
  }
  setPreviousData(){
    this.setState({dataForAddressBox: null});
  }

  updateAddress(address, index){
    var data = {user_id: this.props.token, requestType: "UPDATE", address: address}
    this.props.updateCustomerAddress(JSON.stringify(data), index)
        .then(()=>this.setState({addressBox: false, ad: true,shouldUpdate: false, shouldAdd: true,}))
    // index  = addressDetails.index;
    // addressTemp = Object.assign([],this.state.customerAddressList);
    // addressTemp[index] = addressDetails;
    //this.setState({ addressBox: false,  customerAddressList: addressTemp});
  }

  // validateData(){
  //   const { recipient_name, recipient_addresss, recipient_email, landmark, city, contact_number, country, alt_number, zipcode } = this.state
  //
  //   if (recipient_name && recipient_addresss && contact_number && alt_number && landmark && city && country && zipcode)
  //     if (recipient_name.length >= 30) {
  //       alert('Full name must be below 30 alphabet');
  //     } else if (!recipient_name.match(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)) {
  //       alert('Please enter your fullname and only alphabets');
  //     } else if (!contact_number.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/)) {
  //       alert('Contact number must be in numeric and only 10 letters');
  //     } else if (!alt_number.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/)) {
  //       alert('Alternate number must be in numeric and only 10 letters');
  //     }// } else if (recipient_email != null) {
  //     //   if (!recipient_email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
  //     //     alert('please type a valid email address');
  //     //   }
  //     // }
  //     else if (city.length > 30 || !city.match(/^([A-Za-z]+?)$/)) {
  //       alert('Please enter a valid city');
  //     } else if (!landmark.match(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)) {
  //       alert('Please enter a valid landmark');
  //     } else if (country.length > 30 || !country.match(/^([A-Za-z]+?)$/)) {
  //       alert('Please enter a valid country');
  //     } else if (zipcode == null) {
  //       alert('Zipcode is mandatory');
  //     } else {
  //       const AddressObject = {
  //         token: this.props.token,
  //         rec_name: recipient_name,
  //         rec_address: recipient_addresss,
  //         landmark,
  //         city,
  //         country,
  //         zipcode,
  //         rec_contact_no: contact_number,
  //         rec_alt_no: alt_number,
  //         // rec_email: recipient_email
  //       }
  //       this.props.setCustomerAddress(AddressObject)
  //       // this.props.navigation.navigate('DeliveryDlts')
  //     }
  // }

  async makePayment(){
    var selectedAddress = null;
    for(var i=0; i<this.props.customerAddressList.length; i++){
      var address = this.props.customerAddressList[i];
      if(address.highlight){
        selectedAddress = address;
      }
    }
    // selectedAddress = this.props.customerAddressList.find(address=>{return address.highlight?address:null});
    debugger;
    if(selectedAddress!==null && selectedAddress!==''){
      console.log({
        token: this.props.token,
        address_id: selectedAddress.address_id,
        cartArray : this.props.cartArray
      });
     await axiosInstance.post('http://cripcoin.co/jewellery/mobileApi/checkout.php',
         {
           token: this.props.token,
           address_id: this.state.selectedAddress.address_id,
           cartArray : this.props.cartArray
         })
         .then(response=>response.data)
         .then(response=>{
           if(response.error){
             alert("Order Unsuccessful!");
           }
           else{
             this.props.navigation.navigate('SuccessScreen', {order_id: response.order_id});
           }
         })
         .catch(e=>{
           console.log("Payment Indeitifier", e)
           alert("Order Unsuccessful");
         })
    }
    else{
      alert("Please select an address!");
    }
  }

  addAddress(address){
    var data = {user_id: this.props.token, requestType: "ADD", address: address}
    this.props.addCustomerAddress(JSON.stringify(data))
        .then(()=>this.setState({addressBox: false, ad: true}))
    // this.setState({customerAddressList: [...this.state.customerAddressList, address]});
    // this.setState({addressBox: false})
    // this.setState({ad: true})
  }

  render() {
    const { isLoading, customerAddressList } = this.props;
    const parent = this.parent;
    return (
      <SafeAreaView style={{ flex: 1, marginHorizontal: 15 }}>
        <AddressBox setPreviousData={()=>this.setPreviousData()}
                    addAddress={(params)=>this.addAddress(params)}
                    onDismiss={()=>this.setState({addressBox: false, dataForAddressBox: null})}
                    data={this.state.dataForAddressBox}
                    visible={this.state.addressBox}
                    add={this.state.shouldAdd}
                    update={this.state.shouldUpdate}
                    updateAddress={(params, index)=>this.updateAddress(params, index)}
        />
        {!customerAddressList.length>0?
            <View style={{flex:1, alignItems:'center', justifyContent: 'center'}}>
              <TouchableWithoutFeedback style={{flex:1}}>
                <View>
                  <PlusButton addAddress={()=>this.setState({addressBox: true})}/>
                  <Text style={{color: '#9f9f9f' ,alignSelf:'center'}}>No Address Saved</Text>
                  <Text style={{color: '#9f9f9f',alignSelf:'center'}}>Tap to add one</Text>
                </View>
              </TouchableWithoutFeedback>

            </View>
            :
            <View style={{marginTop:10,alignItems:'center'}}>
              <Text>Tap to select any address</Text>
            <ScrollView style={{paddingBottom: 20}} showsVerticalScrollIndicator={false}>
              {
                customerAddressList.map((data, index)=>{
                  //this.state.customerAddressList[index] = index;
                  return(
                      <TouchableOpacity key={index} onPress={()=>this.selectAddress(index)}>
                        <Address
                            highlight = {data.highlight}
                            key={index}
                            name = {data.name}
                            address={data.address}
                            editAddress={()=>this.editAddress(index)}
                            delete={()=>this.deleteAddress(index)}
                            phone = {data.contact_no}
                        />
                      </TouchableOpacity>
                  )
                })
              }
              <View style={{alignItems: 'center', marginTop: 20}}>
                <TouchableOpacity onPress={()=>this.setState({addressBox: true})}><View style={{backgroundColor: '#dcc462', borderRadius: 3, padding: 10}}><Text style={{color: 'white',fontWeight:'500'}}>Add Address</Text></View></TouchableOpacity>
              </View>
              <View style={{alignItems: 'center', marginTop: 20}}>
                <TouchableOpacity onPress={()=>this.makePayment()}><View style={{backgroundColor: '#dcc462', borderRadius: 3, padding: 10}}><Text style={{color: 'white',fontWeight:'500'}}>Make Payment</Text></View></TouchableOpacity>
              </View>
            </ScrollView>

            </View>
        }
      </SafeAreaView>
    );
  }

  selectAddress(index){
      //this.setState({customerAddressList : [...this.state.customerAddressList, this.state.customerAddressList[index].highlight : true]})
    // this.setState({
    //   customerAddressList: update(this.state.customerAddressList, {index: {highlight: {$set: 'updated field name'}}})
    // })
    // this.setState(prevState => ({
    //   customerAddressList: [
    //     ...prevState.customerAddressList,
    //     {prevState.customerAddressList[index].highlight : true},
    //   ],
    // }));
    if(this.state.selectedAddress){
      i  = this.state.selectedAddress.index;
      address = this.state.customerAddressList[i];
      address.highlight = false;
      this.setState({customerAddressList: [...this.state.customerAddressList, address]});
      address = this.state.customerAddressList[index];
      address.highlight = true;
      address.index = index;
      this.setState({customerAddressList: [...this.state.customerAddressList, address]});
      this.setState({selectedAddress: address});
    }
    else {
      address = this.state.customerAddressList[index];
      address.highlight = true;
      address.index = index;
      this.setState({selectedAddress: address});
      this.setState({customerAddressList: [...this.state.customerAddressList, address]});
    }
  }
}

class AddressBox extends Component{
  constructor(props){
    super(props);
    this.state = {
      address: null, address_id: null, alt_no: null, city: null, contact_no: null, country: null,
      email: null, highlight: false, index: null, landmark: null, name: null, token: null,
      unique_id: null, zipcode: null
    }
    this.initialState = this.state;
  }

  addAddress(){
    this.props.setPreviousData();
    var customerDetails = this.state;
    this.props.addAddress(customerDetails);
  }
  updateAddress(){
    var customerDetails = this.state;
    this.props.updateAddress(customerDetails, customerDetails.index);
  }
  copyDataToState(){
    this.resetValues();
    console.log("PROPS", this.props.data);
    console.log("STATE", this.state);
    this.props.update?this.props.data?this.setState(this.props.data):null:null;
  }

  resetValues(){
    this.setState(this.initialState);
  }
  render(){

    return (
        <Modal onShow={()=>this.copyDataToState()} visible={this.props.visible} transparent={true} onDismiss={() =>{this.resetValues(); this.props.onDismiss()}}>
          <TouchableWithoutFeedback onPress={() => this.props.onDismiss()}>
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#737373'}}>
              <TouchableWithoutFeedback>

                <View style={{width: 90*(width/100), padding: 20, height: 500, borderRadius: 10, backgroundColor: 'white'}}
                      onStartShouldSetResponder={(env) => false}>
                  <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{marginTop: 20}}>
                      <View style={styles.input}>
                        <TextInput value={this.state.name} placeholder='* Recipient`s name'
                                   onChangeText={(name) => {
                                     this.setState({name})
                                   }}
                                   style={styles.TextInput} placeholderTextColor={'grey'}/>
                      </View>
                    </View>
                    <View>
                      <View style={styles.input}>
                        <TextInput value={this.state.address} placeholder='Recipient`s address'
                                   onChangeText={(address) => {
                                     this.setState({address})
                                   }}
                                   style={styles.TextInput} placeholderTextColor={'grey'}/>
                      </View>
                    </View>

                    <View>
                      <View style={styles.input}>
                        <TextInput
                            value={this.state.landmark}
                            placeholder='(Landmark)'
                                   onChangeText={(landmark) => {
                                     this.setState({landmark})
                                   }}
                                   style={styles.TextInput} placeholderTextColor={'grey'}/>
                      </View>
                    </View>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                      <View style={{width: width / 2 - 25}}>
                        <View style={styles.input}>
                          <TextInput
                              value={this.state.city}
                              placeholder='Enter City'
                                     onChangeText={(city) => {
                                       this.setState({city})
                                     }}
                                     style={styles.TextInput} placeholderTextColor={'grey'}/>
                        </View>
                      </View>
                      <View style={{width: width / 2 - 25}}>
                        <View style={styles.input}>
                          <TextInput placeholder='Enter Country'
                                     value={this.state.country}
                                     onChangeText={(country) => {
                                       this.setState({country})
                                     }}
                                     style={styles.TextInput} placeholderTextColor={'grey'}/>
                        </View>
                      </View>
                    </View>

                    <View>
                      <View style={styles.input}>
                        <TextInput placeholder='Enter Zip Code'
                                   value={this.state.zipcode}
                                   onChangeText={(zipcode) => {
                                     this.setState({zipcode})
                                   }}
                                   style={styles.TextInput} placeholderTextColor={'grey'}/>
                      </View>
                    </View>
                    <View style={styles.input}>
                      <TextInput placeholder='Recipient`s contact number'
                                 value={this.state.contact_no}
                                 onChangeText={(contact_no) => {
                                   this.setState({contact_no})
                                 }}
                                 style={styles.TextInput} placeholderTextColor={'grey'}/>
                    </View>
                    <View style={styles.input}>
                      <TextInput placeholder='Recipient`s Alt. number'
                                 value={this.state.alt_no}
                                 onChangeText={(alt_no) => {
                                   this.setState({alt_no})
                                 }}
                                 style={styles.TextInput} placeholderTextColor={'grey'}/>
                    </View>
                    <TouchableOpacity style={{marginTop: 10}}>
                      <Text
                          onPress={() => this.props.update?this.updateAddress():this.props.add?this.addAddress():null}
                          style={styles.buttonText}>{this.props.update?"Update Address":"Add Address"}</Text>
                    </TouchableOpacity>
                  </ScrollView>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
    );
  }
}
const Address = (props) =>{
  const styles = {
    container: {
      marginTop: 10, shadowOffset:{  width: 10,  height: 10,  },
      shadowColor: 'rgba(0,0,0,0.22)',
      shadowOpacity: 1.0,
    },
    component: {
      padding: 10,
      borderTopColor: 'rgba(255,255,255,0)',
      borderRightColor: 'rgba(255,255,255,0)',
      borderLeftColor: 'rgba(255,255,255,0)',
      borderWidth:1,
      borderBottomColor: '#fff'
    },
    buttonContainer: {
      flex:1, borderWidth:1,
      borderRightColor: 'white',
      borderLeftColor: 'rgba(255,255,255,0)',
      borderBottomColor: 'rgba(255,255,255,0)',
      borderTopColor: 'rgba(255,255,255,0)',
      alignItems:'center', justifyContent: 'center',
    },
    phoneNumber: {
      padding: 10,
      borderTopColor: 'rgba(255,255,255,0)',
      borderRightColor: 'rgba(255,255,255,0)',
      borderLeftColor: 'rgba(255,255,255,0)',
      borderWidth:1,
      borderBottomColor: '#fff'
    }
  }
  return(
      <View key={props.key} style={styles.container}>
        <View style={{width: 90*(width/100), borderRadius: 3,backgroundColor: props.highlight?'#C7B048':'#e1e7e8'}}>
          <View style={styles.component}>
            <Text style={{fontSize: 20}}>{props.name}</Text>
          </View>
          <View style={styles.component}>
            <Text>{props.address}</Text>
          </View>
          <View type="footer" style={styles.phoneNumber}>
            <Text>{props.phone}</Text>
          </View>
          <View type="footer" style={{flexDirection: 'row', padding: 10}}>
            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={()=>props.editAddress()}>
                <Text style={{textTransform:'uppercase',fontWeight:'500',color:'#222',padding:5}}>Edit</Text>
              </TouchableOpacity>
            </View>
            <View style={{flex:1, alignItems:'center', justifyContent: 'center',}}>
              <TouchableOpacity onPress={()=>props.delete()}>
                <Text style={{textTransform:'uppercase',fontWeight:'500',color:'#222',padding:5}}>Delete</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
  );
}
const PlusButton = (props) => {
  parent =  {width: 100, height: 100, color: '#a8a8a8'};
  return(
      <TouchableWithoutFeedback onPress={()=>props.addAddress("fas")}>
    <View style={{ marginBottom:5, alignSelf: 'center', borderColor: parent.color ,width: parent.width, height: parent.height, borderWidth: 1, borderRadius: 25, borderStyle:'dashed'}}>
      <View>
        <View style={{ alignSelf: 'center', position:'absolute',borderRadius: 10, top:20%parent.height,backgroundColor: parent.color ,width: 8%parent.width, height: 60%parent.height}}/>
        <View style={{ alignSelf: 'center', borderRadius: 10, top: parent.height/2-(8%parent.height)/2,backgroundColor: parent.color ,width: 60%parent.width, height: 8%parent.height}}/>
      </View>
    </View>
      </TouchableWithoutFeedback>
  );
}

const mapStateToProps = (state) => ({
  isLoading: state.customerReducer.isLoading,
  allProducts: state.productReducer.allProducts,
  cartArray: state.cartReducer.cartArray,
  customerAddressList: state.customerReducer.customerAddress,
  wishlistArray: state.cartReducer.wishlistArray,
  error: state.customerReducer.error,
  errorMsg: state.customerReducer.errorMsg,
  success: state.customerReducer.success,
  token: state.authReducer.token
});
const mapDispatchToProps = {
  addToCart, addToWishlist, removeFromCart, addCustomerAddress, updateCustomerAddress, getCustomerAddressList, deleteCustomerAddress
}

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryAddress);

const styles = StyleSheet.create({
  logo: { width: 200, resizeMode: 'contain', },
  input: { borderWidth: 1, borderColor: 'grey', paddingLeft: 10, borderRadius: 5, marginBottom: 10, height: 45,paddingTop: Platform.OS === 'ios' ? 12 : 0, backgroundColor: '#fff' },
  inputRadio: { borderWidth: 1, borderColor: 'grey', padding: 10, borderRadius: 5, marginBottom: 10, backgroundColor: '#fff' },
  TextInput: { fontSize: 16, color: '#333' },
  buttonText: {
    borderWidth: 1,
    fontSize: 18,
    fontWeight: '100',
    padding: 10,
    textAlign: 'center',
    color: '#fff',
    borderColor: 'grey',
    backgroundColor: '#C7B048',
    borderRadius: 5,
  }
});
