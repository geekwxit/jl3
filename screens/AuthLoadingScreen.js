import React, { Component } from "react";
import { View, Text, StyleSheet, ActivityIndicator, AsyncStorage, Platform, Linking } from "react-native";

import { connect } from 'react-redux';
// actions imported
import { setUserToken, getUserDetails, changeAppLanguage} from '../redux/actions/actions';
import {Language} from "./Language";

class AuthLoadingScreen extends Component {
    static navigationOptions = {
    };


    constructor() {
        super()
        this.loadAppLanguage();
        this.loadApp();
    }

    componentDidMount() {
        if (Platform.OS === 'android') {
            Linking.getInitialURL().then(url => {
                this.navigate(url);
            });
        } else {
            Linking.addEventListener('url', this.handleOpenURL);
        }
    }

    navigate(e){
        debugger;
    }

    handleOpenURL(event){
        let routeInfo = null;
        let data = new Object();
        let url = event.url;
        url = url.replace(/.*?:\/\//g, '');
        routeInfo = url.split('/');
        routeName = routeInfo[0];
        data.id      = routeInfo[1];
        data.name = routeInfo[2];

        if(routeName=='product'){
            this.props.navigation.navigate('Product', {
                pro_id: data.id,
                guest_id: this.state.guest_id,
                pro_name: data.name
            })
        }
    }



    loadAppLanguage = async () => {
        const language = await AsyncStorage.getItem('@app_language');
        language?this.props.changeAppLanguage(language=='ar'?Language.ar:Language.en):null;
    }

    loadApp = async () => {
        const userToken = await AsyncStorage.getItem('userToken')

        this.props.setUserToken(userToken)
        userToken?this.props.getUserDetails({user_id: userToken}):null;
        this.props.navigation.navigate(userToken ? 'App' : 'Auth')

    }

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator />
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
});
const mapDispatchToProps = {
    setUserToken, getUserDetails, changeAppLanguage
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthLoadingScreen);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
