import React, { Component } from "react";
import { View, Text, StyleSheet, Image, Dimensions, AsyncStorage,TouchableWithoutFeedback, TouchableOpacity} from "react-native";
import Carousel from 'react-native-carousel-view';
import ImageSlider from 'react-native-image-slider';
import { connect } from 'react-redux';
//import axios from "../redux/misc/axiosInstance";
// actions imported
import {  setGuestId, changeAppLanguage } from '../redux/actions/actions';

import axios from 'axios';
import {Language} from "./Language";

const { height, width } = Dimensions.get('window');

class Welcome extends Component {

  static getDerivedStateFromProps(nextProps, prevState) {
    console.log("NEW LANGUAGE: ", nextProps.language, "OLD LANGUAGE: ", prevState.language)
    if(nextProps.language!=prevState.language){
      return {
        language: nextProps.language
      }
    }
    return null;
  }

  constructor(props) {
    super(props)
    this.state = { imagesOfSlider: [], images: [], guest_id: '', englishButton: '#C7B048', arabicButton: '#ffffff', language: Language.en}
  }
  componentDidMount() {
    const url = 'http://cripcoin.co/jewellery/mobileApi/welcome.php';

    //data = axios.get(url);
    // fetch(url, {
    //   method: 'POST',
    //   headers: { 'Accept': 'application/json', 'Content-Type': 'application/json', },
    // })
    this.getAllData(url);
  }

  async getAllData(url){
    await axios.get(url)//.then((response) => response.json())
        .then(response=>response.data)
        .then((responseData) => {
          for (var i = 0; i < responseData.images.length; i++) {
            const copied = [...this.state.images]
            copied[i] = responseData.images[i].s_image
            this.setState({ images: copied }, () => { console.log("images", this.state.images); })
          }
        })
        .catch((error) => {
          console.log(error, "Error");
        });
    //this.randomString();
  }

  randomString() {
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 10;
    var randomstring = '';

    for (var i = 0; i < string_length; i++) {
      var rnum = Math.floor(Math.random() * chars.length);
      randomstring += chars.substring(rnum, rnum + 1);
    }
    this.setState({ guest_id: randomstring });
  }

  async requestGuestID(){
    const url = 'http://cripcoin.co/jewellery/mobileApi/generateGuest.php';
    await axios.get(url).then(response=>response.data)
        .then(response=>{
          response?
              !response.error?
                  this.props.setGuestId(response.guestID)
              //    this.setState({guest_id: response.guestID})
              :alert(this.state.language.screens.Welcome.guestIdError)
              :alert(this.state.language.screens.Welcome.guestIdError)
        })
        .catch(e=>console.error(e));
  }
  home() {
    this.requestGuestID().then(()=>{
      console.warn(this.state.guest_id, "this is guest id forund in fdsaf");
      this.props.navigation.navigate('Home');
    });
    //this.props.setGuestId('d6a3e17da388b2e');
    //this.props.navigation.navigate('Home');
    //this.props.setGuestId(this.state.guest_id)

  }
  render() {
    const strings = this.props.language.screens.Welcome;
    return (
      <View showsVerticalScrollIndicator={false} style={styles.welcome}>
        <View style={{ flex: 1, alignItems: 'center' }}>
          {/*<Carousel
              width={width}
              height={width}
              delay={3000}
              indicatorAtBottom={true}
              indicatorSize={20}
              indicatorText="✵"
              indicatorColor="#C7B048"
              >
              <View style={styles.sliderView}>
                <Image style={styles.sliderImg} source={require('../images/sl4.jpg')} />
              </View>
              <View style={styles.sliderView}>
                <Image style={styles.sliderImg} source={require('../images/sl3.jpg')} />
              </View>
              <View style={styles.sliderView}>
                <Image style={styles.sliderImg} source={require('../images/sl5.jpg')} />
              </View>
          </Carousel>*/}
          <View style={{ width: width, height: width, width: width }}>
            <ImageSlider
              autoPlayWithInterval={3000}
              images={this.state.images}
            />
          </View>
          <View style={{ marginTop: 18 }}>
            <Text style={{ color: '#C7B048', fontSize: 30, fontWeight: '700', }}>{strings.freeShipping}</Text>
          </View>

          <View style={{ marginTop: 10 }}>
            <Text style={{ paddingHorizontal: 20, fontSize: 16, textAlign: 'center' }}>{strings.details}</Text>
          </View>

          <View style={{ marginTop: 60 }}>
            <TouchableOpacity onPress={() => this.home()}>
              <Text style={{ paddingHorizontal: 20, fontSize: 20, fontWeight: '700', textAlign: 'center', color: '#1f3f47', }}>{strings.skip}</Text>
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 60 }}>
            <Text style={{ paddingHorizontal: 20, fontSize: 20, fontWeight: '700', textAlign: 'center', color: '#1f3f47', }}>{strings.selectAppLang}</Text>
              <View style={{marginTop: 5,height: 50, flexDirection:'row',width: 200, alignSelf: 'center'}}>
                <TouchableWithoutFeedback onPress={()=>this.changeLanguage(Language.en)}>
                <View style={{borderTopLeftRadius: 5,borderWidth:1,borderColor:'#C7B048', borderBottomLeftRadius: 5,alignItems:'center', justifyContent: 'center',backgroundColor:this.props.language.code=='en'?'#C7B048':'#fff', height: 50, width: 100}}>
                  <Text>English</Text>
                </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={()=>this.changeLanguage(Language.ar)}>
                <View style={{borderTopRightRadius: 5, borderWidth:1, borderColor:'#C7B048', borderBottomRightRadius: 5,alignItems:'center', justifyContent: 'center',backgroundColor:this.props.language.code=='ar'?'#C7B048':'#fff', height: 50, width: 100}}>
                  <Text>عربي</Text>
                </View>
                </TouchableWithoutFeedback>
              </View>
          </View>
        </View>

        <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('SignIn')} style={styles.bottomBtn1}>
            <Text style={{ fontSize: 16, fontWeight: '700', textAlign: 'center', color: '#fff' }}>{strings.signIn}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.props.navigation.navigate('SignUp')} style={styles.bottomBtn2}>
            <Text style={{ fontSize: 16, fontWeight: '700', textAlign: 'center', color: '#fff' }}>{strings.signUp}</Text>
          </TouchableOpacity>
        </View>

      </View>
    );
  }

  async changeLanguage(language){
    // this.setState({
    //   englishButton: language.name=='English'?'#C7B048':'#FFFFFF',
    //   arabicButton: language.name=='Arabic'?'#C7B048':'#FFFFFF',
    // });
    await AsyncStorage.setItem('@app_language', language.code)
        .then(()=>this.props.changeAppLanguage(language))
  }
}

const styles = StyleSheet.create({
  welcome: { flex: 1, backgroundColor: '#fff' },
  sliderImg: { flex: 1, resizeMode: 'cover', height: null, width: null },
  bottomBtn1: { backgroundColor: '#C7B048', width: width / 2 - 1, paddingVertical: 12, borderTopRightRadius: 5 },
  bottomBtn2: { backgroundColor: '#C7B048', width: width / 2 - 1, paddingVertical: 12, borderTopLeftRadius: 5 },
});


const mapStateToProps = (state) => ({
  isLoading: state.productReducer.isLoading,
  allProducts: state.productReducer.allProducts,
  cartArray: state.cartReducer.cartArray,
  error: state.productReducer.error,
  errorMsg: state.productReducer.errorMsg,
  language: state.homeReducer.app_language
});
const mapDispatchToProps = {
  setGuestId, changeAppLanguage
}

export default connect(mapStateToProps, mapDispatchToProps)(Welcome);
