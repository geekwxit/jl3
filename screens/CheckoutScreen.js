import React, { Component } from "react";
import { View,	Text,	StyleSheet	} from "react-native";

class CheckoutScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text>CheckoutScreen</Text>
            </View>
        );
    }
}
export default CheckoutScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});