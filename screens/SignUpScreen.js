import React, { Component } from "react";
import {
    View,
    Text,
    SafeAreaView,
    Platform,
    TouchableOpacity,
    Dimensions,
    TextInput,
    Image,
    ScrollView,
    StyleSheet,
    ActivityIndicator
} from "react-native";
// connect for redux
import { connect } from 'react-redux';
// actions imported
import { userSignIn } from '../redux/actions/actions';
// apiRequest
import { signIn, signUp } from '../redux/misc/apiRequest';
// Data storing in async
import { saveDataInAsync } from '../redux/misc/asyncData';
// Import Loader
import Loader from '../screens/components/Loader';
const PASSWORDICON = require('../images/key3.png');
const USEREMAILICON = require('../images/emailicon.png');
const USERICON = require('../images/usericon.png');


const {width, height} = Dimensions.get('window')

class SignUpScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fullname: null,
            contact_no: 9199123912,
            email: null,
            password: null,
            confirm_password: null,
            isLoading: false, signUpButtonStatus: false
        }
    }
    async handleSignUp() {
        const { fullname, contact_no, email, password, confirm_password } = this.state;
        var regex = /\S+@\S+\.\S+/;
        if (fullname && contact_no && email && password && confirm_password) {
            if (!regex.test(email)) {
                alert(this.props.language.invalidEmail);
            } else if (contact_no.length <= 8) {
                alert(this.props.language.contact10);
            } else if (password !== confirm_password) {
                alert(this.props.language.passMatch);
            } else {
                this.setState({ isLoading: true, signUpButtonStatus: true})
                const token = await signUp(fullname, contact_no, email, password);
                debugger;
                if (token) {
                    saveDataInAsync('userToken', token);
                    this.props.userSignIn(token);
                    this.setState({ isLoading: false }, () => {
                        this.props.navigation.navigate('Home');
                    })
                } else {
                    this.setState({ isLoading: false, signUpButtonStatus: false }, () => {
                        alert(this.props.language.network)
                    })
                }
            }
        } else {
            alert(this.props.language.detailsReq);
        }
    }

  parentWidth = width-85;
    render() {
        const {isLoading} = this.state
        return (
            <SafeAreaView style={{ backgroundColor: '#000' }}>
            <ScrollView>

                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 15, marginBottom: 15 }}>
                        <Image source={require('../images/logoz.png')} style={styles.logo} />
                    </View>

                    <View style={{ width:width,justifyContent: 'center',backgroundColor:'#fff', borderTopLeftRadius: 30, borderTopRightRadius :30 }}>

                    <View >
                        <Text style={{fontSize: 30, color: 'red',textAlign:'center',marginTop:30}}>{this.props.language.signUpText}</Text>
                    </View>

                    <View style={{alignItems: 'center', marginTop: 30, marginBottom: 10}}>
                        <View style={{flexDirection: 'row', marginRight:width/10}}>
                            <View style={{height:45, width: this.parentWidth+45, position: 'absolute', paddingRight: 10, borderRadius: 30, backgroundColor: 'grey',  alignItems:'flex-end', justifyContent: 'center'}}>
                                <Image style={{height: 40, width: 30, resizeMode:'contain', }} source={USERICON}/>
                            </View>
                            <View style={{borderWidth: 1, backgroundColor: 'white', width: this.parentWidth,borderColor: 'grey',paddingLeft: 15, paddingRight: 15, paddingTop: Platform.OS === 'ios' ? 12 : 0, borderRadius: 30, height: 45 }}>
                                <TextInput onChangeText={(fullname) => this.setState({ fullname })} autoCapitalize='none' placeholder={this.props.language.username} textAlign={'center'} style={styles.TextInput} placeholderTextColor={'grey'} />
                            </View>
                        </View>
                    </View>

                    <View style={{alignItems: 'center', marginTop: 10, marginBottom: 10}}>

                        <View style={{flexDirection: 'row'}}>
                            <View style={{marginLeft:50,zIndex:5,borderWidth: 1, backgroundColor: 'white', width: this.parentWidth,borderColor: 'grey',paddingLeft: 15, paddingRight: 15, paddingTop: Platform.OS === 'ios' ? 12 : 0, borderRadius: 30, height: 45 }}>
                                <TextInput onChangeText={(email) => this.setState({ email })} autoCapitalize='none'
                                           secureTextEntry={false} placeholder={this.props.language.email} textAlign={'center'}
                                           style={styles.TextInput} placeholderTextColor={'grey'} />

                            </View>
                            <View style={{height:45, width: this.parentWidth, position: 'absolute', borderRadius: 30, backgroundColor: 'grey',  alignItems:'flex-start', justifyContent: 'center'}}>
                                <Image style={{height: 40,left: 15, width: 30, resizeMode:'contain', }} source={USEREMAILICON}/>
                            </View>
                        </View>
                    </View>

                    <View style={{alignItems: 'center', marginTop: 10, marginBottom: 10}}>

                        <View style={{flexDirection: 'row'}}>
                            <View style={{marginLeft:50,zIndex:5,borderWidth: 1, backgroundColor: 'white', width: this.parentWidth,borderColor: 'grey',paddingLeft: 15, paddingRight: 15, paddingTop: Platform.OS === 'ios' ? 12 : 0, borderRadius: 30, height: 45 }}>
                                <TextInput onChangeText={(password) => this.setState({ password })} autoCapitalize='none'
                                           secureTextEntry={true} placeholder={this.props.language.password} textAlign={'center'}
                                           style={styles.TextInput} placeholderTextColor={'grey'} />

                            </View>
                            <View style={{height:45, width: this.parentWidth, position: 'absolute', borderRadius: 30, backgroundColor: 'grey',  alignItems:'flex-start', justifyContent: 'center'}}>
                                <Image style={{height: 40,left: 15, width: 30, resizeMode:'contain', }} source={PASSWORDICON}/>
                            </View>
                        </View>
                    </View>

                    <View style={{alignItems: 'center', marginTop: 10, marginBottom: 10}}>

                        <View style={{flexDirection: 'row'}}>
                            <View style={{marginLeft:50,zIndex:5,borderWidth: 1, backgroundColor: 'white', width: this.parentWidth,borderColor: 'grey',paddingLeft: 15, paddingRight: 15, paddingTop: Platform.OS === 'ios' ? 12 : 0, borderRadius: 30, height: 45 }}>
                                <TextInput onChangeText={(confirm_password) => this.setState({ confirm_password })} autoCapitalize='none'
                                           secureTextEntry={true} placeholder={this.props.language.confpassword} textAlign={'center'}
                                           style={styles.TextInput} placeholderTextColor={'grey'} />

                            </View>
                            <View style={{height:45, width: this.parentWidth, position: 'absolute', borderRadius: 30, backgroundColor: 'grey',  alignItems:'flex-start', justifyContent: 'center'}}>
                                <Image style={{height: 40,left: 15, width: 30, resizeMode:'contain', }} source={PASSWORDICON}/>
                            </View>
                        </View>
                    </View>

                    <View style={{alignItems:'center', marginTop: 30}}>
                        <TouchableOpacity disabled={this.state.signUpButtonStatus} onPress={() => this.handleSignUp()}>
                            <View style={{
                                borderRadius: 30, borderWidth: 1, alignItems: 'center', borderColor: 'grey',
                                justifyContent: 'center', height: 40, paddingLeft: 10,
                                paddingRight: 30, width: this.parentWidth + 40}}>
                                {this.state.isLoading?<ActivityIndicator />:
                                    <Text style={{fontSize: 20}}>
                                        {this.props.language.signUpButton}
                                    </Text>}
                            </View>
                        </TouchableOpacity>
                    </View>


                    <View style={{alignItems: 'center'}}>
                    <View style={{ marginTop: 20 , flexDirection: 'row', alignItems: 'center'}}>
                        <View>
                            <Text style={{textAlign: 'center', fontSize: 18, fontWeight: '700', color: '#333', paddingBottom: 10, paddingTop: 10 }}>{this.props.language.haveAccount}</Text>
                        </View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('SignIn')}>
                            <Text style={{ fontSize: 18, fontWeight: '700', textAlign: 'center', color: '#dd0e00' }}>{this.props.language.signIn}</Text>
                        </TouchableOpacity>
                    </View>
                    </View>
                </View>
                {/*<Loader visible={isLoading} />*/}
            </ScrollView>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state) => ({
    signIn: state.authReducer.signIn,
    token: state.authReducer.token,
    language: state.homeReducer.app_language.screens.SignUp,
});
// const mapDispatchToProps = (dispatch) => ({
//     userSignIn: () => dispatch(userSignIn)
// })

export default connect(mapStateToProps, { userSignIn })(SignUpScreen);

const styles = StyleSheet.create({
    logo: { width: 100, height: 100, resizeMode: 'contain', },

    input: { borderWidth: 1, borderColor: 'grey', marginHorizontal: 50, paddingLeft: 15, borderRadius: 15, marginBottom: 15, height: 45,paddingTop: Platform.OS === 'ios' ? 12 : 0, },
    TextInput: { fontSize: 16, color: '#333' },
    button: { marginTop: 30, marginHorizontal: 50, },
    buttonText: {
        borderWidth: 1,
        fontSize: 16,
        fontWeight: '700',
        padding: 12,
        textAlign: 'center',
        color: '#fff',
        borderColor: '#C7B048',
        backgroundColor: '#C7B048',
        borderRadius: 15,
    }
});
