import React, { Component } from "react";
import { View, ActivityIndicator,Text, StyleSheet, AsycStorage, Button, Dimensions,Platform, TouchableOpacity, TextInput, SafeAreaView, Image, ScrollView } from "react-native";
// connect for redux
import { connect } from 'react-redux';
// actions imported
import { userSignIn } from '../redux/actions/actions';
// apiRequest
import { signIn } from '../redux/misc/apiRequest';
// Data storing in async
import { saveDataInAsync } from '../redux/misc/asyncData';
// Loader
import Loader from '../screens/components/Loader';
import {Language} from "./Language";

const {height, width} = Dimensions.get('window');
const PASSWORDICON = require('../images/key3.png');
const USERICON = require('../images/emailicon.png');

class SignInScreen extends Component {
     parentWidth = width-85;
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null,
            isLoading: false,
            goto: null,
            data: null,
        }
        this.setState({ goto: this.props.navigation.getParam('goto', 'NO-DATA') , data : this.props.navigation.getParam('data', 'NO-DATA') })
    }

    async handleSignIn() {
        const { email, password, goto } = this.state;
        var regex = /\S+@\S+\.\S+/;

        if (email && password) {
            if (regex.test(email)) {
                this.setState({ isLoading: true })
                var data = await signIn(email, password);
                console.log(data.token, 'in token')
                if (data.token) {
                    saveDataInAsync('userToken', data.token);
                    saveDataInAsync('name', data.name);
                    saveDataInAsync('email', data.email);
                    saveDataInAsync('phone', data.phone);
                    this.props.userSignIn(data)
                    this.setState({ isLoading: false }, () => {
                        // if (goto  == "NO-DATA") {
                            this.props.navigation.navigate('Home');
                        // } else {
                        //     this.props.navigation.navigate('DeliveryAddress', { data });
                        // }
                    })
                } else {
                    this.setState({ isLoading: false }, () => {
                        alert(this.props.language.incorrectEmailPassword)
                    })
                }
            } else {
                alert(this.props.language.validEmail);
            }
        } else {
            alert(this.props.language.emailPassReq);
        }
    }



    render() {
        const { isLoading } = this.state;
        console.log(this.props.signIn, this.props.token)
        return (
            <SafeAreaView style={{ backgroundColor: '#000' }}>
            <ScrollView>



                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 15, marginBottom: 15 }}>
                        <Image source={require('../images/logoz.png')} style={styles.logo} />
                    </View>

                    <View style={{ width:width,justifyContent: 'center',backgroundColor:'#fff', borderTopLeftRadius: 30, borderTopRightRadius :30 }}>

                    <View>
                    <View>
                        <Text style={{fontSize: 30, color: 'red',textAlign:'center',marginTop:30}}>{this.props.language.loginText}</Text>
                    </View>

                        <View style={{alignItems: 'center', marginTop: 30, marginBottom: 10}}>

                            <View style={{flexDirection: 'row', marginRight:width/10}}>
                                <View style={{height:45, width: this.parentWidth+45, position: 'absolute', paddingRight: 10, borderRadius: 30, backgroundColor: 'grey',  alignItems:'flex-end', justifyContent: 'center'}}>
                                    <Image style={{height: 40, width: 30, resizeMode:'contain', }} source={USERICON}/>
                                </View>
                                <View style={{borderWidth: 1, backgroundColor: 'white', width: this.parentWidth,borderColor: 'grey',paddingLeft: 15, paddingRight: 15, paddingTop: Platform.OS === 'ios' ? 12 : 0, borderRadius: 30, height: 45 }}>
                                    <TextInput onChangeText={(email) => this.setState({ email })} autoCapitalize='none' placeholder={this.props.language.email} textAlign={'center'} style={styles.TextInput} placeholderTextColor={'grey'} />
                                </View>
                            </View>
                        </View>


                        <View style={{alignItems: 'center', marginTop: 10, marginBottom: 10}}>

                            <View style={{flexDirection: 'row'}}>
                                <View style={{marginLeft:50,zIndex:5,borderWidth: 1, backgroundColor: 'white', width: this.parentWidth,borderColor: 'grey',paddingLeft: 15, paddingRight: 15, paddingTop: Platform.OS === 'ios' ? 12 : 0, borderRadius: 30, height: 45 }}>
                                    <TextInput onChangeText={(password) => this.setState({ password })} autoCapitalize='none'
                                               secureTextEntry={true} placeholder={this.props.language.password} textAlign={'center'}
                                               style={styles.TextInput} placeholderTextColor={'grey'} />

                                </View>
                                <View style={{height:45, width: this.parentWidth, position: 'absolute', borderRadius: 30, backgroundColor: 'grey',  alignItems:'flex-start', justifyContent: 'center'}}>
                                    <Image style={{height: 25,left: 15, width: 25, resizeMode:'contain', }} source={PASSWORDICON}/>
                                </View>
                            </View>
                        </View>

                        <TouchableOpacity onPress={()=>false}>
                        <View style={{alignItems: 'flex-end', paddingRight: width/10}}>
                            <Text style={{color: 'grey', fontSize: 15}}>{this.props.language.forgot}</Text>
                        </View>
                        </TouchableOpacity>

                        <View style={{alignItems:'center', marginTop: 30}}>
                            <TouchableOpacity onPress={() => this.handleSignIn()}>
                                <View style={{
                                    borderRadius: 30, borderWidth: 1, alignItems: 'center',
                                    justifyContent: 'center', height: 40, paddingLeft: 10,
                                    paddingRight: 30, width: this.parentWidth + 40}}>
                                    {this.state.isLoading?<ActivityIndicator />:
                                    <Text style={{fontSize: 20}}>
                                        {this.props.language.loginButton}
                                    </Text>}
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View/>
                    <View style={{alignItems: 'center'}}>
                    <View style={{ marginTop: 20 , flexDirection: 'row', alignItems: 'center'}}>
                        <View>
                        <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: '700', color: '#333', paddingBottom: 10, paddingTop: 10 }}>{this.props.language.haveAccount} </Text>
                        </View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('SignUp')}>
                            <Text style={{ fontSize: 18, fontWeight: '700', color: '#dd0e00' }}>{this.props.language.signUp}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                </View>
                {/*<Loader visible={isLoading} />*/}
            </ScrollView>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state) => ({
    signIn: state.authReducer.signIn,
    token: state.authReducer.token,
    language: state.homeReducer.app_language.screens.SignIn
});
// const mapDispatchToProps = (dispatch) => ({
//     userSignIn: () => dispatch(userSignIn)
// })

export default connect(mapStateToProps, { userSignIn })(SignInScreen);

const styles = StyleSheet.create({
    logo: { width: 100, height: 100, resizeMode: 'contain', },

    input: { borderWidth: 1, borderColor: 'black', marginHorizontal: 50, paddingLeft: 15, borderRadius: 30, marginBottom: 15, height: 45,paddingTop: Platform.OS === 'ios' ? 12 : 0, },
    TextInput: { fontSize: 16, color: '#333' },
    button: { marginTop: 30, marginHorizontal: 50, },
    buttonText: {
        borderWidth: 1,
        fontSize: 16,
        fontWeight: '700',
        padding: 12,
        textAlign: 'center',
        color: '#fff',
        borderColor: '#C7B048',
        backgroundColor: '#C7B048',
        borderRadius: 5,
    }
});
