import React, { Component } from "react";
import { View, AsyncStorage, Modal,Text, TouchableOpacity, StyleSheet, Image, Dimensions, ActivityIndicator } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome'
const { height, width } = Dimensions.get('window');
import { Container, Content, Button } from 'native-base';
import {connect} from  'react-redux';
import  axios from 'axios';
//import AxiosInstance from '../axios_instance';

import {
    viewCart, viewWishlist, logout
} from "../redux/actions/actions";
import ModalDropdown from "react-native-modal-dropdown";

class AccountScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userData: {name: '', phone: '', email: ''}, user_id: null, token: null, isLoading: false
        }
    }

    static getDerivedStateFromProps(props, state){
        if(state.userData.name != props.userData.name || state.userData.phone != props.userData.phone){
            return {
                userData : {name: props.userData.name, phone: props.userData.phone, email: props.userData.email}
            }
        }
        return null;
    }

    static navigationOptions = ({ navigation }) => ({
        title: typeof (navigation.state.params) === 'undefined' || typeof (navigation.state.params.title) === 'undefined' ? 'SOmething' : navigation.state.params.title,
        headerLeft: <Icon name={'md-arrow-back'}
                          onPress={ (p) => { navigation.navigate('Home') ; console.log("function is being called ", p)}} style={{marginLeft:10}}/>
    });

    componentDidMount() {
        this.props.navigation.setParams({ title: 'My Account' });
        //this.getUserData({user_id: this.props.token});
    }

    async getUserData(params){
        const url = 'http://cripcoin.co/jewellery/mobileApi/userdata.php';
        await axios.post(url, params)
        //await axios.get(url, {method: 'POST', body: params})
            .then(response=>response.data)
            .then(response=>!response.error?response.userData:null)
            .then((response)=>
                response!=null?
                this.setState({
                    userData: {
                        name: response.c_name,
                        phone: response.c_phone,
                        email: response.c_email
                    },
                    isLoading: false
                }):null
            )
    }

    logout = async() => {
        AsyncStorage.clear();
        this.props.navigation.navigate('AuthLoading')
    }
    accountSetting() {
        this.props.navigation.navigate('AccountSetting', {userData: this.state.userData});
    }
    render() {

            //const {c_email , c_name ,c_phone} = this.state.userData
            //console.log(this.state.userData)
            return (
                <Container>
                    <Modal
                        style={{top: '50%', left: '50%', transform: 'translate(-50%, -50%) !important'}}
                        animationType='fade'
                        transparent={true}
                        onRequestClose={()=>this.setState({isLoading: false})}
                        visible={this.state.isLoading}
                        //onShow={this.resetValues()}
                    >
                        <View style={{flex:1 ,alignItems: 'center', justifyContent: 'center', backgroundColor:'#00000069'}}>
                                <ActivityIndicator color='white' size={'large'} />
                        </View>
                    </Modal>
                    <Content >
                        <View style={styles.container}>

                        <View style={{backgroundColor:'#fff',padding:20}}>
                            <View style={{flexDirection:'row', justifyContent:'flex-start', alignItems: 'center'}}>
                                <View style={{ alignItems: 'center' }}>
                                    <Image style={{ height: 80, width: 80,borderWidth:0.5,borderColor:'#ccc' }} source={require('../images/my_account.png')} />
                                </View>
                                <View style={{marginLeft:15 }}>
                                        <Text style={{ fontSize: 25, color: '#333',fontWeight:'500' }}>{this.state.userData.name}</Text>
                                        <Text style={{ fontSize: 16, color: '#333', paddingVertical: 5, }}>{this.state.userData.phone}</Text>
                                        <Text style={{ fontSize: 14, color: '#333' }}>{this.state.userData.email}</Text>
                                </View>
                            </View>
                        </View>

                        <View style={{backgroundColor:'#fff',padding:20,marginVertical:20}}>
                                <View>
                                    <Text style={{textTransform:'uppercase',color:'#111',fontWeight:'500',fontSize:20}}>{this.props.language.accountInfo}</Text>
                                </View>
                                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                    <View>
                                        <Text style={{fontSize:16,paddingTop:15,color:'#777',fontWeight:'500'}}>{this.props.language.fullname}</Text>
                                        <Text style={{fontSize:16,paddingTop:15,color:'#777',fontWeight:'500'}}>{this.props.language.emailId}</Text>
                                        <Text style={{fontSize:16,paddingTop:15,color:'#777',fontWeight:'500'}}>{this.props.language.address}</Text>
                                    </View>
                                    <View>
                                        <Text style={{fontSize:16,paddingTop:15,color:'#333',fontWeight:'500',textAlign:'right',textTransform:'capitalize'}}>Eric Walters</Text>
                                        <Text style={{fontSize:16,paddingTop:15,color:'#333',fontWeight:'500',textAlign:'right',textTransform:'lowercase'}}>example@gmail.com</Text>
                                        <Text numberOfLines={1} style={{fontSize:16,paddingTop:15,color:'#333',fontWeight:'500',textAlign:'right'}}>3A-35, NIT Faridabad</Text>
                                    </View>
                                </View>
                        </View>

                        <View style={{backgroundColor:'#fff',padding:20,}}>

                                    <View style={{ paddingVertical: 10, backgroundColor: '#fff', flexDirection: 'row', justifyContent: 'space-between', }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View><Icon name='cogs' style={{ fontSize: 18, marginTop: 3, paddingRight: 7, color: '#777' }} /></View>
                                            <TouchableOpacity onPress={() => this.accountSetting()}>
                                                <View><Text style={{ fontSize: 17, color: '#333', }}>{this.props.language.setting}</Text></View>
                                            </TouchableOpacity>
                                        </View>
                                        <View>
                                            <TouchableOpacity style={{ alignItems: 'flex-end' }}>
                                                <Icon onPress={() => this.accountSetting()} name='chevron-right' style={{ color: '#000', fontSize: 16,marginTop:5 }} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                    <View style={{ paddingVertical: 10, backgroundColor: '#fff', flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View><Icon name='list' style={{ fontSize: 18, marginTop: 3, paddingRight: 7, color: '#777' }} /></View>
                                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Orders', {})}>
                                                <Text style={{ fontSize: 17, color: '#333', }}>{this.props.language.orders}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View>
                                            <TouchableOpacity style={{ alignItems: 'flex-end' }} >
                                                <Icon onPress={() => this.myOrder()} name='chevron-right' style={{ color: '#000', fontSize: 16,marginTop:5 }} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                    <View style={{ paddingVertical: 10, backgroundColor: '#fff', flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View><Icon name='sign-out' style={{ fontSize: 18, marginTop: 3, paddingRight: 7, color: '#777' }} /></View>
                                            <TouchableOpacity onPress={() => this.logout()}>
                                                <Text style={{ fontSize: 17, color: '#333', }}>{this.props.language.logout}</Text>
                                            </TouchableOpacity>

                                        </View>
                                        <View>
                                            <TouchableOpacity style={{ alignItems: 'flex-end' }}>
                                                <Icon onPress={() => this.logout()} name='chevron-right' style={{ color: '#000', fontSize: 16,marginTop:5 }} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                            </View>

                        </View>
                    </Content>
                </Container>
            );
    }
}

const mapStateToProps = (state) => ({
    customerAddress: state.customerReducer.customerAddress,
    token: state.authReducer.token,
    gallery: state.productReducer.productGallery,
    relatedProduct: state.productReducer.relatedProduct,
    userData: {name: state.authReducer.name, email: state.authReducer.email, phone:  state.authReducer.phone},
    language: state.homeReducer.app_language.screens.AccountScreen,
});
const mapDispatchToProps = {
    viewWishlist, viewCart,
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountScreen);
//export default AccountScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: '#f7f7f7',
    }
});
