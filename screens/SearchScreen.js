import React, { Component } from "react";
import { View, Text, StyleSheet, Dimensions, Image, ScrollView, TextInput,Platform, TouchableOpacity } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';

import Loader from '../screens/components/Loader';
// connect for redux
import { connect } from 'react-redux';
// actions imported
import { setHomeBannerData, getAllProduct, search } from '../redux/actions/actions';

const { height, width } = Dimensions.get('window')


class SearchScreen extends Component {

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.searchData != prevState.searchData) {
            console.log(nextProps);
            return {
                searchData: nextProps.searchData
            }
        }
        if (nextProps.error !== prevState.error) {
            alert(nextProps.errorMsg)
            return {
                isLoading: false,
                error: nextProps.error
            }
        }

        return null
    }


    constructor(props) {
        super(props);

        this.state = {
            searchData: [] , error:false
        }
    }


    sendSearchData(text) {
        this.setState({ typeKey: text })
        if (text.length > 3) {
            // alert(text)
            let key = { key: text }
            this.props.search(key)
        }
    }

    getRatingStar(rating) {
        var rows = []

        for (i = 1; i <= 5; i++) {

            let fillIconName = "ios-star";

            if (i > rating) {
                fillIconName = "ios-star-outline"
            }

            rows.push(
                <Icon name={fillIconName} size={16} style={{ color: '#FFD715' }} />
            )
        }
        return rows
    }

    render() {
        const { isLoading } = this.props;
        const { searchData } = this.state;
        console.log(searchData, 'this is search data');
        return (
            <ScrollView>
                <View style={{ paddingHorizontal: 10, backgroundColor: '#fff' }}>
                    <View style={{ marginTop: 10, padding: 10 }}>
                        <View style={styles.input}>
                            <TextInput placeholder='Search Products '
                                onChangeText={(key) => this.sendSearchData(key)}
                                value={this.state.typeKey}
                                style={styles.TextInput} placeholderTextColor={'grey'} />
                        </View>
                    </View>
                    <View style={{ marginTop: 20, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                        {searchData.map((data, key) => (
                            <TouchableOpacity key={key} onPress={() => this.props.navigation.navigate('Product', {
                                pro_id: data.pro_id, pro_name: data.pro_title
                            })}
                                style={{ width: width / 2 - 15, height: width / 1.5, borderWidth: 0.5, borderColor: '#bbb', marginBottom: 10, }}>
                                <View style={{ flex: 2 }}>
                                    <Image source={{ uri: 'http://cripcoin.co/jewellery/images/' + data.featured_image }} style={{ flex: 1, height: null, width: null, resizeMode: 'cover', }} />
                                </View>
                                <View style={{ paddingHorizontal: 10, paddingVertical: 8, borderTopWidth: 0.5, borderColor: '#ddd' }}>
                                    <View style={{ alignItems: 'flex-start', }}>
                                        <Text style={{ fontSize: 12, fontWeight: '100' }}>{data.pro_base_category_name}</Text>
                                        <Text numberOfLines={1} style={{ fontSize: 14, color: '#555' }}>{data.pro_title}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', paddingVertical: 2 }}>
                                        {this.getRatingStar(data.pro_rating)}
                                    </View>
                                    <Text style={{ fontSize: 17, color: '#555', fontWeight: '500' }}>{data.pro_final_price}
                                        <Text style={{ textDecorationLine: 'line-through', fontWeight: '200', fontSize: 14, color: '#bbb' }}>
                                            {data.pro_sale_price}</Text>
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        ))}
                    </View>
                    <Loader visible={isLoading} />
                </View>
            </ScrollView>
        );
    }
}

const mapStateToProps = (state) => ({
    isLoading: state.productReducer.isLoading,
    allProducts: state.productReducer.allProducts,
    searchData: state.productReducer.searchData,
    typeKey: state.productReducer.typeKey,
    cartArray: state.cartReducer.cartArray,
    error: state.productReducer.error,
    errorMsg: state.productReducer.errorMsg,
});
const mapDispatchToProps = {
    getAllProduct, search
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);

const styles = StyleSheet.create({
    TextInput: { fontSize: 16, color: '#333' },
    input: { borderWidth: 1, borderColor: 'grey', paddingLeft: 10, borderRadius: 5, marginBottom: 10, height: 45,paddingTop: Platform.OS === 'ios' ? 12 : 0, backgroundColor: '#fff' },
})