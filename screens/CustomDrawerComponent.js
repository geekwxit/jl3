import React, { Component } from "react";
import { Image, View, Text, StyleSheet, AsyncStorage, TouchableOpacity, SafeAreaView, ScrollView, ActivityIndicator, Linking, Modal, Alert } from "react-native";
import { DrawerActions } from 'react-navigation'

import Icon from 'react-native-vector-icons/MaterialIcons';
import PlusIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { logout } from '../redux/actions/actions';
class DrawerComponent extends Component {

    //static getDerived

    constructor(props) {
        super(props);
        console.log('i m evil constructor');
        this.state = {
            name : null
        }
    }

    async logout() {
        let keys = ['userToken', 'name', 'email', 'phone'];

        Alert.alert(this.props.language.logoutAlertTitle,this.props.language.logoutMsg,
            [
                {text: this.props.language.logoutCancelButton, onPress: () => console.log('Cancel Pressed')},
                {text: this.props.language.logoutOkButton, onPress: async () => {
                    try {
                        await AsyncStorage.multiRemove(keys, (e) => {alert(this.props.language.logoutSuccess);})
                        this.props.logout()
                        this.setState({name:null})
                    }catch (e) {
                        console.log('error in removing key', e)
                    }
                }}
            ],
            { cancelable: false },
        );
    }


    render() {

        const { name } = this.props
        return (
            <SafeAreaView style={styles.container}>
                <View style={{ flexDirection: 'column', borderBottomWidth: 0.5, marginLeft: 12, borderColor: '#888', marginBottom: 15 }}>
                    <View>
                        <Image source={require('../images/logo.png')} style={{ height: 70, width: 200, resizeMode: 'contain', marginVertical: 15, marginRight: 20 }} />
                    </View>


                    <View style={{ marginBottom: 20 }}>
                        <Text style={{ fontSize: 20, color: "#1c2f52" }}>
                            {this.props.language.greetText}  {name ? name.charAt(0).toUpperCase() + name.slice(1) : 'Guest'}
                                </Text>
                    </View>

                    <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                        <View style={{ justifyContent: 'center' }}>
                            <TouchableOpacity
                                style={[{ flexDirection: 'row' }]}
                                onPress={() => this.props.navigation.navigate('Home')}>
                                {/* <Image source={require('../images/i6.png')}
                                        style={[{ height: 18, width: 18, marginTop: 4 },
                                        ]} /> */}
                                <Text style={[styles.drawerLink]}>{this.props.language.home}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
                <View style={{ marginLeft: 12, marginTop: 3 }}>

                    <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                        <View style={{ justifyContent: 'center' }}>
                            <TouchableOpacity
                                style={[{ flexDirection: 'row' }]}
                                onPress={() => this.props.navigation.navigate('Wishlist')}>
                                {/* <Image source={require('../images/i6.png')}
                                        style={[{ height: 18, width: 18, marginTop: 4 },
                                        ]} /> */}
                                <Text style={[styles.drawerLink]}>{this.props.language.wishlist}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                        <View style={{ justifyContent: 'center' }}>
                            <TouchableOpacity
                                style={[{ flexDirection: 'row' }]}
                                onPress={() => this.props.navigation.navigate('Cart')}>
                                {/* <Image source={require('../images/i2.png')}
                                            style={[{ height: 18, width: 18, marginTop: 4 },
                                            ]} /> */}
                                <Text style={[styles.drawerLink]}>{this.props.language.cart}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                        <View style={{ justifyContent: 'center' }}>
                            <TouchableOpacity
                                style={[{ flexDirection: 'row' }]}
                                onPress={() => this.props.navigation.navigate('Shop')}>
                                {/* <Image source={require('../images/i2.png')}
                                            style={[{ height: 18, width: 18, marginTop: 4 },
                                            ]} /> */}
                                <Text style={[styles.drawerLink]}>{this.props.language.shop}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                        <View style={{ justifyContent: 'center' }}>
                            <TouchableOpacity
                                style={[{ flexDirection: 'row' }]}
                                onPress={() => this.props.navigation.navigate('Orders')}>
                                {/* <Image source={require('../images/i2.png')}
                                            style={[{ height: 18, width: 18, marginTop: 4 },
                                            ]} /> */}
                                <Text style={[styles.drawerLink]}>{this.props.language.orders}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    {this.props.token != null && this.props.token != '' ?
                        (<View style={{justifyContent: 'flex-start', alignItems: 'flex-start'}}>
                            <View style={{justifyContent: 'center'}}>
                                <TouchableOpacity
                                    style={[{flexDirection: 'row'}]}
                                    onPress={() => this.props.navigation.navigate('Account')}>
                                    {/* <Image source={require('../images/i2.png')}
                                            style={[{ height: 18, width: 18, marginTop: 4 },
                                            ]} /> */}
                                    <Text style={[styles.drawerLink]}>{this.props.language.account}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>)
                        :null
                    }
                </View>
                <ScrollView>

                    { this.props.guest_id && !this.props.token && <TouchableOpacity
                        style={{ paddingVertical: 10, marginTop: 10, borderTopWidth: 0.5, borderColor: '#ccc', alignItems: 'center' }}
                        onPress={() => this.props.navigation.navigate('SignIn', {
                            fromPage: 'drawer'
                        })}>
                        <Text style={styles.logBtn}>
                            {this.props.language.signInButton}
                            </Text>
                    </TouchableOpacity>
                    }



                    {this.props.token &&
                        <View>
                            <TouchableOpacity
                                style={{ paddingVertical: 10, marginTop: 10, borderTopWidth: 0.5, borderColor: '#ccc', alignItems: 'center' }}
                                onPress={() => this.logout()}>
                                <Text style={styles.logBtn}>
                                    {this.props.language.logoutButton}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    }



                    {/* <View>
                        <TouchableOpacity
                            style={{ paddingVertical: 10, marginTop: 10, borderTopWidth: 0.5, borderColor: '#ccc', alignItems: 'center' }}
                            onPress={() => this.props.navigation.navigate('Webview')}>
                            <Text style={{
                                color: '#fff', backgroundColor: '#1c2f52', paddingHorizontal: 10, paddingBottom: 7,
                                paddingTop: 5, fontSize: 15, fontWeight: '500', borderRadius: 5, textAlign: 'center'
                            }}>
                                Join As A Vendor
                                </Text>
                        </TouchableOpacity>
                    </View> */}
                    {/* <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' , marginVertical:10}}>
                            <Image source={require('../images/offer.png')} style={{ width: 110, height: 100 }} />
                        </View> */}
                </ScrollView>
            </SafeAreaView>
        );
    }
}


const mapStateToProps = (state) => ({
    token: state.authReducer.token,
    guest_id: state.authReducer.guest_id,
    name: state.authReducer.name,
    email: state.authReducer.email,
    phone : state.authReducer.phone,
    language: state.homeReducer.app_language.screens.Drawer,
});
const mapDispatchToProps = {
    logout
}
export default connect(mapStateToProps, mapDispatchToProps)(DrawerComponent);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    drawerLink: {
        paddingHorizontal: 15,
        paddingBottom: 7,
        fontSize: 18,
        color: '#333',
    },
    drawerLinkInner: {
        paddingBottom: 7,
        fontSize: 16,
        color: '#777',
    },
    logBtn: {
        color: '#fff', backgroundColor: '#1c2f52', paddingHorizontal: 10, paddingBottom: 7, paddingTop: 5, fontSize: 15, fontWeight: '500', width: 110,
        borderRadius: 5, textAlign: 'center'
    }
});
