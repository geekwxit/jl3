import React, { Component } from "react";
import { View,Text,StyleSheet,ScrollView, TouchableOpacity ,TextInput	} from "react-native";
import { tsImportEqualsDeclaration } from "@babel/types";
import { Textarea, } from "native-base";
class EditDeliveryAddress extends Component {

constructor(){
    super();
    this.state = {
        name:'hi',
        pass:''
    }
}

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>

                <View style={{marginHorizontal:20,marginTop:20}}>
                <Text style={{color:'#333',fontSize:20,fontWeight:'400',textAlign:'center',marginBottom:20}}>Change  Address</Text>
                    <View>
                        <Text>Address</Text>
                        <View>
                            <Textarea rowSpan={3} bordered placeholder="Enter Address" />
                        </View>
                    </View>
                    <View style={{marginTop:20}}>
                        <Text>Street / City</Text>
                        <View>
                        <Textarea rowSpan={3} bordered placeholder="Enter " />
                        </View>
                    </View>
                    

                    <View style={{marginTop:20}}>
                        <Text>Landmark</Text>
                        <View>                       
                            <TextInput placeholder='Place near by you' style={{borderBottomWidth:0.5,borderBottomColor:'#bbb',marginBottom:20}}/>
                        </View>
                    </View>

                    <View style={{marginTop:20}}>
                        <Text>Zip Code</Text>
                        <View>                       
                            <TextInput placeholder='Enter Zip Code' style={{borderBottomWidth:0.5,borderBottomColor:'#bbb',marginBottom:20}}/>
                        </View>
                    </View>


                    <View style={{flexDirection:'row',justifyContent:"space-around",marginTop:20}}>
                        <View style={{width:'50%'}}>
                            <TouchableOpacity style={{marginTop:0}}>
                                <Text style={{color:'#777',fontSize:17,fontWeight:'500',textAlign:'center'}} >CANCEL</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{width:'50%'}}>
                            <TouchableOpacity style={{marginTop:0}}>
                                <Text style={{color:'#0068f7',fontSize:17,fontWeight:'500',textAlign:'center'}} >SAVE</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
 
 

               




                </ScrollView>
            </View>
        );
    }
}
export default EditDeliveryAddress;

const styles = StyleSheet.create({
    container: {
        flex: 1, 

    }
});