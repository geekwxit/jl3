import React, { Component } from "react";
import { View, Text, Dimensions, Image, FlatList, Picker, TouchableOpacity, StyleSheet, ScrollView } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import ModalDropdown from 'react-native-modal-dropdown';
import {Toast} from './components/Toast';
import Loader from '../screens/components/Loader';
import { addToCart, addToWishlist, removeFromCart, removeQuantity, addQuantity, changeQuantity, viewCart } from '../redux/actions/actions';

import { connect } from 'react-redux';

const { height, width } = Dimensions.get('window');
const DEMO_OPTIONS_1 = ['  1  ', '  2  ', '  3  ', '  4  ', '  5  '];

// class Product extends Component {

//     state = { number: '' }
//     updateNumber = (number) => {
//         this.setState({ number: number })
//     }

//     render() {
//         return (
//             <View style={{ marginVertical: 9, backgroundColor: '#ffffff', elevation: 2, paddingTop: 0, borderTopWidth: 0.5, borderColor: '#ccc' }}>
//                 <View style={{ flexDirection: 'row', height: width / 3.7, paddingHorizontal: 0, }}>
//                     <View style={{ flex: 2.5, padding: 10 }}>
//                         <Image source={this.props.Gimg} style={{ flex: 1, height: null, width: null, resizeMode: 'contain', }} />
//                     </View>
//                     <View style={{ flex: 8, alignItems: 'flex-start', paddingLeft: 10, paddingVertical: 10 }}>
//                         <View >
//                             <Text numberOfLines={1} style={{ fontSize: 16, color: '#333', paddingRight: 10 }}>{this.props.proName}</Text>
//                         </View>

//                         <View style={{ flexDirection: 'row', marginTop: 7 }}>
//                             <Icon name="md-star" size={16} style={{ color: '#FFD715' }} />
//                             <Icon name="md-star" size={16} style={{ color: '#FFD715' }} />
//                             <Icon name="md-star" size={16} style={{ color: '#FFD715' }} />
//                             <Icon name="md-star" size={16} style={{ color: '#FFD715' }} />
//                             <Icon name="md-star" size={16} style={{ color: '#FFD715' }} />
//                         </View>

//                         <View style={{ paddingTop: 7 }}>
//                             <Text style={{ fontSize: 17, color: '#555', fontWeight: '500' }}> ${this.props.proPrice} <Text style={{ textDecorationLine: 'line-through', fontWeight: '200', fontSize: 14, color: '#aaa' }}> $999</Text></Text>
//                         </View>
//                     </View>

//                 </View>

//                 <View style={{ flexDirection: 'row', borderWidth: 0.5, borderColor: '#ccc' }}>
//                     <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center' }}>
//                         <Picker style={{ width: 100, height: 20, textAlign: 'center' }} selectedValue={this.state.number} onValueChange={this.updateNumber}>
//                             <Picker.Item label="   1" value="1" />
//                             <Picker.Item label="   2" value="2" />
//                             <Picker.Item label="   3" value="3" />
//                         </Picker>
//                     </View>
//                     <View style={{ width: '50%', }}>
//                         <TouchableOpacity style={{ paddingVertical: 10, borderLeftWidth: 0.5, borderColor: '#bbb' }}>
//                             <Text style={{ fontSize: 16, fontWeight: '400', textAlign: 'center', color: '#888' }}><Icon name="md-trash" size={18} style={{ color: '#888', }}></Icon>  Remove</Text>
//                         </TouchableOpacity>
//                     </View>
//                 </View>
//             </View>
//         );
//     }
// }


class CartScreen extends Component {
    //user_id = null; guest_id = null;

    static getDerivedStateFromProps(nextProps, prevState) {
        console.log("get dervied state is called: NEXTPROPS: ", nextProps);

        console.log("get dervied state is called: PREVSTAATE:", prevState);

        if (nextProps.cartArray !== prevState.cartArray) {
            nextProps.navigation.setParams({
                productOnCart: nextProps.cartArray.length
            })

            return {
                cartArray: nextProps.cartArray
            }
        }
        return null
    }

    constructor(props) {
        super(props);
        guest_id = this.props.navigation.getParam('guest_id', null);
        user_id = this.props.navigation.getParam('user_id', null);
        this.state = {
            cartData: [], cartArray: [], totalPrice: 0, amountPayable: 0 ,toastVisible : false,toastText : '',
            guest_id : guest_id, user_id: user_id
        }
        this.viewCart();
    }
    makeToast(text){
        this.setState({toastText: text})
        this.setState({toastVisible: true});
        setTimeout(()=>this.closeToast(), 2000);
    }
    closeToast(){
        this.setState({toastVisible: false});
    }
    viewCart(){
        this.state.user_id?this.props.viewCart(
            JSON.stringify({user_id: this.state.user_id})):
            this.props.viewCart(JSON.stringify({guest_id: this.state.guest_id}));
    }

    getRatingStar(rating) {
        var rows = []

        for (i = 1; i <= 5; i++) {

            let fillIconName = "ios-star";

            if (i > rating) {
                fillIconName = "ios-star-outline"
            }

            rows.push(
                <Icon name={fillIconName} size={16} style={{ color: '#FFD715' }} />
            )
        }
        return rows
    }
    getPrice() {
        var price = 0
        this.state.cartArray.map((data) => {
            price += data.c_quantity * data.pro_sale_price;
            //price += Number(data.pro_sale_price)
        })
        return price
    }
    getPayableAmount(amount) {
        var amountPayable = amount + 99;
        // this.setState({ amountPayable: amountPayable })
        return amountPayable
    }


    updateQuantity(newQuantity, pro_id, prevQuantity, index){
        const product = {product_id: pro_id, qty: newQuantity}
        if(this.props.token!=null && this.props.token != ''){
            product.user_id = this.props.token;
        }
        else if(this.state.guest_id!=null && this.state.guest_id != ''){
            product.guest_id = this.state.guest_id;
        }
        console.log("parsing Int with something: somequan_>", prevQuantity, "newquan>", newQuantity, "parsedIUnt:",parseInt(prevQuantity)+parseInt(newQuantity), parseInt(prevQuantity)+parseInt(newQuantity)<=0)
        if(parseInt(prevQuantity)+parseInt(newQuantity)<=0){
            this.props.removeFromCart(
                JSON.stringify(product), index, newQuantity).
            then(()=>
                this.makeToast(this.props.language.productRemovedFromCart)
            );
        }
        else {
            this.props.changeQuantity(
                JSON.stringify(product), index, newQuantity).
            then(()=>
                this.makeToast(this.props.language.productQuantity)
            );
        }
        console.log(product);
    }

    render() {
        //debugger;
        const { cartData, cartArray } = this.state;
        console.log('cardData inside render', cartArray)

        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <Toast onRequestClose={()=>this.closeToast()} text={this.state.toastText} visible={this.state.toastVisible}/>
                {cartArray.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text
                            style={{
                                fontSize: 20, fontWeight: 'bold',

                            }}>{this.props.language.emptyCart}</Text>
                    </View> :
                    <View style={{ flex: 1 }}>

                        <ScrollView>
                            <View  style={{marginHorizontal:10,}}>
                                <FlatList
                                    data={cartArray}
                                    extraData={{ value: [this.state.pickValue0, this.state.pickValue1] }}
                                    keyExtractor={(item, key) => item.pro_id}
                                    renderItem={({ item, index }) =>

                                        <View style={{backgroundColor: '#ffffff', elevation: 2, paddingTop: 0, borderBottomWidth: 0.5, borderColor: '#ccc' }}>

                                            <View style={{ flexDirection: 'row', height: width /3, paddingHorizontal: 0,marginTop:10 }}>

                                                <View style={{ width:'35%',}}>
                                                    <Image source={{ uri: 'http://cripcoin.co/jewellery/images/' + item.featured_image }}
                                                        style={{ flex: 1, height: null, width: null, resizeMode: 'cover', }} />
                                                </View>

                                                <View style={{ width:'65%',paddingLeft: 10,}}>

                                                    <View style={{ flexDirection: 'row',justifyContent:'space-between',marginTop:7}}>
                                                        <View style={{ width: '80%', }}>
                                                            <Text numberOfLines={1} style={{ fontSize: 16, color: '#333', paddingRight: 10 }}>{item.pro_title}</Text>
                                                        </View>
                                                        <View>
                                                            <TouchableOpacity onPress={() => this.updateQuantity(-item.c_quantity, item.pro_id, item.c_quantity, index)}>
                                                                 <Icon name="md-trash" size={25} style={{ color: '#aaa',borderRadius:50 }}></Icon>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>

                                                    <View style={{ flexDirection: 'row',justifyContent:'space-between',marginTop:12}}>
                                                        <View>
                                                            <Text style={{ fontSize: 17, color: '#555', fontWeight: '500' }}>${item.pro_sale_price} <Text style={{ textDecorationLine: 'line-through', fontWeight: '200', fontSize: 14, color: '#aaa' }}> ${item.pro_final_price}</Text></Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', marginTop:5}}>
                                                            {this.getRatingStar(item.pro_rating)}
                                                        </View>
                                                    </View>


                                                    <View style={{justifyContent: 'space-between', flexDirection: 'row', width:'50%',marginTop:15}}>
                                                        <TouchableOpacity onPress={() => { this.updateQuantity(1, item.pro_id, item.c_quantity, index)}}>
                                                            <Icon name={'md-add-circle'} size={25} />
                                                        </TouchableOpacity>
                                                        <Text style={{ fontSize: 20 }}>{item.c_quantity ? item.c_quantity : '1'}</Text>
                                                        <TouchableOpacity onPress={() => { this.updateQuantity(-1, item.pro_id, item.c_quantity, index)}}>
                                                            <Icon name={'md-remove-circle'} size={25} />
                                                        </TouchableOpacity>
                                                    </View>

                                                </View>


                                            </View>
                                        </View>
                                    }
                                />
                            </View>

                        </ScrollView>

                        {cartArray.length > 0 &&
                        <View>

                            <View style={{ marginVertical: 10, paddingHorizontal: 10 }}>
                                    <View><Text style={{ fontSize: 18, color: '#777', fontWeight: '500' }}>{this.props.language.priceDetails}</Text></View>
                                    <View style={{ borderTopWidth: 0.5, borderColor: '#ccc', marginVertical: 5 }} />
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5 }}>
                                        <View>
                                            <Text style={{ fontSize: 16, color: '#777', }}>{this.props.language.price}</Text>
                                        </View>
                                        <View>
                                            <Text style={{ fontSize: 16, color: '#777', }}>${this.getPrice()}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5 }}>
                                        <View>
                                            <Text style={{ fontSize: 16, color: '#777', }}>{this.props.language.delivery}</Text>
                                        </View>
                                        <View>
                                            <Text style={{ fontSize: 16, color: '#777', }}>$99</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between',}}>
                                        <View>
                                            <Text style={{ fontSize: 16, color: '#333', fontWeight: '500' }}>{this.props.language.amountPayable}</Text>
                                        </View>
                                        <View>
                                            <Text style={{ fontSize: 16, color: '#333', fontWeight: '500' }}>${this.getPayableAmount(this.getPrice())}</Text>
                                        </View>
                                    </View>
                                </View>

                            <View style={{ paddingVertical: 7, paddingHorizontal: 7, flexDirection: 'row', borderTopWidth: 0.5, borderColor: '#888' }}>
                                <View style={{ justifyContent: 'center', width: '50%', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 18, color: '#2f5e6a', fontWeight: '700' }}>{this.props.language.total}: <Text style={{ color: '#333' }}>${this.getPayableAmount(this.getPrice())}</Text></Text>
                                </View>
                                <View style={{ width: '50%', }}>
                                    <TouchableOpacity
                                        onPress={() => this.checkout()}
                                        style={{ backgroundColor: '#C7B048', paddingVertical: 12 }}>
                                        <Text style={{ fontSize: 18, fontWeight: '700', textAlign: 'center', color: '#fff' }}> {this.props.language.checkout}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            </View>
                        }
                    </View>}

            </View>
        );

    }

    checkout(){
        if (this.props.token){
            if(this.props.cartArray.length>0){
                this.props.navigation.navigate('DeliveryAddress');
            }
            console.log(this.props.cartArray);
            //if(this.props.cartArray)
            // if (this.props.customerAddress.length <= 0 && !this.props.success) {
            //     this.props.navigation.navigate('DeliveryAddress', { data: "NO-DATA", quantity: '' });
            // }
            // else{
            //     this.props.navigation.navigate('DeliveryDlts', { data: "NO-DATA", quantity: '' });
            // }
        }
        else{
            this.props.navigation.navigate('SignIn', { data: "NO_DATA" })
        }
    }
}

const mapStateToProps = (state) => ({
    isLoading: state.customerReducer.isLoading,
    allProducts: state.productReducer.allProducts,
    cartArray: state.cartReducer.cartArray,
    customerAddress: state.customerReducer.customerAddress,
    wishlistArray: state.cartReducer.wishlistArray,
    error: state.customerReducer.error,
    errorMsg: state.customerReducer.errorMsg,
    success: state.customerReducer.success,
    token: state.authReducer.token,
    language: state.homeReducer.app_language.screens.ProductScreen,
});
const mapDispatchToProps = {
    addToCart, addToWishlist, removeFromCart, removeQuantity, addQuantity, viewCart, changeQuantity,
}
export default connect(mapStateToProps, mapDispatchToProps)(CartScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 30,
    },
    pickerStyle: {
        width: 100,
        height: 40
    },
    cell: {
        flex: 1,
        borderWidth: StyleSheet.hairlineWidth,
    },

    dropdown_1: {
        flex: 1,
        top: 32,
        left: 8,
    },
});
