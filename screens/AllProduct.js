import React, { Component } from "react";
import { View, Text, StyleSheet, Dimensions, Image,ScrollView,FlatList,TouchableOpacity, AsyncStorage } from "react-native";
import {Container,Content,Icon } from 'native-base';


const {height,width} = Dimensions.get('window')

class AllProduct extends Component {
    static navigationOptions = ({ navigation }) => {
        return{
          title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,  
        };
      };
    constructor(props){
        super(props)
        this.state={dataOfProduct:[]}
      }
    componentDidMount(){
        this.props.navigation.setParams({ title: "All Product"})
        
        const url='http://cripcoin.co/jewellery/mobileApi/all-product.php';

        fetch(url, {
            method: 'POST',
            headers: { 'Accept': 'application/json','Content-Type': 'application/json',},
        })
        .then((response) => response.json())
        .then((responseData) => { 
                console.log("responseData.product",responseData.all_product);
                if(responseData.all_product!=''){
                    this.setState({
                        dataOfProduct:responseData.all_product
                    })
                }
        }) 
        .catch((error) => {
            console.log("Error");
        });
                
     }
     _goToProductPage(pro_id){
         this.props.navigation.navigate('product',{
            pro_id:pro_id
         })
     }
    render() {
        const {dataOfProduct} =this.state;
        return (
            <Container >
              <Content>
              <View style={{marginTop:20}}>
                    <ScrollView>
                        <FlatList
                            data={dataOfProduct}
                            numColumns={2}
                            renderItem={({item}) =>
                            <View style={{width:width / 2 - 20, height:(width / 1.5),marginLeft:10, marginRight:10,marginBottom:20, borderWidth:0.5,borderColor:'#ddd'}}>
                                                        <View style={{}}>
                                                            <TouchableOpacity onPress={() => this._goToProductPage(item.pro_id)}> 
                                                                <Image
                                                                    source={{uri: 'http://cripcoin.co/jewellery/images/'+item.featured_image}}
                                                                    style={{height:width / 1.5-80,width:width / 2 - 20,}}      
                                                                />
                                                            </TouchableOpacity>
                                                        </View>
                                                        <View style={{alignItems:'center',marginTop:15}}>
                                                            <Text style={{color:'#000000'}}>{item.pro_title} </Text>
                                                        </View>
                                                        <View style={{alignItems:'center',marginTop:15}}>
                                                            <Text style={{color:'#000000'}}>${item.pro_final_price} </Text>
                                                        </View>
                                                    </View>
                                      }
                            />
                    </ScrollView>
                </View>
              </Content>
            </Container>
        );
    }
}
export default AllProduct                                              ;

 