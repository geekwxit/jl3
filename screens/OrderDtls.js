import React, { Component } from "react";
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity, TextInput, ScrollView } from 'react-native';


const { height, width } = Dimensions.get('window');

class OrderDetails extends Component {
    render() {
        var orderDetails = this.props.navigation.getParam('orderDetails', null)
        console.log(orderDetails)
        if (orderDetails) {
            return (
                <View style={styles.container}>
                    <ScrollView>
                        <View>
                            <View>
                                {orderDetails.product.map((productData, key) => (
                                    <View key={key} style={{ width: width, height: width / 5.5, backgroundColor: '#fff', flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <View style={{ flex: 4, }}>
                                            <Image source={{ uri: 'http://cripcoin.co/jewellery/images/' + productData.featured_image }}
                                                style={{ flex: 1, height: null, width: null, resizeMode: 'contain', marginTop: 7 }} />
                                        </View>
                                        <View style={{ flex: 8, paddingLeft: 10 }}>
                                            <View>
                                                <Text style={{ fontSize: 18, color: '#333', paddingRight: 10 }}
                                                    numberOfLines={1} >{productData.pro_title}</Text>
                                            </View>
                                            <View style={{ marginTop: 5 }}>
                                                <Text style={{ fontSize: 16, color: '#777', fontWeight: '500', }}>{productData.pro_base_category_name}</Text>
                                            </View>
                                            <View style={{ marginTop: 5 }}>
                                                <Text style={{ fontSize: 14, color: '#333', }}>Qty: 1</Text>
                                            </View>
                                        </View>
                                        <View style={{ flex: 3, }}>
                                            <Text style={{ fontSize: 18, color: '#cc0000', fontWeight: '500', }}>${productData.pro_final_price}</Text>
                                        </View>
                                    </View>
                                ))}
                            </View>
                            {/* above product section */}
                            <View style={{ marginVertical: 15, marginHorizontal: 10, backgroundColor: '#fff' }}>
                                <View><Text style={{ fontSize: 18, color: '#555', fontWeight: '500' }}>Price Details</Text></View>
                                <View style={{ borderTopWidth: 0.5, borderColor: '#ccc', marginVertical: 5 }} />
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5 }}>
                                    <View>
                                        <Text style={{ fontSize: 16, color: '#777', }}>Price</Text>
                                    </View>
                                    <View>
                                        <Text style={{ fontSize: 16, color: '#777', }}>${orderDetails.ord_total_price}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5 }}>
                                    <View>
                                        <Text style={{ fontSize: 16, color: '#777', }}>Delivery</Text>
                                    </View>
                                    <View>
                                        <Text style={{ fontSize: 16, color: '#777', }}>{orderDetails.ord_shipping_charge}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5 }}>
                                    <View>
                                        <Text style={{ fontSize: 16, color: '#333', fontWeight: '500' }}>Total Amount</Text>
                                    </View>
                                    <View>
                                        <Text style={{ fontSize: 16, color: '#333', fontWeight: '500' }}>${orderDetails.ord_final_price}</Text>
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 30 }}>
                                    <View><Text style={{ fontSize: 18, color: '#555', fontWeight: '500' }}>Order Date</Text></View>
                                    <View><Text style={{ fontSize: 16, color: '#333', paddingTop: 2 }}>{orderDetails.ord_date}</Text></View>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                    <View><Text style={{ fontSize: 18, color: '#555', fontWeight: '500' }}>Delivery Date</Text></View>
                                    <View><Text style={{ fontSize: 16, color: '#333', paddingTop: 2 }}>12-June-2019</Text></View>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 30 }}>
                                    <View><Text style={{ fontSize: 18, color: '#555', fontWeight: '500' }}>Order Status</Text></View>
                                    <View><Text style={{ fontSize: 16, color: '#cc0000', paddingTop: 2 }}>{orderDetails.ord_status}</Text>
                                        {/* <Text style={{ fontSize: 16, color: 'green', paddingTop: 2 }}>Delivered</Text></View> */}
                                    </View>

                                    {/*
                                        <View>
                                            <View style={{ marginTop: 30 }}>
                                                <Text style={{ fontSize: 18, color: '#555', fontWeight: '500' }}>Recipient's Address:</Text>
                                                <Text style={{ fontSize: 16, color: '#333', paddingTop: 5, textAlign: 'justify' }}>Nostrud duis non occaecat exercitation consequat enim. excepteur anim cillum consequat. Duis minim duis ad aliquip ad dolore.</Text>
                                            </View>
                                            <View style={{ marginTop: 15 }}>
                                                <Text style={{ fontSize: 18, color: '#555', fontWeight: '500' }}>Recipient's Contact Number:</Text>
                                                <Text style={{ fontSize: 16, color: '#333', paddingTop: 5, textAlign: 'justify' }}>+91 851886518</Text>
                                            </View>
                                        </View> */}


                                </View>
                            </View>
                        </View>
                    </ScrollView>

                </View>
            );
        }
    }
}
export default OrderDetails;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff', paddingTop: 20
    }
});
