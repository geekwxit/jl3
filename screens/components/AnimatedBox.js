import React, {Component} from 'react';
import {Text, View, Animated, TouchableWithoutFeedback, StyleSheet, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

export default class AnimatedBox extends Component {

    description = {heightValue: new Animated.Value(0), isShowing: false};

    toggleDescription() {
        Animated.timing(
            this.description.heightValue,
            {
                toValue: this.description.isShowing ? 0 : 300,
                duration: 500
            }
        ).start(() => this.description.isShowing = !this.description.isShowing);
        // this.setState({
        //     description : {isShowing: !this.state.description.isShowing}
        // })
        //debugger;
    }


    render() {
        return (
            <View>
                <TouchableWithoutFeedback onPress={() => this.toggleDescription()}>
                    <View style={styles.box}>
                        <Text>{this.props.title}</Text>
                        <Text>&gt;</Text>
                    </View>
                </TouchableWithoutFeedback>
                <Animated.View style={{
                    height: this.description.heightValue,
                    width: width - 20
                }} desc="Text to show">
                    {this.props.children}
                </Animated.View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    box: {
    flexDirection: 'row',
    //backgroundColor: 'rgba(248,162,82,0.77)',
    width: width - 20,
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
    padding: 10
}});

