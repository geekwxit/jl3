import React, {Component} from 'react';
import {View,Text,Modal, Dimensions} from 'react-native'

const {width, height}  = Dimensions.get('window');

export const Toast=(props)=>{   
    return (
        <Modal onRequestClose={()=>props.onRequestClose()} style={{top: '50%', left: '50%', transform: 'translate(-50%, -50%) !important'}} visible={props.visible} animationType='fade' transparent={true}> 
                <View style={{ flex: 1, alignItems: 'center',top:width*(3/5), justifyContent:'center' }}>
                    <View style={{padding: 10, paddingRight: 15, paddingLeft:15, backgroundColor: '#6e6e6e', borderRadius: 20}}>
                        <Text style={{color: 'white'}}>{props.text}</Text>
                    </View>
                </View>
        </Modal>
    )
}