import React, { Component } from "react";
import { View, Text, StyleSheet, Dimensions, Image, ScrollView, TouchableOpacity, Modal, SafeAreaView } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
const { height, width } = Dimensions.get('window')

class ShopScreen extends Component {

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.allProducts != prevState.allProducts) {
            return {
                allProducts: nextProps.allProducts
            }
        }
        return null
    }
    state = {
        filter: 'all',
        sort: 'all',
        allProducts: [],
        sortingModal: false,
        filterModal: false
    }

    getRatingStar(rating) {
        var rows = []

        for (i = 1; i <= 5; i++) {

            let fillIconName = "ios-star";

            if (i > rating) {
                fillIconName = "ios-star-outline"
            }

            rows.push(
                <Icon name={fillIconName} size={16} style={{ color: '#FFD715' }} />
            )
        }
        return rows
    }

    setSorting(param) {
        if (param == 'l2h') {
            const sortedArray = this.props.allProducts.sort((a, b) => (a.pro_final_price - b.pro_final_price))
            this.setState({ sort: param, allProducts: [...sortedArray], sortingModal: false })

        } else if (param == 'h2l') {
            const sortedArray = this.props.allProducts.sort((a, b) => (b.pro_final_price - a.pro_final_price))
            this.setState({ sort: param, allProducts: [...sortedArray], sortingModal: false })
        } else if (param == 'all') {
            this.setState({ sort: param, allProducts: [...this.props.allProducts], sortingModal: false })
        }
    }

    render() {

        // const { allProducts } = this.props
        const { allProducts, sortingModal, filterModal } = this.state

        return (
            <SafeAreaView>
                {/* <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.setState({ filterModal: true })}>
                        <Text>Filter</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({ sortingModal: true })}>
                        <Text>Sorting</Text>
                    </TouchableOpacity>
                </View> */}


                <View style={{flexDirection:'row',justifyContent:'space-around',borderBottomColor:'#ccc',borderBottomWidth:1,marginBottom:5}}>
                    <TouchableOpacity onPress={() => this.setState({ sortingModal: true })} style={{justifyContent:'center',height:45,borderRightColor:'#ccc',borderRightWidth:0.5,width:'49.93%',alignItems:'center'}}>
                        <View style={{flexDirection:'row'}}>
                            <View>
                                <Image source={require('../images/sort.png')} style={{height:13,width:15,marginTop:3,marginRight:10,resizeMode:'stretch'}} />
                            </View>
                            <View><Text style={{color:'#333',fontWeight:'500'}}>{this.props.language.sortLabel}</Text></View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({ filterModal: true })} style={{justifyContent:'center',height:45,borderLeftColor:'#ccc',borderLeftWidth:0.5,width:'49.93%',alignItems:'center'}}>
                        <View style={{flexDirection:'row'}}>
                            <View>
                                <Image source={require('../images/filter.png')} style={{height:15,width:15,marginTop:3,marginRight:10}} />
                            </View>
                            <View><Text style={{color:'#333',fontWeight:'500'}}>{this.props.language.filterLabel}</Text></View>
                        </View>
                    </TouchableOpacity>
                </View>

                <View>
                    <ScrollView>

                <View>
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={sortingModal}
                        onRequestClose={() => {
                            this.setState({ sortingModal: false })
                        }}>
                        <TouchableOpacity style={[styles.modalDiv, { backgroundColor: 'rgba(0,0,0,0.8)' }]} onPress={() => this.setState({ sortingModal: false })}>
                            <View style={[styles.modalInnerDiv]}>
                                <Text style={{color:'#C7B048',fontSize:20,fontWeight:'600',paddingBottom:10,borderBottomWidth:0.5,borderBottomColor:'#bbb',width:'100%',textAlign:'center',marginBottom:10}}>{this.props.language.sortOptTitle}</Text>
                                <TouchableOpacity style={styles.sortBtn} onPress={() => this.setSorting('all')}>
                                    <Text style={styles.sortBtnTxt} >{this.props.language.sortOptAll}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.sortBtn} onPress={() => this.setSorting('l2h')}>
                                    <Text style={styles.sortBtnTxt} >{this.props.language.sortOptLowHigh}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.sortBtn} onPress={() => this.setSorting('h2l')}>
                                    <Text style={styles.sortBtnTxt} >{this.props.language.sortOptHighLow}</Text>
                                </TouchableOpacity>
                            </View>
                        </TouchableOpacity>
                    </Modal>
                </View>
                <View>
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={filterModal}
                        onRequestClose={() => {
                            this.setState({ sortingModal: false })
                        }}>
                        <TouchableOpacity style={[styles.modalDiv, { backgroundColor: 'rgba(0,0,0,0.8)' }]} onPress={() => this.setState({ sortingModal: false })}>

                            <View style={[styles.modalInnerDiv]}>
                            <Text style={{color:'#C7B048',fontSize:20,fontWeight:'600',paddingBottom:10,borderBottomWidth:0.5,borderBottomColor:'#bbb',width:'100%',textAlign:'center',marginBottom:10}}>{this.props.language.filterOptTitle}</Text>
                                <TouchableOpacity style={styles.sortBtn} onPress={() => this.setSorting('all')}>
                                    <Text style={styles.sortBtnTxt}>{this.props.language.filterOptAll}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.sortBtn} onPress={() => this.setSorting('l2h')}>
                                    <Text style={styles.sortBtnTxt}>{this.props.language.filterOptCategory}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.sortBtn} onPress={() => this.setSorting('h2l')}>
                                    <Text style={styles.sortBtnTxt}>{this.props.language.filterOptHighLow}</Text>
                                </TouchableOpacity>
                            </View>
                        </TouchableOpacity>
                    </Modal>
                </View>
                <View style={{ paddingHorizontal: 10, backgroundColor: '#fff' }}>
                    <View style={{ marginTop: 5, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                        {allProducts.map((data, key) => (
                            <TouchableOpacity key={key} onPress={() => this.props.navigation.navigate('Product', {
                                pro_id: data.pro_id, pro_name: data.pro_title
                            })}
                                style={{ width: width / 2 - 15, height: width / 1.5, borderWidth: 0.5, borderColor: '#bbb', marginBottom: 10, }}>
                                <View style={{ flex: 2 }}>
                                    <Image source={{ uri: 'http://cripcoin.co/jewellery/images/' + data.featured_image }} style={{ flex: 1, height: null, width: null, resizeMode: 'cover', }} />
                                </View>
                                <View style={{ paddingHorizontal: 10, paddingVertical: 8, borderTopWidth: 0.5, borderColor: '#ddd' }}>
                                    <View style={{ alignItems: 'flex-start', }}>
                                        <Text style={{ fontSize: 12, fontWeight: '100' }}>{data.pro_base_category_name}</Text>
                                        <Text numberOfLines={1} style={{ fontSize: 14, color: '#555' }}>{data.pro_title}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', paddingVertical: 2 }}>
                                        {this.getRatingStar(data.pro_rating)}
                                    </View>
                                    <Text style={{ fontSize: 17, color: '#555', fontWeight: '500' }}>{data.pro_final_price}
                                        <Text style={{ textDecorationLine: 'line-through', fontWeight: '200', fontSize: 14, color: '#bbb' }}>
                                            {data.pro_sale_price}</Text>
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        ))}
                    </View>
                </View>

                </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state) => ({
    isLoading: state.productReducer.isLoading,
    allProducts: state.productReducer.allProducts,
    cartArray: state.cartReducer.cartArray,
    error: state.productReducer.error,
    errorMsg: state.productReducer.errorMsg,
    language: state.homeReducer.app_language.screens.ShopScreen,
});
const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(ShopScreen);

const styles = StyleSheet.create({
    modalDiv: {
        flex: 1, justifyContent: 'center',
    },
    modalInnerDiv: {flex: 1, justifyContent: 'center', alignItems: 'center',marginVertical: height / 3, textAlign: 'center',backgroundColor: '#f8f8ff', marginHorizontal: width / 8, borderRadius: 5
    },
    sortBtn:{width:'100%',},
    sortBtnTxt:{color:'#333', fontWeight:'400',paddingVertical:8,paddingHorizontal:20,fontSize:17}
})
