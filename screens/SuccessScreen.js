import React, { Component } from "react";
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, ScrollView, BackHandler,Platform } from "react-native";
const { height, width } = Dimensions.get('window');
import { StackActions, NavigationActions } from 'react-navigation';

import { resetState } from '../redux/actions/actions';

import { connect } from 'react-redux';


const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'Home' })],
});


class Success extends Component {
    componentDidMount() {
        this.backhandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.resetState();
            this.props.navigation.dispatch(resetAction);
            return true;
        });
    }
    go_back() {
        this.props.resetState()
        this.props.navigation.dispatch(resetAction);
    }

    componentWillUnmount() {
        this.backhandler.remove();
    }

    render() {

        const order_id = this.props.navigation.state.params.order_id
        console.log(order_id);

        return (
            <View style={styles.container}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 20, }}>
                    <View style={{ alignItems: 'center' }}>
                        <Image style={{width: Platform.OS === 'ios' ? 100 : 150,height: Platform.OS === 'ios' ? 100 : 150, }} source={require('../images/success.png')} />
                    </View>
                    <View style={{ marginTop: 15, alignItems: 'center' }}>
                        <Text style={{ fontWeight: '700', fontSize: 35, color: '#C7B048' }}>Thank you</Text>
                        <Text style={{ fontSize: 22, color: '#333', paddingVertical: 5, marginTop: 10, textAlign: 'center' }}>Your Order has been Successfully Placed.</Text>
                        <Text style={{ fontSize: 22, color: '#333', paddingVertical: 5, marginTop: 10, textAlign: 'center' }}>Your Order ID :</Text>
                        <Text style={{textAlign:'center',padding:10,color:'#000',fontWeight:''}}>{order_id}</Text>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('OrderDetails')} style={{ marginTop: 20 }}>
                            <Text style={{ color: '#0068f7', fontSize: 16 }} >View Order Details</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.go_back()}
                            style={{ marginTop: 30 }}>
                            <Text style={{ color: '#fff', fontSize: 16, paddingHorizontal: 15, paddingVertical: 7, backgroundColor: '#C7B048', borderColor: '#C7B048', borderWidth: 1, borderRadius: 5, textTransform: 'uppercase' }} >Continue Shopping</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        );
    }
}


const mapStateToProps = (state) => ({

});
const mapDispatchToProps = {
    resetState
}

export default connect(mapStateToProps, mapDispatchToProps)(Success);

const styles = StyleSheet.create({
    container: {
        flex: 1, justifyContent: 'center'
    }
});
