import React, { Component } from "react";
import { View, Text, Dimensions, Image, Picker, TouchableOpacity, ScrollView,SafeAreaView } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';

import {Toast} from './components/Toast'
import { connect } from 'react-redux';
import Loader from '../screens/components/Loader';
import {
    addToCart,
    addToWishlist,
    removeFromWishlist,
    removeInWishlist,
    setGuestId,
    viewWishlist
} from '../redux/actions/actions';


const { height, width } = Dimensions.get('window');

class WishlistScreen extends Component {


    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.cartArray !== prevState.cartArray) {
            console.log(nextProps)
            return {
                cartArray: nextProps.cartArray
            }
        }

        if (nextProps.wishlistArray !== prevState.wishlistArray) {
            return {
                wishlistArray: nextProps.wishlistArray
            }
        }

        return null;
    }
    state = {
        cartArray: [],
        wishlistArray: [],
        toastText: '',
        toastVisible: false,
        guest_id: ''
    }

    componentDidMount() {
        guest_id = this.props.navigation.getParam('guest_id', null);
        this.setState({guest_id});
    }


    addToCart = (id, index) => {
        product = {product_id: id, qty: '1', token: '', guest_id: ''}
  //      let product = this.props.allProducts.find(product => product.pro_id == id);
        console.log("thisis worhslist : ", this.props.wishlistArray);
        console.log("im m passing index too: ", index);
        console.log("USERID: " , this.props.token);
        console.log("GUESITID: " , this.state.guest_id);
        if(this.props.token!=null && this.props.token != ''){
            product.user_id = this.props.token;
        }
        else if(this.state.guest_id!=null && this.state.guest_id != '') {
            product.guest_id = this.state.guest_id;
        }
        if(this.props.wishlistArray[index]!=null||this.props.wishlistArray[index]!=undefined){
            this.props.addToCart(JSON.stringify(product)).then(()=>{
                // const wishlistArray = [...this.props.wishlistArray];
                // wishlistArray.splice(index, 1);
                const product2 = {user_id: this.props.token?this.props.token:null, guest_id: product.guest_id?product.guest_id:null,  product_id: id, qty: '1'}
                this.props.removeFromWishlist(JSON.stringify(product2), index).then(()=>{
                    this.makeToast(this.props.language.movedToCart);
                });
            })
        }
        else{
            this.makeToast(this.props.language.productNotExists);
        }
    }

    removeFromWishlist(id, index){
        const product = {product_id: id}
        if(this.props.token!=null && this.props.token != ''){
            product.user_id = this.props.token;
        }
        else if(this.state.guest_id!=null && this.state.guest_id != ''){
            product.guest_id = this.state.guest_id;
        }
        if(this.props.wishlistArray[index]!=null){
            this.props.removeFromWishlist(JSON.stringify(product), index).then(()=>{
                this.makeToast(this.props.language.removeFromWishlist);
            })
        }
        // this.props.viewWishlist(this.props.token)
        // const index = this.props.wishlistArray.findIndex(function (pro_id) {
        //     console.log(pro_id);
        //     return pro_id.pro_id == id
        // });

        // const wishlistArray = [...this.props.wishlistArray];
        // wishlistArray.splice(index, 1);
        // console.log(index, wishlistArray);
        // this.props.removeFromWishlist(wishlistArray);
    }

    makeToast(text){
        this.setState({toastText: text})
        this.setState({toastVisible: true});
        setTimeout(()=>this.closeToast(), 2000);
    }
    closeToast(){
        this.setState({toastVisible: false});
    }

    render() {
        const { wishlistArray , remove , viewWishlist} = this.props


        console.log(this.state.wishlistArray, this.state.cartArray)
        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <Toast onRequestClose={()=>this.closeToast()} text={this.state.toastText} visible={this.state.toastVisible}/>

                <View style={{ flex: 1 }}>
                    {wishlistArray.length > 0 ?
                    <ScrollView>
                        <View style={{ flex: 1, marginTop: 9, }}>
                            {wishlistArray.map((data, index) => (
                                <Product data={data} key={index}
                                    addToCart={(id)=>this.addToCart(id, index)} removeFromWishlist={(id)=>this.removeFromWishlist(id, index)} />
                            ))}
                        </View>
                        </ScrollView>
                         :
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text
                                    style={{
                                        fontSize: 20, fontWeight: 'bold',
                                    }}>{this.props.language.wishlistEmpty}</Text>
                            </View>
                        }
                </View>

                {/* <View style={{paddingVertical:7,paddingHorizontal:7,flexDirection:'row',borderTopWidth:0.5,borderColor:'#888'}}>
                <View style={{justifyContent:'center',width:'50%',alignItems:'center'}}>
                    <Text style={{fontSize:18,color:'#FF8200',fontWeight:'700'}}>TOTAL: <Text style={{color:'#333'}}> $9999</Text></Text>
                </View>
                <View style={{ width:'50%', }}>
                    <TouchableOpacity style={{backgroundColor:'#5CAA16',paddingVertical:12}}>
                        <Text style={{fontSize:18,fontWeight:'700',textAlign:'center',color:'#fff'}}> CHECKOUT</Text>
                    </TouchableOpacity>
                </View>
            </View> */}

            </View >
        );
    }
}


class Product extends Component {

    state = { number: '' }
    updateNumber = (number) => {
        this.setState({ number: number })
    }


    getRatingStar(rating) {
        var rows = []

        for (i = 1; i <= 5; i++) {

            let fillIconName = "ios-star";

            if (i > rating) {
                fillIconName = "ios-star-outline"
            }

            rows.push(
                <Icon name={fillIconName} size={16} style={{ color: '#FFD715' }} />
            )
        }
        return rows
    }

    addToCart(id) {
        this.props.addToCart(id);
    }

    removeFromWishlist(id) {
        this.props.removeFromWishlist(id)
    }

    render() {

        const { data, removeFromWishlist, addToCart } = this.props

        return (
            <SafeAreaView>
             <View style={{backgroundColor: '#ffffff', elevation: 2, paddingTop: 0, borderBottomWidth: 0.5, borderColor: '#ccc',paddingHorizontal:10 }}>
                <View style={{ flexDirection: 'row', height: width /3, paddingHorizontal: 0,marginTop:10 }}>
                    <View style={{ width:'35%',}}>
                        <Image source={{ uri: 'http://cripcoin.co/jewellery/images/' + data.featured_image }}
                         style={{ flex: 1, height: null, width: null, resizeMode: 'cover', }} />
                    </View>

                     <View style={{ width:'65%',paddingLeft: 10,}}>

                     <View style={{ flexDirection: 'row',justifyContent:'space-between',marginTop:7}}>
                        <View style={{ width: '80%', }}>
                            <Text numberOfLines={1} style={{ fontSize: 16, color: '#333', paddingRight: 10 }}>{data.pro_title}</Text>
                        </View>
                        <View>
                            <TouchableOpacity onPress={() => this.removeFromWishlist(data.pro_id)}>
                                    <Icon name="md-trash" size={25} style={{ color: '#aaa',borderRadius:50 }}></Icon>
                            </TouchableOpacity>
                        </View>
                    </View>

                        <View style={{ paddingTop: 7 }}>
                            <Text style={{ fontSize: 17, color: '#555', fontWeight: '500' }}>${data.pro_final_price}
                            <Text style={{ textDecorationLine: 'line-through', fontWeight: '200', fontSize: 14, color: '#bbb' }}>  {data.pro_sale_price}</Text></Text>
                        </View>

                        <View style={{ paddingTop: 10,width:'60%' }}>
                            <TouchableOpacity
                                onPress={() => this.addToCart(data.pro_id)}
                                style={{ paddingVertical: 8,backgroundColor:'#222'}}>
                                <Text style={{ fontSize: 12, fontWeight: '400', textAlign: 'center', color: '#fff',textTransform:'uppercase' }}>
                                    <Icon name="md-cart" size={14} style={{ color: '#fff', }}></Icon>  {this.props.language.addToCart}</Text>
                            </TouchableOpacity>
                        </View>

                    </View>

                </View>


            </View>
            </SafeAreaView>
        );
    }
}


const mapStateToProps = (state) => ({
    isLoading: state.productReducer.isLoading,
    allProducts: state.productReducer.allProducts,
    cartArray: state.cartReducer.cartArray,
    wishlistArray: state.cartReducer.wishlistArray,
    error: state.productReducer.error,
    token: state.authReducer.token,
    errorMsg: state.productReducer.errorMsg,
    remove : state.cartReducer.remove,
    language: state.homeReducer.app_language.screens.WishlistScreen,
});
const mapDispatchToProps = {
    addToCart, addToWishlist, removeFromWishlist , removeInWishlist , viewWishlist
}

export default connect(mapStateToProps, mapDispatchToProps)(WishlistScreen);
