import React, { Component } from 'react';
import { View, StyleSheet, Animated,Dimensions } from 'react-native';

const {height,width} = Dimensions.get('window');

class ImageLoader extends Component {
  state = {
    opacity: new Animated.Value(0),
  }

  onLoad = () => {
    Animated.timing(this.state.opacity, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }

  render() {
    return (
      <Animated.Image
        onLoad={this.onLoad}
        {...this.props}
        style={[
          {
            opacity: this.state.opacity,
            transform: [
              {
                scale: this.state.opacity.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0.1, 1],
                })
              },
            ],
          },
          this.props.style,
        ]}
      />
    );
  }
}

const SplashScreen = () => (
  <View style={styles.container}>
    <ImageLoader
      style={styles.image}
      source={require('../images/bg.jpg')}
    />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#040809',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    flex:1,
    width: width+2,
    height:height+2,
    resizeMode:'cover',
  },
});

export default SplashScreen;
