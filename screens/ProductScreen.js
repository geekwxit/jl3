import React, { Component } from "react";
import {Alert, Animated, View, FlatList, Text, TouchableWithoutFeedback, StyleSheet, SafeAreaView, Modal, Dimensions, Image, ScrollView, TouchableOpacity } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import ImageViewer from 'react-native-image-zoom-viewer';
import Loader from '../screens/components/Loader';
import { connect } from 'react-redux';
import {removeFromCart, addToCart,changeQuantity, addToWishlist,removeFromWishlist, sendProductInBuyNow, getSingleProduct, addInWishlist, viewCart, viewWishlist } from '../redux/actions/actions'
import ImageSlider from 'react-native-image-slider';
import renderIf from 'render-if';
import {Toast} from './components/Toast';
import PlusIcon from "react-native-vector-icons/MaterialCommunityIcons";
import cart from "../redux/reducers/cart";
//import Cart from './CartScreen'
const { height, width } = Dimensions.get('window');
const LOREM = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

class ProductScreen extends Component {
    firstRun = true;

    description = {heightValue : new Animated.Value(0), isShowing: false};

    static navigationOptions = ({ navigation }) => {
        // console.log(navigation.state)
        const title = typeof (navigation.state.params) === 'undefined' || typeof (navigation.state.params.pro_name) === 'undefined' ? '' : navigation.state.params.pro_name;
        const { params } = navigation.state;
        return {
            headerTitle: (<Text style={{color: 'white'}}>{title.length > 17 ? title.substring(0, 17 - 3) + '...' : title}</Text>),
            titleColor: '#fff',
            // headerLeft: (<Icon name={'md-arrow-back'}
            //                 onPress={ () => { navigation.navigate('Home') }} style={{marginLeft:10,width:10,height:10}}/>),
            headerRight: (
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ justifyContent: 'center' }}>
                        <View>
                            <TouchableOpacity onPress={() => navigation.state.params.handleSave2()}>
                                <Image
                                    style={{ width: 40, height: 40 }}
                                    source={require('../images/wishlist_icon.png')}
                                />
                            </TouchableOpacity>
                        </View>
                        {renderIf(navigation.state.params.productOnwishList > 0)(
                            <View style={{
                                textAlign: 'center',
                                justifyContent: 'center',
                                position: 'absolute', zIndex: 100, top: 0, marginLeft: 30, marginTop: 7, backgroundColor: 'red', width: 15, height: 15, borderRadius: 7.5
                            }}>
                                <Text style={{ color: 'white', fontSize: 8, paddingLeft: 4 }}>{navigation.state.params.productOnwishList}</Text>
                            </View>
                        )}
                    </View>
                    <View>
                        <View style={{ width: 55, height: 55, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                            <TouchableOpacity onPress={() => navigation.state.params.handleSave()}>
                                <Image
                                    style={{ width: 30, height: 30, resizeMode: 'contain' }}
                                    source={require('../images/carticon.png')}
                                />
                            </TouchableOpacity>
                        </View>
                        {renderIf(navigation.state.params.productOnCart > 0)(
                            <View style={{
                                textAlign: 'center',
                                justifyContent: 'center',
                                position: 'absolute',
                                zIndex: 100, marginLeft: 30,
                                marginTop: 7, backgroundColor: 'red',
                                width: 15, height: 15, borderRadius: 7.5
                            }}>
                                <Text style={{ color: 'white', fontSize: 8, paddingLeft: 4 }}>{navigation.state.params.productOnCart}</Text>
                            </View>
                        )}
                    </View>
                </View>
            ),
            headerLeft: (
                <TouchableWithoutFeedback onPress={()=>navigation.goBack()}>
                <View style={{alignItems: 'center' , justifyContent: 'center', width : 50, height: 50}}>
                    <Image style={{ width: 30, height: 30,resizeMode: 'contain'}} source={require('../images/backButton.png')} />
                </View>
                </TouchableWithoutFeedback>
            ),
            headerStyle: {
                backgroundColor: '#000'
            },
        };
    };

    toggleDescription(){
        Animated.timing(
            this.description.heightValue,
            {
                toValue: this.description.isShowing?0:1000,
                duration: 500
            }
        ).start(()=>this.description.isShowing = !this.description.isShowing);
        // this.setState({
        //     description : {isShowing: !this.state.description.isShowing}
        // })
        //debugger;
    }

    constructor(props) {
        super(props)
        console.log("prodddddddddddddddddddddddddddddddddddd")
        this.state = {
            description: {heightValue : new Animated.Value(0), isShowing: false},
            images: [],
            productDetail: [],
            relatedProduct: [],
            guest_id: '',
            pro_id: '',
            isLoading: false,
            zoomImage: false,
            isAddedToWishlist: false,
            cartArray: [], wishlistArray: [], cartCount: null, wishlistCount: 0,
            toastVisible : false,
            toastText : ''
        }
        // this.updateCart();this.updateWishlist();

    }
    makeToast(text){
        this.setState({toastText: text})
        this.setState({toastVisible: true});
        setTimeout(()=>this.closeToast(), 2000);
    }
    closeToast(){
        this.setState({toastVisible: false});
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        console.log("Calling get derived :" , nextProps.cartCount, " custom state: ", prevState.cartCount);
        console.log("Calling get derived for wishlist :" , nextProps.wishlistCount, " custom state: ", prevState.wishlistCount);
        if (nextProps.cartCount !== prevState.cartCount) {
            console.log(nextProps.wishlistCount, "this is props");
            nextProps.navigation.setParams({
                productOnCart: nextProps.cartCount
            })
            return {
                cartCount: nextProps.cartCount
            }
        }
        if(nextProps.wishlistCount!==prevState.wishlistCount && nextProps.wishlistCount!=null&&nextProps.wishlistCount!=undefined){

            nextProps.navigation.setParams({
                productOnwishList: nextProps.wishlistCount
            })
            return{
                wishlistCount: nextProps.wishlistCount,
            }
        }
        if (nextProps.singleProduct !== prevState.productDetail) {
            nextProps.navigation.setParams({
                productOnCart: nextProps.cartCount
            })
            return {
                productDetail: nextProps.singleProduct
            }
        }
        if(nextProps.cartArray != prevState.cartArray){
            return{
                cartArray: nextProps.cartArray
            }
        }

        if (nextProps.gallery !== prevState.images) {
            return {
                images: [...nextProps.gallery]
            }
        }
        if (nextProps.relatedProduct !== prevState.relatedProduct) {
            return {
                relatedProduct: [...nextProps.relatedProduct]
            }
        }
        if (nextProps.isLoading !== prevState.isLoading) {
            console.log("this is my next props for adding wishlist", nextProps)
            return {
                isLoading: nextProps.isLoading
            }
        }
        if (nextProps.cartCount !== prevState.cartCount) {

            nextProps.navigation.setParams({
                productOnCart: nextProps.cartCount
            })

            return {
                cartArray: nextProps.cartArray
            }
        }
        return null;
    }

    componentDidMount() {
        const { allProducts } = this.props
        this.props.navigation.setParams({ handleSave: this._moveToCartPage });
        this.props.navigation.setParams({ handleSave2: this._moveToWishlistPage });
        var pro_id = this.props.navigation.getParam('pro_id', null);
        var guest_id = this.props.navigation.getParam('guest_id', null);

        this.props.getSingleProduct(pro_id);
        this.props.viewWishlist(JSON.stringify({token: this.props.token, guest_id: guest_id}));
        this.props.viewCart(JSON.stringify({user_id: this.props.token, guest_id: guest_id})).then(
            console.log("cart count on prodcut screen: ", JSON.stringify({user_id: this.props.token}))
        );
        // single product
        let product = allProducts.find(product => product.pro_id == pro_id);

        // gallery
        var productGallery = product.gallery_img.split("/");
        var sliderImagesArray = Array.from(productGallery, (image) => 'http://cripcoin.co/jewellery/images/' + image)

        // related product

        var rProduct = allProducts.filter(relatedProduct => relatedProduct.pro_base_category == product.pro_base_category)
        this.props.navigation.setParams({ title: product.pro_title })
        console.log(rProduct)
        this.setState({
            guest_id, pro_id,
            // productDetail: product,
            // relatedProduct: rProduct,
            images: sliderImagesArray
        });

        // console.log(this.state.relatedProduct)

    }

    updateWishlist(id){
        if(this.firstRun==true){
            this.firstRun = false;
        try{this.props.wishlistArray.forEach(item=>{
            if(item.w_product_id===id){
                throw "found";
            }})
            throw "notfound";
        }
        catch(e){
            if(e==="found"){
                return '#ff4343';
            }
            else {
                return '#bbb';
            }
        }}
        else{return false}
        console.log('wishlist collector: ', this.props.wishlistArray);
        console.log('wishlist obeser: ', id);
    }

    _moveToCartPage = () => {
        console.log('guest_id in moveToCartPage', this.state.guest_id)
        console.log('gusersrd in moveToCartPage', this.props.token)
        if(this.props.token!=null && this.props.token != '' && this.props.token!=undefined){
            this.props.navigation.navigate('Cart', {
                user_id: this.props.token,
                guest_id: null
            });
        }
        else if(this.state.guest_id!=null && this.state.guest_id != '' && this.state.guest_id!=undefined){
            this.props.navigation.navigate('Cart', {
                guest_id: this.state.guest_id,
                user_id: null
            });
        }
    }
    _moveToWishlistPage = () => {
        console.log('guest_id in moveToCartPage', this.state.guest_id)
        this.props.navigation.navigate('Wishlist', {
            guest_id: this.state.guest_id,
            // user_id: this.state.user_id,
        });
    }

    _relatedProduct(pro_id, name) {
        this.props.navigation.push('Product', {
            pro_id: pro_id,
            guest_id: this.state.guest_id,
            pro_name: name
        })
    }

    findWishlistIndex(product_id){
        i = null;
        console.log("myid", product_id);
        console.log("wAAA", this.props.wishlistArray);
        this.props.wishlistArray.map((item,index)=>{
            if(item.w_product_id==product_id){
                i = index;
            }
        })
        return i;
    }


    addToWishlist(pro_id){
        const product = {product_id: pro_id}
        if(this.props.token!=null && this.props.token != ''){
            product.user_id = this.props.token;
        }
        else if(this.state.guest_id!=null && this.state.guest_id != ''){
            product.guest_id = this.state.guest_id;
        }
        index = this.findWishlistIndex(pro_id);
        console.log("the array is : ", this.props.wishlistArray, '\n      ', index)
        if(this.props.wishlistArray.map(item=>item.w_product_id).includes(pro_id)){
            this.props.removeFromWishlist(JSON.stringify(product), index).then(()=>{
                //this.setState({isAddedToWishlist: false});
                this.makeToast(this.props.language.wishlistRemove);
            });
        }
        else{
        //if(this.state.isAddedToWishlist!=true){
            //try{
                if(this.props.cartArray.map(item=>item.c_product_id).includes(pro_id)){
                    Alert.alert(this.props.language.wishlistAlertTitle,
                            this.props.language.wishlistAlertMsg,
                            [{text: this.props.language.yes, onPress:()=>{
                                this.props.addToWishlist(JSON.stringify(product)).then(()=>{
                                    //this.setState({isAddedToWishlist: !this.state.isAddedToWishlist});
                                    this.makeToast(this.props.language.wishlistSuccess);
                                });
                            }
                            }, {text:this.props.language.no, onPress:()=>{return false}}]);
                }
                else{
                    this.props.addToWishlist(JSON.stringify(product)).then(()=>{
                        this.makeToast(this.props.language.wishlistSuccess)
                    });
                }
            }

    }


    buyNow(pro_id) {
        if (this.props.token) {
            if (this.props.customerAddress.length <= 0 && !this.props.success) {
                this.props.navigation.navigate('DeliveryAddress', { data: this.state.productDetail });
            } else {
                this.props.navigation.navigate('DeliveryDlts', { data: this.state.productDetail });
            }
        } else {
            this.props.navigation.navigate('SignIn', { goto: true, data: this.state.productDetail });
        }
    }

    addToCart(pro_id, quantity) {
        const product = {
            product_id: pro_id,
            qty: '1',
            product_price : this.state.productDetail.pro_sale_price,
        };

        //Checking if user is logged in or will give guest id...
        if(this.props.token!=null && this.props.token != ''){
            product.user_id = this.props.token;
        }
        else if(this.state.guest_id!=null && this.state.guest_id != ''){
            product.guest_id = this.state.guest_id;
        }
        if (this.props.cartArray.length > 0) {
            try{
                this.props.cartArray.forEach(data => {
                if (data.pro_id == pro_id && quantity == 1){
                    Alert.alert(this.props.language.buyAlert, this.props.language.alreadyInCart,
                        [{text:this.props.language.yes, onPress:()=>{this.props.addToCart(JSON.stringify(product)).then(()=>{
                            this.setState({cartCount: this.props.cartCount});this.makeToast(this.props.language.quantityIncrease);return true;}
                        )}}, {text:this.props.language.no, onPress:()=>false}])
                    //alert('Product Already in cart first');
                }
                else if(data.pro_id == pro_id){
                    this.props.addToCart(JSON.stringify(product)).then(()=>{
                        this.makeToast(this.props.language.quantityIncreased);
                        this.setState({cartCount: this.props.cartCount});
                    });
                    throw true;
                }
                else{
                    this.props.addToCart(JSON.stringify(product)).then(()=>{
                        this.makeToast(this.props.language.productAddedToCart);
                        }
                    );
                    throw true;
                }
            });}
            catch(e){}
        }
        else {
            this.props.addToCart(JSON.stringify(product)).then(()=>{
            this.makeToast(this.props.language.productAddedToCart)});
        }
    }


    getRatingStar(rating) {
        var rows = []

        for (i = 1; i <= 5; i++) {

            let fillIconName = "ios-star";

            if (i > rating) {
                fillIconName = "ios-star-outline"
            }

            rows.push(
                <Icon name={fillIconName} size={16} style={{ color: '#FFD715' }} />
            )
        }
        return rows
    }
    zoomedImagePath = null;

    showInModal(product){
        this.setState({zoomImage: true});
        this.zoomedImagePath = product.image;
    }

    hideZoomModal(){
        this.setState({zoomImage: false});
    }
    updateQuantity(newQuantity, pro_id, prevQuantity){
        INDEX = null;
        this.props.cartArray.map((item, index)=>{
            if(item.c_product_id==pro_id){
                INDEX = index;
            }
        })
        const data = {user_id: this.props.token, product_id: pro_id, qty: newQuantity, guest_id: this.state.guest_id}
        console.log("parsing Int with something: somequan_>", prevQuantity, "newquan>", newQuantity, "parsedIUnt:",parseInt(prevQuantity)+parseInt(newQuantity), parseInt(prevQuantity)+parseInt(newQuantity)<=0)
        if(parseInt(prevQuantity)+parseInt(newQuantity)<=0){
            this.props.removeFromCart(JSON.stringify(data), INDEX, newQuantity).then(()=>this.makeToast(this.props.language.productRemovedFromCart));
        }
        else {
            this.props.changeQuantity(JSON.stringify(data), INDEX, newQuantity).then(()=>this.makeToast(this.props.language.productQuantity));
        }
        console.log(data);
    }

    state = {ScrollView1:false,ScrollView2:false,ScrollView3:false,}

    toggleScrollView1(){
        alert('sdsds');
        if (this.state.ScrollView1){
            this.setState({ScrollView1:true,ScrollView2:false,ScrollView3:false,})
        }
    }

    toggleScrollView2(){
        if (this.state.ScrollView2){
            this.setState({ScrollView1:false,ScrollView2:true,ScrollView3:false,})
        }
    }

    toggleScrollView3(){
        if (this.state.ScrollView3){
            this.setState({ScrollView1:false,ScrollView2:false,ScrollView3:true,})
        }
    }

    render() {
        const { productDetail, isLoading, pro_id } = this.state;
        const { relatedProduct, gallery } = this.props;
        var sliderImagesArray = Array.from(gallery, (image) => 'http://cripcoin.co/jewellery/images/' + image)
        let quantity = 0;
        let isInWishlist = false;
        if (isLoading) {
            return (<Loader visible={isLoading} />)
        } else if (productDetail.length == 0 && !isLoading) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginHorizontal: 30 }}>
                    <Text>Something went wrong on your network. Please Check your Internet Connection and hit the retry button.</Text>
                    <TouchableOpacity onPress={() => this.getSingleProduct(pro_id)}
                        style={{ backgroundColor: '#C7B048', paddingVertical: 10 , paddingHorizontal:40 }}>
                        <Text style={{ fontSize: 18, fontWeight: '700', textAlign: 'center', color: '#fff' }}> RETRY</Text>
                    </TouchableOpacity>
                </View>)
        }
        else {
            return (
                <SafeAreaView style={styles.container}>
                        <Modal
                            style={{top: '50%', left: '50%', transform: 'translate(-50%, -50%) !important'}}
                            animationType='fade'
                            transparent={true}
                            onRequestClose={()=>this.hideZoomModal()}
                            visible={this.state.zoomImage}
                        >
                            <ImageViewer imageUrls={[{url: this.zoomedImagePath}]}/>

                        </Modal>
                        {console.log("consoleing this", this.props.wishlistArray)}
                    {
                        this.props.wishlistArray&&this.props.wishlistArray.length>0?isInWishlist = this.props.wishlistArray.map(item=>item.w_product_id).includes(productDetail.pro_id):null
                    }

                    <Toast onRequestClose={()=>this.closeToast()} text={this.state.toastText} visible={this.state.toastVisible}/>

                    <ScrollView showsVerticalScrollIndicator={false }>
                        <View style={{ alignItems: 'center', marginTop: 10, height: width,borderBottomColor:'#ddd',borderBottomWidth:0.5 }}>
                            <View style={{ alignItems: 'center' }}>
                                <ImageSlider onPress={(image)=>{this.showInModal(image)}}
                                    autoPlayWithInterval={3000}
                                    images={sliderImagesArray}
                                    style={styles.sliderImg}
                                />
                            </View>
                        </View>
                        <View style={{ marginHorizontal: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10, }}>
                                <View >
                                    <Text style={{ fontSize: 20, color: '#444', fontWeight: '500' }}>${productDetail.pro_final_price} <Text style={{ textDecorationLine: 'line-through', fontWeight: '200', fontSize: 14, color: '#bbb' }}> $000</Text></Text>
                                </View>
                                <View style={{ flexDirection: 'row',  marginTop: 5 }}>
                                    {this.getRatingStar(productDetail.pro_rating)}
                                </View>
                            </View>
                            <View style={{ marginTop:5, }}>
                                <Text numberOfLines={2} style={{ fontSize: 20, color: '#555',textTransform:'capitalize' }}>{productDetail.pro_title}</Text>
                            </View>
                        </View>

                        <View style={{borderWidth:0.5,borderColor:'#bbb',marginVertical:15,marginHorizontal:10}}/>

                        <View style={{flexDirection: 'row', paddingBottom: 10,alignItems: 'center', justifyContent:'space-around',marginHorizontal:10}}>
                                <TouchableOpacity onPress={() => this.addToWishlist(pro_id)}>
                                    <View style={{width: width / 3, alignItems: 'center'}}>
                                        <Image style={{width: 30, height: 30, resizeMode: 'contain'}}
                                            source={require('../images/wishlisticonplus.png')}/>
                                        <Text>{this.props.language.wishlist}</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.showReviews(pro_id)}>
                                    <View style={{width: width / 3, alignItems: 'center'}}>
                                        <Image style={{width: 30, height: 30, resizeMode: 'contain',opacity:0.5}}
                                            source={require('../images/reviewicon.png')}/>
                                        <Text>{this.props.language.productReviewsTitle}</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.shareProduct(pro_id)}>
                                    <View style={{width: width / 3, alignItems: 'center'}}>
                                        <Image style={{width: 30, height: 30, resizeMode: 'contain'}}
                                            source={require('../images/shareicon.png')}/>
                                        <Text>{this.props.language.share}</Text>
                                    </View>
                                </TouchableOpacity>
                        </View>

                        <View style={{ marginTop: 10,marginHorizontal:10 }}>
                            <View style={{backgroundColor: '#f5f7f7', padding : 10,borderColor: '#bbb', borderLeftWidth: 5,marginBottom:10}}>
                                <Text style={{fontSize:15,color:'#333'}}>{this.props.language.desc}</Text>
                            </View>
                            <Text style={{ fontSize: 16, textAlign: 'justify' }}> {productDetail.full_description}</Text>

                            <View style={{ borderColor: '#eee', borderWidth: 0.5, marginTop: 10, }} />


                            <View style={{marginTop:20}}>
                                <TouchableOpacity style={styles.tglBtn} onPress={()=>{this.setState({ScrollView1:true,ScrollView2:false,ScrollView3:false})}}>
                                <Text style={styles.tglTxt}>{this.props.language.productDescTitle}</Text>
                                <Text style={styles.tglTxt}><Icon name={this.state.ScrollView1 ? "ios-arrow-down" :"ios-arrow-forward"} style={{marginTop:5}} size={20}/></Text>
                                </TouchableOpacity>
                                    {this.state.ScrollView1 ?
                                        <View title="Product Description"><Text style={{textAlign: 'justify',borderBottomColor:'#bbb',borderBottomWidth:0.5,paddingVertical:10}}>{LOREM}</Text></View> : null
                                    }
                                <TouchableOpacity style={styles.tglBtn} onPress={()=>{this.setState({ScrollView1:false,ScrollView2:true,ScrollView3:false})}}>
                                <Text style={styles.tglTxt}>{this.props.language.productDetailTitle}</Text>
                                <Text style={styles.tglTxt}><Icon name={this.state.ScrollView2 ? "ios-arrow-down" :"ios-arrow-forward"} style={{marginTop:5}} size={20}/></Text>
                                </TouchableOpacity>
                                    {this.state.ScrollView2 &&
                                        <View title="Product Details"><Text style={{textAlign: 'justify',borderBottomColor:'#bbb',borderBottomWidth:0.5,paddingVertical:10}}>{LOREM}</Text></View>
                                    }
                                <TouchableOpacity style={styles.tglBtn} onPress={()=>{this.setState({ScrollView1:false,ScrollView2:false,ScrollView3:true})}} >
                                <Text style={styles.tglTxt}>{this.props.language.productReviewsTitle}</Text>
                                <Text style={styles.tglTxt}><Icon name={this.state.ScrollView3 ? "ios-arrow-down" :"ios-arrow-forward"} style={{marginTop:5}} size={20}/></Text>
                                </TouchableOpacity>
                                    {this.state.ScrollView3 &&
                                        <View title="Reviews"><Text style={{textAlign: 'justify',borderBottomColor:'#bbb',borderBottomWidth:0.5,paddingVertical:10}}>{LOREM}</Text></View>
                                    }
                            </View>

                        </View>



                        <View style={{ marginTop: 30, paddingHorizontal: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10 }}>
                                <View><Text style={styles.hdng}>{this.props.language.relatedProduct}</Text></View>
                                <View>
                                    <TouchableOpacity  onPress={() => this.props.navigation.navigate('Shop')}>
                                    <Text style={styles.hdng}> <Text style={{ color: '#333',marginTop:15 }}>{this.props.language.viewAllButton} <Icon name="ios-arrow-forward" style={{ color: '#333',marginTop:15 }} size={20}   /></Text></Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <View style={{ marginTop: 10,marginBottom:10 }}>
                                {relatedProduct.length > 0 ? <ScrollView>
                                        <FlatList
                                            data={relatedProduct}
                                            horizontal={true}
                                            keyExtractor={(item, key) => key}
                                            showsHorizontalScrollIndicator={false}
                                            renderItem={({item}) =>
                                                <TouchableOpacity
                                                    onPress={() => this._relatedProduct(item.pro_id, item.pro_title)}
                                                    style={{flex:1,width: width/3,marginRight: 10,borderWidth: 0.5,borderColor: '#ddd'}}>
                                                    <View style={{borderBottomWidth: 0.5,height: width/3, borderBottomColor: '#ddd',flex:1}}>
                                                        <Image source={{uri: 'http://cripcoin.co/jewellery/images/' + item.featured_image}} style={{flex:1,width: null,height: null,resizeMode: 'cover',}}/>
                                                    </View>
                                                    <View style={{alignItems: 'center',marginTop: 7,}}>
                                                        <Text numberOfLines={1} style={{color: '#000',fontSize: 16,paddingHorizontal: 10}}>{item.pro_title}</Text>
                                                    </View>
                                                    <View style={{alignItems: 'center',marginVertical:8}}>
                                                        <Text numberOfLines={1} style={{color: '#000',fontSize: 16,fontWeight:'500',paddingHorizontal: 10}}>${item.pro_final_price}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            }
                                        />
                                    </ScrollView>
                                    : console.log(this.state.relatedProduct)}
                            </View>
                        </View>
                    </ScrollView>

                    <View style={{ paddingVertical: 7, paddingHorizontal: 7, flexDirection: 'row', borderTopWidth: 0.5, borderColor: '#888' }}>
                       {
                           this.props.cartArray.map(item=>{
                               if(item.c_product_id == productDetail.pro_id){
                                    quantity = item.c_quantity;
                               }
                           })
                       }

                        <ADDTOCART_BUTTON buttonText={this.props.language.addToCartButton} addToCart={()=>this.addToCart(productDetail.pro_id, quantity)} quantity={quantity} updateQuantity={(value)=>this.updateQuantity(value, productDetail.pro_id, quantity)}/>
                        <View style={{ width: '50%', }}>
                            <TouchableOpacity onPress={() => this.buyNow(this.state.pro_id)}
                                style={{ backgroundColor: '#C7B048', paddingVertical: 10 }}>
                                <Text style={{ fontSize: 18, fontWeight: '700', textAlign: 'center', color: '#fff' }}>{this.props.language.buyNowButton}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </SafeAreaView>
            );
        }
    }
}

class ADDTOCART_BUTTON extends Component{
    render(){
        if (parseInt(this.props.quantity) > 0) {
            return (<View style={{flex: 1, justifyContent: 'space-around', flexDirection: 'row', paddingVertical: 10}}>
                <TouchableOpacity onPress={()=>this.props.updateQuantity(1)}>
                    <PlusIcon name={'plus'} size={20}/>
                </TouchableOpacity>
                <Text style={{fontSize: 20}}>{this.props.quantity}</Text>
                <TouchableOpacity onPress={()=>this.props.updateQuantity(-1)}>
                    <PlusIcon name={'minus'} size={20}/>
                </TouchableOpacity>
            </View>)
        } else {
            return (<TouchableOpacity onPress={() => this.props.addToCart()}
                                      style={{justifyContent: 'center', width: '50%', alignItems: 'center'}}>
                <Text style={{fontSize: 18, color: '#2f5e6a', fontWeight: '700'}}>{this.props.buttonText}</Text>
            </TouchableOpacity>)
        }
    }
}

const mapStateToProps = (state) => ({
    isLoading: state.productReducer.isLoading,
    allProducts: state.productReducer.allProducts,
    singleProduct: state.productReducer.singleProduct,
    cartArray: state.cartReducer.cartArray,
    wishlistArray: state.cartReducer.wishlistArray,
    cartCount: state.cartReducer.cartCount,
    wishlistCount: state.cartReducer.wishlistCount,
    customerAddress: state.customerReducer.customerAddress,
    error: state.productReducer.error,
    errorMsg: state.productReducer.errorMsg,
    buyNow: state.cartReducer.buyNow,
    success: state.customerReducer.success,
    token: state.authReducer.token,
    gallery: state.productReducer.productGallery,
    relatedProduct: state.productReducer.relatedProduct,
    language: state.homeReducer.app_language.screens.ProductScreen,
});
const mapDispatchToProps = {
    addToCart, changeQuantity, addToWishlist, sendProductInBuyNow, removeFromCart, getSingleProduct, addInWishlist, viewWishlist, viewCart, removeFromWishlist
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductScreen);

const styles = StyleSheet.create({
    container: {backgroundColor: '#fff',flex: 1},
    sliderImg: { resizeMode: 'contain', flex: 1, width: null, height: null, },
    sliderView: { borderBottomWidth: 0.5},
   item: {paddingBottom: 5,paddingLeft: 5,fontSize: 18,color: '#666'},
    hdng: {fontSize: 18,color: '#555',fontWeight: '700'},
    btn: {backgroundColor: '#C7B048',paddingHorizontal: 10,paddingVertical: 2.5,borderRadius: 10},
    sliderView: { borderRadius: 0, borderWidth: 2, borderColor: '#ccc', marginLeft: 20, marginRight: 20 },
    tglBtn:{borderBottomColor:'#bbb',borderBottomWidth:0.5,justifyContent:'space-between',flexDirection:'row'},
    tglTxt:{fontSize:18,color:'#333',paddingVertical:8,}
});
