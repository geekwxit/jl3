import React, { Component } from "react";
import { View,Text,StyleSheet,ScrollView, TouchableOpacity ,TextInput, AsyncStorage	} from "react-native";
//import Storage from 'react-native-storage';
import {connect} from  'react-redux';
import {viewCart, viewWishlist, setUserDetails} from "../redux/actions/actions";
import axios from "axios";
import {compose} from "redux";

class AccountSetting extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: typeof(navigation.state.params)==='undefined' || typeof(navigation.state.params.title) === 'undefined' ? '': navigation.state.params.title,
    });
    constructor(props){
        super(props);
        this.state = {
            userData: {name: '', phone: '', email: ''},
            name: '',
            pass:'',
            currentPassword:'',
            newPassword:'' ,
            newConfirmPassword:'',
            user_id:'',
            fullName: '',
            mobileNumber:''
        }
    }

    componentDidMount(){
        userData = this.props.navigation.getParam('userData');
        this.setState({userData});
    }

    changePassword(){
        oldPassword = this.state.currentPassword.toString().trim();
        newPassword = this.state.newPassword.toString().trim();
        confPass = this.state.newConfirmPassword.toString().trim();
        var response = null;
        const data = {old_password: oldPassword, new_password: newPassword, email: this.state.userData.email}
        if(!(oldPassword==''||newPassword==''||confPass==='')){
            oldPassword===newPassword?
                alert(this.props.language.oldPassError)
                :confPass===newPassword?
                    this.requestUpdateData(data, 'password')
                    :alert(this.props.language.passMatch);
        }
        else {
            alert(this.props.language.fieldsEmpty);
        }
    }

    updateUserData(){
        username = this.state.userData.name.toString().trim();
        phone = this.state.userData.phone.toString().trim();
        email = this.state.userData.email.toString().trim();
        if(username!=null && phone !=null && email !=null && username!='' && phone !='' && email !='' ){
            data = {name: username, email: email, phone: phone, userDataChange : true};
            this.props.setUserDetails(data);
        }
        else {
            alert(this.props.language.fieldsEmpty);
        }
    }

    async requestUpdateData(fields, requestType){
        const url = "http://cripcoin.co/jewellery/mobileApi/update_user_data.php";
        data = {};
        switch(requestType){
            case 'password' :
                data = {passwordChange : true, ...fields};
                break;
            case 'userdata' :
                data = {userDataChange : true, ...fields};
                break;
            default: data = null;
        }
        console.log(data);
        await axios.post(url, data)
              .then(response=>response.data)
              .then(response=>!response.error?alert(response.msg):alert(this.props.language.wentWrong))
              .catch(e=>console.log(e));
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>

                    <View style={{marginHorizontal:10,marginTop:20}}>
                        <View>
                            <Text>{this.props.language.fullname}</Text>
                            <View>
                                <TextInput
                                    style={{borderBottomWidth:0.5,borderBottomColor:'#bbb',marginBottom:20}}
                                    onChangeText={(value) => this.setState({userData: {...userData, name: value }})}
                                    value={this.state.userData.name}
                                />
                            </View>
                        </View>
                        <View>
                            <Text>{this.props.language.mobileNumber}</Text>
                            <View>
                                <TextInput
                                    style={{borderBottomWidth:0.5,borderBottomColor:'#bbb',marginBottom:20}}
                                    onChangeText={(value) => this.setState({userData: {...userData, phone: value }})}
                                    value={this.state.userData.phone}
                                />
                            </View>
                        </View>
                        <View>
                            <TouchableOpacity style={{marginTop:0}} onPress={()=>this.updateUserData()}>
                                <Text style={{color:'#0068f7',fontSize:17,fontWeight:'500',textAlign:'center'}} >{this.props.language.updateButton}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    {/* <View style={{borderWidth:0.5,borderColor:'#bbb',marginTop:30}} /> */}

                    <View style={{marginHorizontal:10,marginTop:50}}>

                        <View>
                            <Text>{this.props.language.email}</Text>
                            <View>
                                <TextInput
                                value={this.state.userData.email}
                                    style={{borderBottomWidth:0.5,borderBottomColor:'#bbb',marginBottom:20}}
                                    editable={false}
                                />
                            </View>
                        </View>

                    </View>


                    <View style={{marginHorizontal:10,marginTop:50,marginBottom:30}}>
                        <View>
                            <Text style={{color:'#333',fontSize:20,fontWeight:'400',textAlign:'center',marginBottom:20}}>{this.props.language.changePassword}</Text>
                            <Text>{this.props.language.currPass}</Text>
                            <View>
                                <TextInput
                                    // placeholder='Enter current password'
                                    style=  {{borderBottomWidth:0.5,borderBottomColor:'#bbb',marginBottom:20}}
                                    onChangeText={(value) => this.setState({currentPassword:value })}
                                />
                            </View>
                        </View>
                        <View>
                            <Text>{this.props.language.newPass}</Text>
                            <View>
                                <TextInput
                                    // placeholder='Set new password'
                                    style={{borderBottomWidth:0.5,borderBottomColor:'#bbb',marginBottom:20}}
                                    onChangeText={(value) => this.setState({newPassword:value })}
                                />
                            </View>
                        </View>
                        <View>
                            <Text>{this.props.language.confNewPass}</Text>
                            <View>
                                <TextInput
                                    // placeholder='Confirm new password'
                                    style={{borderBottomWidth:0.5,borderBottomColor:'#bbb',marginBottom:20}}
                                    onChangeText={(value) => this.setState({newConfirmPassword:value })}
                                />
                            </View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:"space-around",marginTop:20}}>
                            {/* <View style={{width:'50%'}}>
                            <TouchableOpacity style={{marginTop:0}}>
                                <Text style={{color:'#777',fontSize:17,fontWeight:'500',textAlign:'center'}} >CANCEL</Text>
                            </TouchableOpacity>
                        </View>*/}
                            <View style={{width:'50%'}}>
                                <TouchableOpacity style={{marginTop:0}} onPress={()=>this.changePassword()}>
                                    <Text style={{color:'#0068f7',fontSize:17,fontWeight:'500',textAlign:'center'}} >{this.props.language.saveButton}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>




                </ScrollView>
            </View>
        );
    }
}


const mapStateToProps = (state) => ({
    customerAddress: state.customerReducer.customerAddress,
    token: state.authReducer.token,
    gallery: state.productReducer.productGallery,
    relatedProduct: state.productReducer.relatedProduct,
    userData: {name: state.authReducer.name, email: state.authReducer.email, phone:  state.authReducer.name},
    language: state.homeReducer.app_language.screens.SettingScreen,
});
const mapDispatchToProps = {
    viewWishlist, viewCart, setUserDetails
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountSetting);
const styles = StyleSheet.create({
    container: {
        flex: 1,

    }
});
