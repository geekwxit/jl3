import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import { createSwitchNavigator, createStackNavigator, createDrawerNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
// redux implementation
import { Provider } from 'react-redux';
import { store, persistor } from './redux/store'
import { PersistGate } from 'redux-persist/integration/react';
import SettingScreen from './screens/SettingScreen';
import SplashScreen from './screens/SplashScreen';
import AuthLoadingScreen from './screens/AuthLoadingScreen';
import WelcomeScreen from './screens/WelcomeScreen';
import SignInScreen from './screens/SignInScreen';
import SignUpScreen from './screens/SignUpScreen';
import HomeScreen from './screens/HomeScreen';
import CartScreen from "./screens/CartScreen";
import WishlistScreen from "./screens/WishlistScreen";
import ProductScreen from './screens/ProductScreen';
import ShopScreen from "./screens/ShopScreen";
import SearchScreen from './screens/SearchScreen';
import DeliveryAddress from './screens/DeliveryAddress';
//import DeliveryDlts from './screens/DeliveryDtls';
import EditDeliveryAddress from './screens/EditDeliveryAddress';
import OrderDetails from './screens/OrderDtls';
import Orders from './screens/Orders';
import SuccessScreen from './screens/SuccessScreen';

//CHANGERS BY dfasdfGEEK
//Changes byme


import AllProduct from "./screens/AllProduct";


import DrawerComponent from './screens/CustomDrawerComponent';
import AccountScreen from './screens/AccountScreen';
import AccountSetting from './screens/SettingScreen';

// import OrderScreens from './screens/OrderScreens';
import CheckoutScreen from './screens/CheckoutScreen';

export default class App extends Component {

  constructor() {
    super();
    this.state = { Splash: false };
  }

  componentDidMount() {
    setTimeout(() => { this.splashSet() }, 2000);

  }

  splashSet() { this.setState({ Splash: true }); }

  render() {
      console.disableYellowBox = true;
    if (!this.state.Splash) {
      return <SplashScreen />;
    } else {
      return (
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <MainAppContainer />
          </PersistGate>
        </Provider>)
    }
  }
}

const AuthStackNavigator = createStackNavigator({
  Welcome: {
    screen: WelcomeScreen,
    navigationOptions: {
      title: 'Home',
      header: null
    }
  },
  SignIn: {
    screen: SignInScreen,
    navigationOptions: {
      // headerTitleStyle: {flex:0.8, textAlign:'center' },
      // title:'Sign In'
      header: null
    }
  },
  SignUp: {
    screen: SignUpScreen,
    navigationOptions: {
      // headerTitleStyle: {flex:0.8, textAlign:'center'},
      // title: 'Sign Up'
      header: null
    }
  }
  // all_Product:{
  //   screen: AllProduct,
  // },
  // product:{
  //   screen:ProductScreen
  // }
}, {
    initialRouteName: 'Welcome'
  });

// const AppTabNavigator = createBottomTabNavigator({
//
//   Home:{
//     screen:HomeScreen,
//     navigationOptions:{
//       tabBarLabel:'Home',
//       tabBarIcon:({ tintColor }) => (
//         <Icon name='md-home' color={tintColor} size={25}  />
//       )
//     }
//   },
//   HappyHours:{
//     screen:HomeScreen,
//     navigationOptions:{
//       tabBarLabel:'Happy Hours',
//       tabBarIcon:({ tintColor }) => (
//         <Icon name='md-alarm' color={tintColor} size={25}  />
//       )
//     }
//   },
//   Wishlist:{
//     screen:WishlistScreen,
//     navigationOptions:{
//       tabBarLabel:'Wishlist',
//       tabBarIcon:({ tintColor }) => (
//         <Icon name='md-heart' color={tintColor} size={25}  />
//       )
//     }
//   },
//   Account:{
//     screen:SettingScreen,
//     navigationOptions:{
//       tabBarLabel:'Account',
//       tabBarIcon:({ tintColor }) => (
//         <Icon name='md-person' color={tintColor} size={25}  />
//       )
//     }
//   },
//
// },{
//   tabBarOptions : {
//     activeTintColor:'#C7B048',
//     inactiveTintColor:'#fff',
//     style: {
//       backgroundColor: '#081012',
//       borderTopWidthColor:0,
//       shadowOffset:{width:5, height:3},
//       shadowColor:'#000',
//       shadowOpacity:0.5,
//       elevation:5,
//       paddingBottom:3,
//       height:55
//     }
//   }
// });


const AppStackNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: ({ navigation }) => ({

      headerLeft: (
        <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
          <View style={{ paddingHorizontal: 10 }}>
            <Icon name="md-menu" size={30} style={{ color: '#fff' }} />
          </View>
        </TouchableOpacity>
      ),
      headerTitle: (
        <View style={{
          justifyContent: 'center',
          alignItems: 'center', flex: 1
        }}>
          <Image style={{ height: 45, resizeMode: 'contain', }} source={require('./images/logo.png')} />
        </View>
      ),
      headerTitleStyle: {
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
      }, headerStyle: {
        backgroundColor: '#000', height: 70
      },
    })
  },
  Cart: {
    screen: CartScreen,
    navigationOptions: ({
      headerTitle: 'My Cart'
    })
  },



  Wishlist: {
    screen: WishlistScreen,
    navigationOptions: ({
      headerTitle: 'My Wishlist'
    })
  },


  Product: {
    screen: ProductScreen,
  },

  Shop: {
    screen: ShopScreen,
    navigationOptions: ({
      headerTitle: 'Shop'
    })
  },


  // SignIn: {
  //   screen: SignInScreen,
  //   navigationOptions: ({
  //     headerTitle: 'Sign In'
  //   })
  // },

  // SignUp: {
  //   screen: SignUpScreen,
  //   navigationOptions: ({
  //     headerTitle: 'Sign Up'
  //   })
  // },

  Search: {
    screen: SearchScreen,
    navigationOptions: ({
      headerTitle: 'Search'
    })
  },
  DeliveryAddress: {
    screen: DeliveryAddress,
    navigationOptions: {
      title: 'Delivery Address'
    }
  },
  // DeliveryDlts: {
  //   screen: DeliveryDlts,
  //   navigationOptions: {
  //     title: 'Delivery Address'
  //   }
  // },
  EditDeliveryAddres: {
    screen: EditDeliveryAddress
  },
  OrderDetails: {
    screen: OrderDetails,
    navigationOptions: {
      title: 'Order Details'
    }
  },
  Orders: {
    screen: Orders
  },
  SuccessScreen: {
    screen: SuccessScreen,
    navigationOptions: {
      header: null
    }
  },
  SignIn: {
    screen: SignInScreen,
    navigationOptions: {
      headerTitleStyle: { flex: 0.8, textAlign: 'center' },
      title: 'Sign In'
    }
  },
  SignUp: {
    screen: SignUpScreen,
    navigationOptions: {
      headerTitleStyle: { flex: 0.8, textAlign: 'center' },
      title: 'Sign Up'

    }
  },

}, {
    initialRouteName: 'Home'
  })

const AccountStack = createStackNavigator( {
  Account: {
    screen: AccountScreen,
    // navigationOptions:{
    //   headerTitle: 'Account',
    // }
  },
  AccountSetting: {
    screen: AccountSetting,
    navigationOptions: {
      title: 'Account Settings'
    }
  }
});
const AppDrawerNavigator = createDrawerNavigator({
  Home: AppStackNavigator,
  Wishlist: AppStackNavigator,
  Cart: AppStackNavigator,
  Shop: AppStackNavigator,
  // Product: AppStackNavigator,
  // SignIn: AppStackNavigator,
  // SignUp: AppStackNavigator,
  // Search: AppStackNavigator,
  // Orders: {
  //   screen: Orders,
  //   navigationOptions: {
  //     title: 'My Orders'
  //   }
  // },
  AccountStack : AccountStack
},{
  contentComponent: DrawerComponent,
  contentOptions: {
    activeTintColor: '#e91e63',
  }
});

const AppContainer = createSwitchNavigator({
  AuthLoading: AuthLoadingScreen,
  Auth: AuthStackNavigator,
  App: AppDrawerNavigator,
});

const MainAppContainer = createAppContainer(AppContainer)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
